/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.25-MariaDB : Database - retail_game
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `config` */

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `config_type` varchar(100) DEFAULT NULL,
  `config_code` int(11) DEFAULT NULL,
  `config_name` varchar(100) DEFAULT NULL,
  `config_value` int(20) DEFAULT NULL,
  `id_retail` varchar(200) DEFAULT NULL,
  `is_delete` int(11) DEFAULT '0',
  `game_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `config` */

LOCK TABLES `config` WRITE;

insert  into `config`(`config_type`,`config_code`,`config_name`,`config_value`,`id_retail`,`is_delete`,`game_id`) values ('RTN_BRG',1,'Return Barang',100,NULL,0,NULL);
insert  into `config`(`config_type`,`config_code`,`config_name`,`config_value`,`id_retail`,`is_delete`,`game_id`) values ('RTN_BRG',2,'Return Uang',50,NULL,0,NULL);
insert  into `config`(`config_type`,`config_code`,`config_name`,`config_value`,`id_retail`,`is_delete`,`game_id`) values ('RTN_BRG',3,'Return Uang',80,NULL,0,NULL);

UNLOCK TABLES;

/*Table structure for table `suplier` */

DROP TABLE IF EXISTS `suplier`;

CREATE TABLE `suplier` (
  `id_suplier` int(11) NOT NULL AUTO_INCREMENT,
  `nama_suplier` varchar(200) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `tgl` datetime NOT NULL,
  `photo_link` varchar(200) NOT NULL,
  `id_pemilik` varchar(200) NOT NULL,
  `jangka_waktu` int(11) DEFAULT '0',
  `ongkir` int(100) DEFAULT '0',
  `return_barang` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_suplier`,`photo_link`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

/*Data for the table `suplier` */

LOCK TABLES `suplier` WRITE;

insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (1,'Sinar Abadi','Gebang Wetan 23D','087849862859','toko1@toko.co.id','supplier gebang','2017-02-25 14:57:47','','2017',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (2,'Lancar Jaya','Kejawan Gebang 6 23','087849862777','toko2@toko.co.id','supplier Kejawan','2017-02-25 14:58:18','','2017',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (3,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','2017-02-25 14:59:01','','2017',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (4,'Sakinah','Keputih','031456123','toko4@toko.co.id','supplier keputih','2017-02-25 14:59:35','','51',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (5,'Maju Terus','Keputih tambak','0584865','toko5@toko.co.id','supplier keputih tambak','2017-02-25 15:00:42','','2017',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (6,'Sinar Abadi','Gebang Wetan 23D','087849862859','toko1@toko.co.id','supplier gebang','2017-02-25 14:57:47','','1235557',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (7,'Lancar Jaya','Kejawan Gebang 6 23','087849862777','toko2@toko.co.id','supplier Kejawan','2017-02-25 14:58:18','','1235557',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (8,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','2017-02-25 14:59:01','','1235557',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (11,'Sinar Abadi','Gebang Wetan 23D','087849862859','toko1@toko.co.id','supplier gebang','2017-02-25 14:57:47','','41253151\r\n',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (12,'Lancar Jaya','Kejawan Gebang 6 23','087849862777','toko2@toko.co.id','supplier Kejawan','2017-02-25 14:58:18','','41253151\r\n',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (13,'Sakinah','Keputih','031456123','toko4@toko.co.id','supplier keputih','2017-02-25 14:59:35','','41253151\r\n',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (14,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1111',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (15,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1111',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (16,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1111',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (17,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1112',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (18,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1112',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (19,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1112',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (20,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1113',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (21,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1113',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (22,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1113',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (23,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1114',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (24,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1114',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (25,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1114',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (26,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1115',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (27,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1115',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (28,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1115',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (29,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1116',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (30,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1116',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (31,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1116',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (32,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1117',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (33,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1117',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (34,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1117',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (35,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1118',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (36,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1118',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (37,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1118',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (38,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1119',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (39,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1119',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (40,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1119',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (41,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1120',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (42,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1120',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (43,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1120',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (44,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','9999',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (45,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','9999',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (46,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','9999',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (47,'Hidayah','Surabaya','123124124','hidayah@anak.sholeh','toko hidayah','2018-03-27 07:17:05','','9999',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (48,'dadada','sdsadasdasda','123112312312','team10@mbd.com','1231231','2018-03-27 17:58:45','','1120',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (49,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1121',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (50,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1121',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (51,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1121',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (52,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1122',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (53,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1122',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (54,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1122',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (55,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1123',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (56,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1123',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (57,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1123',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (58,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1124',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (59,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1124',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (60,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1124',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (61,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1125',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (62,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1125',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (63,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1125',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (64,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1126',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (65,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1126',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (66,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1126',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (67,'Sinar Abadi','Gebang Wetan 23D','87849862859','toko1@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1127',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (68,'Lancar Jaya','Kejawan Gebang 6 23','87849862777','toko2@toko.co.id','supplier Kejawan','0000-00-00 00:00:00','','1127',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (69,'Nikmat','Gebang Wetan','031 784921','toko3@toko.co.id','supplier gebang','0000-00-00 00:00:00','','1127',2,5000,2);
insert  into `suplier`(`id_suplier`,`nama_suplier`,`alamat`,`hp`,`email`,`deskripsi`,`tgl`,`photo_link`,`id_pemilik`,`jangka_waktu`,`ongkir`,`return_barang`) values (70,'surabaya pasar','keputih','081225116731','sakina@gmai.com','food items','2018-04-11 06:48:40','','1112',2,5000,2);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
