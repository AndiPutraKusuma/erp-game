<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_game');
	}

	public function index()
	{
		$this->load->model('DefaultMenu');
		$this->DefaultMenu->defaultLayout2();
		$this->load->view('dasboard/game';
		$this->load->view('dasboard/footer');
	}

	public function tes($flag)
	{
		if($flag)
		{
			$this->session->set_userdata('game',1);
			$this->session->set_userdata('tanggal_sekarang',date("m/d/Y H:i:s"));
		}
		else
		{
			$this->session->unset_userdata('game');
		}
		redirect('admin');
	}
	public function getPrice()
	{
		$price = $this->m_game->getInfo();
		return $price;
	}

	public function gameStart()
	{
		$tanggal = $this->session->userdata('tanggal');
		$cek = $this->m_game->cek($tanggal);
		if(!$cek)
		{
			$serapan = 800;
			$max = $serapan + (rand(-20,20)*$serapan/100);
			$price = $this->m_game->getInfo($max);

		}
		$this->session->set_userdata('tanggal_sekarang',date("m-d-Y H:i:s"));
		$datetime = new DateTime($this->session->userdata('tanggal'));
		$datetime->add(new DateInterval('P1D'));
		$tanggal =  $datetime->format('Y-m-d');
		$this->session->set_userdata('tanggal',$tanggal);
		echo json_encode(array('success'=>true));
	}



}
