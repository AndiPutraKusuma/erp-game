<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function index(){
		$cek=$this->session->userdata('username');
		$this->load->model('m_menu');
		$flag = $this->m_menu->checkAccess($this->session->userdata('roles'));
		if($flag)
		{
			//data header
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			// echo $data['cek'];
			$this->load->view('dasboard/core/user/index');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	public function data(){
		$cek=$this->session->userdata('username');
		if($cek){
			$id = $this->session->userdata('company');
			$this->load->model('m_user');
			$data = $this->m_user->read($id);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function insert(){
		$this->load->model('m_role');
		$jabatan = $this->m_role->view($this->input->post('privilege'));
		$data = array(
				'id_petugas' => $this->input->post('id_petugas'),
				'nama' => $this->input->post('nama'),
				'jenkel' => $this->input->post('jenkel'),
				'alamat' => $this->input->post('alamat'),
				'hp' => $this->input->post('hp'),
				'email' => $this->input->post('email'),
				'photo_link' => '',
				'jabatan' => $jabatan,
				'password' => md5($this->input->post('password')),
				'company_id' => $this->input->post('company_id'),
				'privilege' => $this->input->post('privilege'),

				'created_by' => $this->session->userdata('id_petugas'),
			);
		$this->load->model('m_user');
		$query=$this->m_user->create($data);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal memasukkan data.<br>Silahkan cek ID atau Email.'));
  		}
	}

	public function update($id){
		$this->load->model('m_role');
		$jabatan = $this->m_role->view($this->input->post('privilege'));
		$data = array(
				'id_petugas' => $this->input->post('id_petugas'),
				'nama' => $this->input->post('nama'),
				'jenkel' => $this->input->post('jenkel'),
				'alamat' => $this->input->post('alamat'),
				'hp' => $this->input->post('hp'),
				'email' => $this->input->post('email'),
				'photo_link' => '',
				'jabatan' => $jabatan,
				'password' => md5($this->input->post('password')),
				'company_id' => $this->input->post('company_id'),
				'privilege' => $this->input->post('privilege'),
				'last_mod_by' => $this->session->userdata('id_petugas'),
			);
		$this->load->model('m_user');
		$query=$this->m_user->update($data,$id);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal update data.<br> Terdapat data yang sama'));
  		}
	}

	public function delete($id){
		$data = array(
				'is_delete' => 1,
				'last_mod_by' => $this->session->userdata('id_petugas'),
			);
		$this->load->model('m_user');
		$query=$this->m_user->delete($data,$id);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal menghapus data.'));
  		}
	}
}
?>