<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	public function index(){
		$cek=$this->session->userdata('username');
		$this->load->model('m_menu');
		$flag = $this->m_menu->checkAccess($this->session->userdata('roles'));
		if($flag){
			//data header
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			// echo $data['cek'];
			$this->load->view('dasboard/core/role/index');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	public function data(){
		$cek=$this->session->userdata('username');
		if($cek){
			$id = $this->session->userdata('roles');
			$this->load->model('m_role');
			$data = $this->m_role->read($id);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function insert(){
		$data = array(
				'parent_roleId' => $this->session->userdata('roles'),
				'role' => $this->input->post('role'),
			);
		$this->load->model('m_role');
		$query=$this->m_role->create($data);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal memasukkan data.<br>Mungkin role sudah tersedia.'));
  		}
	}

	public function update($id){
		$data = array(
				'role' => $this->input->post('role'),
				'parent_roleId' => $this->session->userdata('roles'),
			);
		$this->load->model('m_role');
		$query=$this->m_role->update($data,$id);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal update data.<br>Sudah tersedia role dengan nama yang sama'));
  		}
	}
}
?>