<?php class M_user extends CI_Model {

	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

    public function read($id){
        $this->db->reconnect();
        $this->db->select();
        $this->db->where('company_id',$id);
        $this->db->where('is_delete',0);
        $this->db->from('petugas');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }

    public function create($data){
        $this->db->reconnect();
        // $query=$this->db->query("CALL sp_input_petugas('$data[ktp]','$data[nama]','$data[email]','$data[passwd]')");
        $query = $this->db->query("SELECT `id_petugas`,`email` FROM `petugas` WHERE `id_petugas` = '$data[id_petugas]' OR `email` = '$data[email]'");
        // print_r($query->num_rows());
        if($query->num_rows()==0){
            $ok = $this->db->insert('petugas',$data);
            // echo "yeay";
            // print_r($data);
            return $ok; 
        }
        else{
            // echo "yaah";
            return 0;
        }
        // $row=$query->row();
        // return $row->cek;
    }

    public function update($data,$id){
        $this->db->reconnect();
        $query = $this->db->query("SELECT `id_petugas` FROM `petugas` WHERE (`id_petugas` = '$data[id_petugas]' OR `email` = '$data[email]') AND `id_petugas` <> '$id'");
        // print_r("SELECT `id_petugas` FROM `petugas` WHERE (`id_petugas` = '$data[id_petugas]' OR `email` = '$data[email]') AND `id_petugas` <> '$id'");
        if($query->num_rows()==0){
            $this->db->where('id_petugas',$id);
            $ok = $this->db->update('petugas',$data);
            return $ok;
        } else {
            return 0;
        }
                
    }

    public function getId($email){
        // print_r($email);
        $this->db->reconnect();
        $query = $this->db->query("SELECT `company_id` FROM `petugas` where `email` = '$email'");
        // $query = "SELECT `company_id` FROM `petugas` where `email` = '$email'";
        // $this->db->select('company_id');
        // $this->db->where('email',$email);
        // $this->db->from('petugas');
        // $read = $this->db->get();
        // $child = $read->result();
        // $row = $read->num_rows();
        // var_dump($query);
        // if($row >0 )
        // {
        //     foreach ($child as $data) 
        //     {
        //         $result[] = $data;
        //     }        
        //     return $result;
        // }
        $row=$query->row();
        return $row->company_id;
    }

    public function delete($data,$id){
        $this->db->reconnect();
        // $query = $this->db->query("SELECT `id_petugas` FROM `petugas` WHERE (`id_petugas` = '$data[id_petugas]' OR `email` = '$data[email]') AND `id_petugas` <> '$id'");
        // print_r("SELECT `id_petugas` FROM `petugas` WHERE (`id_petugas` = '$data[id_petugas]' OR `email` = '$data[email]') AND `id_petugas` <> '$id'");
        // if($query->num_rows()==0){
            $this->db->where('id_petugas',$id);
            $ok = $this->db->update('petugas',$data);
            return $ok;
        // } else {
        //     return 0;
        // }
                
    }

}
?>
