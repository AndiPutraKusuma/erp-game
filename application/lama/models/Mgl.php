<?php 
class Mgl extends CI_Model{
	
	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }
	// public function laporanPenjualan(){
	// 	$this->db->reconnect();
	// 		$query = $this->db->query("CALL sp_laporan_penjualan()");
	// 		if ($query->num_rows() > 0)
	// 		{
	// 		foreach ($query->result() as $row)
	// 		{
	// 				$hasil[] = $row;
	// 		}
	// 		return $hasil;
	// 		}
	// 		else{
	// 			return 0;
	// 		}
	// }
	public function profitMargin($uid){
		$this->db->reconnect();
			$query = $this->db->query("select ((A.total-B.total)/B.total) as margin, (A.month) as month, (A.years) as tahun from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' group by month(h.period_id) DESC)A, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=5000 and h.uid='$uid' group by month(h.period_id) DESC)B WHERE A.month = B.month AND A.years=B.years GROUP BY A.month ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}

	public function asset($uid){
		$this->db->reconnect();
			$query2 = $this->db->query("SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id<2000 and h.uid='$uid' group by month(h.period_id) DESC");
			if ($query2->num_rows() > 0)
			{
			foreach ($query2->result() as $row)
			{
					$hasil2[] = $row;
			}
			return $hasil2;
			}
			else{
				return 0;
			}
	}

	public function tato($uid){
		$this->db->reconnect();
			$query2 = $this->db->query("select (A.total/B.total) as tato, (A.month) as month, (A.years) as tahun from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' group by month(h.period_id) DESC)A, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id<2000 and h.uid='$uid' group by month(h.period_id) DESC)B WHERE A.month = B.month AND A.years=B.years GROUP BY A.month");
			if ($query2->num_rows() > 0)
			{
			foreach ($query2->result() as $row)
			{
					$hasil2[] = $row;
			}
			return $hasil2;
			}
			else{
				return 0;
			}
	}

	public function roa($uid){
		$this->db->reconnect();
			$query = $this->db->query("select ((A.total-B.total)) as roa, (A.month) as month, (A.years) as tahun from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' group by month(h.period_id) DESC)A, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=5000 and h.uid='$uid' group by month(h.period_id) DESC)B WHERE A.month = B.month AND A.years=B.years  GROUP BY A.month ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	
	public function roe($uid){
		$this->db->reconnect();
			$query = $this->db->query("select ((A.total-B.total)/C.total) as roa, (A.month) as month, (A.years) as tahun from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' group by month(h.period_id) DESC)A, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=5000 and h.uid='$uid' group by month(h.period_id) DESC)B, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id<2000 and h.uid='$uid' group by month(h.period_id) DESC)C WHERE A.month = B.month AND B.month = C.month AND A.years=B.years AND B.years= C.years GROUP BY A.month ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function filterIncome($data){
		$this->db->reconnect();
			$query = $this->db->query("select l.acc_id,A.acc_name as nama, A.other as lain ,sum(l.line_debit) as total_dbt, sum(l.line_credit) as total_crd from gl_journal_l l, (select acc_name ,acc_code, other from gl_account)A where A.acc_code=l.acc_id and l.journal_id in (SELECT `id` FROM gl_journal_h where `period_id` >= '$data[tgl_awal]' and `period_id` <= '$data[tgl_akhir]' and (`uid`='$data[uid]' or `uid` is NULL)) group by l.acc_id order by l.acc_id ASC ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function filterCash($data){
		$this->db->reconnect();
			$query = $this->db->query("select l.acc_id,A.acc_name as nama, A.other as lain ,sum(l.line_debit) as total_dbt, sum(l.line_credit) as total_crd from gl_journal_l l, (select acc_name ,acc_code, other from gl_account where uid='$data[uid]' or uid is NULL) A where A.acc_code=l.acc_id and l.journal_id in (SELECT `id` FROM gl_journal_h where `period_id` > '$data[tgl_awal]' and `period_id` <= '$data[tgl_akhir]' and (`uid`='$data[uid]' or `uid` is NULL))  group by l.acc_id order by l.acc_id ASC ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	// public function laporanService(){
	// 	$this->db->reconnect();
	// 		$query = $this->db->query("CALL sp_laporan_service()");
	// 		if ($query->num_rows() > 0)
	// 		{
	// 		foreach ($query->result() as $row)
	// 		{
	// 				$hasil[] = $row;
	// 		}
	// 		return $hasil;
	// 		}
	// 		else{
	// 			return 0;
	// 		}
	// }
	public function filterBalance($data){
		$this->db->reconnect();
			$query = $this->db->query("select l.acc_id,A.acc_name as nama, sum(l.line_credit) as total_crd ,sum(l.line_debit) as total_dbt from gl_journal_l l, (select acc_name ,acc_code from gl_account)A where A.acc_code=l.acc_id and l.journal_id in (SELECT `id` FROM gl_journal_h where `period_id` >= '$data[tgl_awal]' and `period_id` <= '$data[tgl_akhir]') and (`uid`='$data[uid]' or `uid` is NULL) group by l.acc_id order by l.acc_id ASC ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
			// return $query->result();
	}
	// public function laporanDefect(){
	// 	$this->db->reconnect();
	// 		$query = $this->db->query("CALL sp_laporan_defect()");
	// 		if ($query->num_rows() > 0)
	// 		{
	// 		foreach ($query->result() as $row)
	// 		{
	// 				$hasil[] = $row;
	// 		}
	// 		return $hasil;
	// 		}
	// 		else{
	// 			return 0;
	// 		}
	// }
	// public function filterLaporanDefect($data){
	// 	$this->db->reconnect();
	// 		$query = $this->db->query("CALL sp_filter_laporanDefect('$data[tgl_awal]','$data[tgl_akhir]')");
	// 		if ($query->num_rows() > 0)
	// 		{
	// 		foreach ($query->result() as $row)
	// 		{
	// 				$hasil[] = $row;
	// 		}
	// 		return $hasil;
	// 		}
	// 		else{
	// 			return 0;
	// 		}
	// }
	public function lapHar($tanggal)
	{
		$this->db->reconnect();
		$result = array();
		$pembelian = array();
		$penjualan = array();
		$item_s = array();
		$item_p = array();
		$total_po =0;
		$total_so =0;

		$this->db->select('id_po');
		$this->db->where('tanggal_po',$tanggal);
		$read = $this->db->get('purchasing');
		if($read->num_rows() > 0 )
		{
			foreach ($read->result() as $data) 
			{
				$id_po[] = $data->id_po;
			}
			$this->db->select('item.id_item as id, sum(jumlah) as jumlah_barang , item.nama_item as name , max(hargaSatuan) as harga, sum(jumlah*hargaSatuan) as total');
			$this->db->where_in('id_purchasing',$id_po);
			$this->db->from('detail_purchasing as purchase');
			$this->db->join('item_master as item','item.id_item = purchase.id_item','INNER');
			$this->db->group_by('purchase.id_item');
			$this->db->order_by('item.id_item','asc');
			$read2 = $this->db->get();
			foreach ($read2->result() as $data2) 
			{
				 array_push(
	                        $item_p,
	                        array(
	                        	'id' => $data2->id,
	                            'name'=>$data2->name,
	                            'jumlah'=>$data2->jumlah_barang,
	                            'harga' => $data2->harga,
	                            'total' => $data2->total,
	                        )
	                );
				 $total_po = $total_po + $data2->total;
			}
		}
		$this->db->select('id_so');
		$this->db->where('tanggal',$tanggal);
		$read3 = $this->db->get('penjualan');
		if($read3->num_rows() > 0)
		{
			foreach ($read3->result() as $data3) 
			{
				$id_so[] = $data3->id_so;
			}
			$this->db->select('item.id_item as id,sum(jumlah) as jumlah_barang , item.nama_item as name , max(harga) as harga, sum(jumlah*harga) as total');
			$this->db->where_in('id_so',$id_so);
			$this->db->from('detail_penjualan as sales');
			$this->db->join('item_master as item','item.id_item = sales.id_item','INNER');
			$this->db->group_by('sales.id_item');
			$this->db->order_by('item.id_item','asc');
			$read2 = $this->db->get();
			foreach ($read2->result() as $data4) 
			{	//item
				 array_push(
	                        $item_s,
	                        array(
	                        	'id' =>$data4->id,
	                            'name'=>$data4->name,
	                            'jumlah'=>$data4->jumlah_barang,
	                            'harga' => $data4->harga,
	                            'total' => $data4->total,
	                        )
	                );
				 $total_so = $total_so + $data4->total;
			}
		}
		array_push(
                $pembelian,
                array(
                	'id' => 'A',
                    'name'=>'Pembelian',
                    'jumlah'=>'',
                    'harga' => '',
                    'total' => '',
                    'children' =>$item_p,
                )
        );
        array_push(
                $penjualan,
                array(
                	'id' => 'B',
                    'name'=>'penjualan',
                    'jumlah'=>'',
                    'harga' => '',
                    'total' => '',
                    'children' => $item_s,
                )
        );

		$sisa = $total_so - $total_po;
		array_push(
                $result,
                array(
                	'id' => 'C',
                    'name'=>'Pemasukan',
                    'jumlah'=>'',
                    'harga' => '',
                    'total' => $total_so,
                    'children' => $penjualan,
                )
        );
        array_push(
                $result,
                array(
                	'id' => 'D',
                    'name'=>'Pengeluaran',
                    'jumlah'=>'',
                    'harga' => '',
                    'total' => $total_po,
                    'children' => $pembelian,
                )
        );
        array_push(
                $result,
                array(
                	'id' => 'E',
                    'name'=>'Total Pendapatan',
                    'jumlah'=>'',
                    'harga' => '',
                    'total' => $sisa,
                    'children' => '',
                )
        );
        return $result;
	}

	public function jenis_produk()
	{
		$id = $this->session->userdata('id_retail');
		$item_s = array();
		$head = array('ds,y');
		$delimiter = ',';
		$enclosure = '|';
		$this->db->select('DATE_SUB(max(tanggal), INTERVAL 500 DAY)');
		$this->db->where('id_pemilik','1235557');
		$maxr = $this->db->get('penjualan');
		foreach ($maxr->row() as $limit) 
			{
				$limit_date = $limit;
			}
		$this->db->select('id_so');
		// $this->db->where('id_pemilik',$id);
		$this->db->where('id_pemilik','1235557');
		$this->db->where('tanggal >', $limit_date);
		$read3 = $this->db->get('penjualan');
		if($read3->num_rows() > 0)
		{
			foreach ($read3->result() as $data3) 
			{
				$id_so[] = $data3->id_so;
			}
			$this->db->select('id_item');
			// $this->db->where('id_pemilik',$id);
			// $this->db->where('id_pemilik',1);
			$read4 = $this->db->get('item_master');
			if($read4->num_rows() >0)
			{
				foreach ($read4->result() as $data5) {
					$item_s[$data5->id_item] = array();
					$this->db->select('sum(jumlah) as y ,  jual.tanggal as ds');
					$this->db->where('sales.id_item',$data5->id_item);
					$this->db->where_in('sales.id_so',$id_so);
					$this->db->from('detail_penjualan as sales');
					$this->db->join('penjualan as jual','jual.id_so = sales.id_so','INNER');
					$this->db->group_by('jual.tanggal');
					$this->db->order_by('jual.tanggal','asc');
					$read2 = $this->db->get();
					foreach ($read2->result() as $data4) 
					{	
						array_push(
	                        $item_s[$data5->id_item],
	                        array(
	                        	'ds' =>$data4->ds,
	                            'y'=>$data4->y,
	                        )
	                	);
					}
					// print_r($item_s[$data5->id_item]);
					$file = fopen('C:\py_file\test'.$data5->id_item.'.csv', 'w');
                	foreach ($head as $line)
					{
						fputcsv($file,explode(',',$line));
					}
                	foreach ($item_s[$data5->id_item] as $line) {
                		// fwrite($file, "\r\n");
                		fputcsv($file, $line);
                		// fwrite($file, "lala\n")
                	}
                	fclose($file);
                	$forecast[$data5->id_item] = $this->mgl->forecast('C:\py_file\test'.$data5->id_item);	
				}
			}
		}
		// return $forecast;
	}

	public function forecast($path)
	{
			// $handle = popen('C:\py_file\fb.py', 'r');
			// echo "'$handle'; " . gettype($handle) . "\n";
			// $read = fread($handle, 2096);
			// echo $read;
			// pclose($handle);

			// exec('py C:\py_file\fb.py C:\py_file\sales.csv', $output, $return);
			
			exec('py C:\py_file\fp_asli.py '.$path, $output, $return);
			// print_r($output);
			$result['date'] = [];
			$result['val'] = [];
			if (sizeof($output)>7) {
				for ($i=7; $i > 0; $i--) { 
					$date = explode(" ",$output[(sizeof($output)-$i)]);
					array_push($result['date'], $date[sizeof($date)-3]);
					array_push($result['val'], $date[sizeof($date)-1]);
					// print_r($date);
				}
			} 
			// for ($i=8; $i > 1; $i--) { 
				// $date = explode(" ",$output[(sizeof($output)-$i)]);
			// 	array_push($result['date'], $date[sizeof($date)-1]);
			// print_r($date);
			// }

			// for ($i=12; $i > 7; $i--) { 
			// 	$val = explode(" ",$output[(sizeof($output)-$i)]);
			// 	array_push($result['val'], $val[sizeof($val)-1]);
			// 	// $print = explode(" ",$output[(sizeof($output)-$i)]);
			// 	// echo $val[4];
			// }
			return $result;
			// print_r($result);
	}

	public function buildTree(array $elements) {
	    // $branch = array();
	    $result = array();
	    $akhir = array();

		// print_r($elements);

	    foreach ($elements as $element) {
	    	$this->db->reconnect();
			$this->db->select('id, acc_code as code, acc_name as name');
			$this->db->where('acc_code',$element['child']);
			$this->db->from('gl_account');
			$read = $this->db->get();
	        if ($read->num_rows() >0) {
	        	foreach ($read->result_array() as $data) {
					$result=$data;
					$this->db->select('child');
					$this->db->where('parent',$data['code']);
					$this->db->from('gl_has');
					$read = $this->db->get();
					if ($read->num_rows() > 0) {
						// foreach ($read->result_array() as $data) {
						// 	unset($result2);
						// 	$result2[]=$data;
						// }
						$result2 = $read->result_array();
						$result['children']=$this->mgl->buildTree($result2);
					}
				}
	        }
	        $akhir[]= $result;
	    }
		return $akhir;	    
	}

	public function account()
	{
		$result = array();
		$real_result = array();
		// $id = $this->session->userdata('id_retail');
		$this->db->reconnect();
		$this->db->select('child');
		// $this->db->where('acc.uid',$id);
		// $this->db->where('acc.uid',NULL);
		$this->db->where('parent',NULL);
		$this->db->from('gl_has');
		$read = $this->db->get();
		if ($read->num_rows() > 0) {
			foreach ($read->result_array() as $data) {
				$result[]=$data;
			}
			$real_result= $this->mgl->buildTree($result);
		}
		return $real_result;
		// print_r($real_result);
	}

	 public function getGroup(){
        $this->db->reconnect();
        $this->db->select('coa_number, CONCAT(coa_number," - ",coa_name) AS nama');
        // $this->db->where('parent_id',$id);
        $this->db->from('gl_group');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
                // $result[]->text = $data->coa_number.' - '.$data->coa_name;
            }        
            return $result;
        }
    }

    public function listAcc($id){
        $this->db->reconnect();
        $this->db->select('acc_code, CONCAT(acc_code," - ",acc_name) AS acc_text');
    	$this->db->where('coa_id',$id);
        $this->db->order_by('acc_code');
        $this->db->from('gl_account');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
                // $result[]->text = $data->coa_number.' - '.$data->coa_name;
            }        
            return $result;
        }
    }

    public function listAcc2(){
        $this->db->reconnect();
        $this->db->select('acc_code as acc_id, CONCAT(acc_code," - ",acc_name) AS acc_text');
        $this->db->order_by('acc_code');
        $this->db->from('gl_account');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
                // $result[]->text = $data->coa_number.' - '.$data->coa_name;
            }        
            return $result;
        }
    }

    public function variabel(){
        $this->db->reconnect();
        $this->db->select();
        // $this->db->order_by('acc_code');
        $this->db->from('gl_variable');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
                // $result[]->text = $data->coa_number.' - '.$data->coa_name;
            }        
            return $result;
        }
    }

    public function create($data,$data2){
        $this->db->reconnect();
        // $query=$this->db->query("CALL sp_input_petugas('$data[ktp]','$data[nama]','$data[email]','$data[passwd]')");
        $query = $this->db->query("SELECT `acc_code` FROM `gl_account` WHERE `acc_code` = '$data[acc_code]'");
        // print_r($query->num_rows());
        if($query->num_rows()==0){
            $ok = $this->db->insert('gl_account',$data);
            if ($ok) {
            	$yes = $this->db->insert('gl_has',$data2);
            	return $yes;
            }
            else{
            	return false;
            }
            // echo "yeay";
             
        }
        else{
            // echo "yaah";
            return 0;
        }
        // $row=$query->row();
        // return $row->cek;
    }

    public function schema(){
        $this->db->reconnect();
        $this->db->select();
        // $this->db->where('parent_roleId',$id);
        $this->db->from('gl_schema_h');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }

    public function schema_line($id){
        $this->db->reconnect();
        $this->db->select();
        $this->db->where('journal_id',$id);
        $this->db->from('gl_schema_l');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
        else
        	return 0;
    }

    public function schema_insert($data){
        $this->db->reconnect();
        // $query=$this->db->query("CALL sp_input_petugas('$data[ktp]','$data[nama]','$data[email]','$data[passwd]')");
        $ok = $this->db->insert('gl_schema_l',$data);
        return $ok;
        // echo "yeay";         
    }

    public function schema_update($data,$id){
        $this->db->reconnect();
        $this->db->where('id',$id);
        $ok = $this->db->update('gl_schema_l',$data);
        return $ok;       
    }

    public function delete($id){
        $this->db->reconnect();
        $this->db->where('id',$id);
        $ok = $this->db->delete('gl_schema_l');
        return $ok;
        // } else {
        //     return 0;
        // }
                
    }
}
?>