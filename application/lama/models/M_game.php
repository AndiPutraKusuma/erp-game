<?php class M_game extends CI_Model {

    public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
    public function getStock($id_barang,$pemilik)
    {
        // print_r($id_barang.' '.$pemilik);
        $stock = array();
        $this->db->reconnect();
        $this->db->select('stock,supplier_id');
        $this->db->where('id_barang',$id_barang);
        $this->db->where('id_retail',$pemilik);
        $read = $this->db->get('item_stock');
        if($read->num_rows() > 0)
        {
            foreach ($read->result() as $data) 
            {
                array_push(
                        $stock,
                        array(
                            'stock'=>$data->stock,
                            'supplier_id'=>$data->supplier_id,
                        )
                ); 

            }
        }
        else
        {
            array_push(
                        $stock,
                        array(
                            'stock'=>0,
                            // 'supplier_id'=>$data->supplier_id,
                        )
                ); 
        }
        // print_r($stock);
        return $stock;
    }  
    public function getInfo($max)
    {
        $company_id = $this->session->userdata('company');
        $idPet=$this->session->userdata('id_retail');
        $homina = array();
        $this->db->reconnect();
        $this->db->select('id_petugas');
        $this->db->where('company_id',$company_id);
        $this->db->where('privilege = 17');
        $read = $this->db->get('petugas');
        foreach ($read->result() as $data) 
        {
            $users[] = $data->id_petugas;
        }
        // print_r($users);
        // $this->db->select('id_item , item_harga , item_marketing ,id_pemilik');
        // $this->db->distinct('id_item');
        $temp = array();
        $this->db->select('tipe');
        $this->db->where_in('id_pemilik',$users);
        $readItem = $this->db->get('item_master');
        foreach ($readItem->result() as $datacek) 
        {
            if(!in_array($datacek->tipe, $temp))
            {
                $temp[] = $datacek->tipe; 
            }
        }
        foreach ($temp as $data2) 
        {   
            $this->db->select('sum(item_harga) as maxHarga , sum(item_marketing) as maxMarket');
            $this->db->where('tipe ',$data2);
            $this->db->where_in('id_pemilik',$users);
            $total = $this->db->get('item_master');
            foreach ($total->result() as $data) 
            {
                $totalHarga = $data->maxHarga;
                $totalMarket = $data->maxMarket;
            }
            $this->db->select('id_item, item_harga , item_marketing ,id_pemilik');
            $this->db->where('tipe ',$data2);
            $this->db->where_in('id_pemilik',$users);
            // $this->db->group_by('tipe');
            $readUser = $this->db->get('item_master');
            foreach ($readUser->result() as $data3) 
            {
                $id_so = uniqid("SO");
                $serapHarga = ($totalHarga - $data3->item_harga ) / $totalHarga;
                // $serapHarga = $data3->item_harga / $totalHarga;
                $serapMarket = $data3->item_marketing / $totalMarket;
                $serapan = floor(((0.7 * $serapHarga) + (0.3 * $serapMarket))*$max);
                $total = $data3->item_harga * $serapan;
                if($serapan > 0)
                {
                    $cekstock = $this->getStock($data3->id_item,$data3->id_pemilik);
                    $this->db->trans_start();
                    // var_dump($cekstock);
                    if($cekstock[0]['stock'] > 0)
                    {
                        echo $cekstock[0]['stock'].' ';
                        foreach ($cekstock as $param) 
                        {
                            if($serapan <= $param['stock'])
                            {
                                $dataDetail = array(
                                    'id_so' => $id_so,
                                    'id_item' => $data3->id_item,
                                    'jumlah' => $serapan,
                                    'harga' => $data3->item_harga,
                                    'id_suplier' =>$param['supplier_id'],
                                    'id_pemilik' =>$data3->id_pemilik,
                                );
                                $stock = $param['stock'] - $serapan;       
                            }
                            else
                            {
                                $dataDetail = array(
                                    'id_so' => $id_so,
                                    'id_item' => $data3->id_item,
                                    'jumlah' => $param['stock'],
                                    'harga' => $data3->item_harga,
                                    'id_suplier' =>$param['supplier_id'],
                                    'id_pemilik' =>$data3->id_pemilik,
                                );
                                $serapan = $serapan - $param['stock'];
                                $stock = 0;

                            }
                            $ok2 = $this->db->insert('detail_penjualan',$dataDetail);
                            if($ok2)
                            {
                                $update = array(
                                'stock' => $stock,
                                'last_mod_user' => $data3->id_pemilik,
                                'last_mod_time' => $this->session->userdata('tanggal'),
                                );
                                $this->db->where('id_barang',$data3->id_item);
                                $this->db->where('supplier_id',$param['supplier_id']);
                                $this->db->where('id_retail',$data3->id_pemilik);
                                $this->db->update('item_stock',$update);
                            }
                            if($total == 0)
                            {
                                break;
                            }
                        }
                        if($ok2)
                        {
                            $hasil = array(
                                        'id_so' => $id_so, 
                                        'id_petugas' => $data3->id_pemilik,
                                        'id_customer' => 1,
                                        'tanggal' =>  $this->session->userdata('tanggal'),
                                        'total' => $total ,
                                        'kurir' => 1,
                                        'status' => 0,
                                        'tanggal_keluar' => $this->session->userdata('tanggal'),
                                        'id_pemilik' => $data3->id_pemilik,
                                    );
                           
                            $ok = $this->db->insert('penjualan',$hasil);
                            if ($ok)
                            {
                                $this->db->trans_commit();
                            }
                            else
                            {
                                $this->db->trans_rollback();
                            }
                        }
                        else
                        {
                            $this->db->trans_rollback();
                        }
                    }
                    // else
                    // {
                    //     echo 'stock kosong';
                    // }
                }
                else
                {
                    echo 'total 0';
                }
            }
        }
                // if($ok)
                // {
                //     $dataDetail = array(
                //             'id_so' => $id_so,
                //             'id_item' => $data3->id_item,
                //             'jumlah' => $serapan,
                //             'harga' => $data3->item_harga,
                //             'id_suplier' =>1,
                //             'id_pemilik' =>$data3->id_pemilik,
                //         );
                //     $ok2 = $this->db->insert('detail_penjualan',$dataDetail);
                //     if($ok2)
                //     {
                //         $this->db->trans_commit();
                //     }
                //     else
                //     {
                //         $this->db->trans_rollback();
                //         break;
                //     }
                // }
                // else
                // {
                //     $this->db->trans_rollback();
                //     break;
                // }
        //     }
        // }
        return true;
    }  

    public function cek($tanggal)
    {
        $this->db->reconnect();
        $this->db->select('id_petugas');
        $this->db->where('company_id',$this->session->userdata('company'));
        $this->db->where('privilege = 17');
        $read = $this->db->get('petugas');
        foreach ($read->result() as $data) 
        {
            $users[] = $data->id_petugas;
        }

        // $this->db->select('count(1) as tes');
        $this->db->where('tanggal',$tanggal);
        $this->db->where_in('id_pemilik',$users);
        $ok = $this->db->get('penjualan');
        return $ok->result();
    }
}
?>
