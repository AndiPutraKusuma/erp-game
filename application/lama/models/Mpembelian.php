<?php
class Mpembelian extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }
	 //add data
	  public function addPurchasing($data,$idjurnalL,$idjurnalH,$pemilik){
		$temp=$idjurnalH;
		$this->db->reconnect();
		$query=$this->db->query("CALL sp_input_purchasing('$data[idTransaksi]','$data[idSuplier]','$data[email]','$data[tgl]','$data[total]','$data[id_pemilik]','$data[ongkir]')");
		foreach($this->cart->contents() as $item){
			$this->db->reconnect();
			$this->db->query("CALL sp_input_detailPurchasing('$data[idTransaksi]','$item[id]','$item[qty]','$item[price]','$data[idSuplier]')");

		}
		$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$data[tgl]',CONCAT('Pembelian ','$data[idTransaksi]'),'$data[id_pemilik]')");

		$this->load->model('mgl');
		$schema = $this->mgl->schema_line(1);
		$searchArray = array("HB", "HJ", "0");
		$replaceArray = array($data['total'],0,0);
		foreach ($schema as $line) {
			$data_line = array(
				'journal_id' => $temp,
				'acc_id' => $line->acc_id,
				'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
				'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
				'uid' => $this->session->userdata('id_petugas'),
			);
			$oke = $this->db->insert('gl_journal_l',$data_line);
		}

		$this->cart->destroy();
	 }
	 //list pemesanan
	 public function listPemesanan($id){
		 $this->db->reconnect();
			$query = $this->db->query("CALL sp_list_purchasing($id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	 }
	 public function pageList_pembelian($start,$limit,$id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_pageList_pembelian($start,$limit,$id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	 //deatil pemesanan
	 public function rincianPemesanan($id){
		 $this->db->reconnect();
			$query = $this->db->query("CALL sp_rincianPembelian('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row) 
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	 }
	 //update status
	 public function updateStatus($data){
		 $this->db->reconnect();
		 $query=$this->db->query("CALL sp_updateStatus_pembelian('$data[idTransaksi]','$data[status]')");
	 }
	 public function countPembelian($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungPembelian($id)");

				$row=$query->row();
				return $row->jumlah;


	}
	 public function countPembelianSukses($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungPembelianSukses($id)");

				$row=$query->row();
				return $row->jumlah;
	}
				
	public function purchasing_header($data)
	{
		$this->db->reconnect();
		$this->db->trans_start();
		$ok = $this->db->insert('purchasing',$data);
		$this->load->model('mpenjualan');
		$haha = $this->mpenjualan->cek2();
		$idjurnalH = $haha->apa;
		$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$data[tanggal_po]',CONCAT('Pembelian ','$data[id_po]'),'$data[id_pemilik]')");
		$this->load->model('mgl');
		$schema = $this->mgl->schema_line(1);
		$searchArray = array("HB", "HJ", "0");
		$replaceArray = array($data['totalHarga'],0,0);
		foreach ($schema as $line) {
			$data_line = array(
				'journal_id' => $idjurnalH,
				'acc_id' => $line->acc_id,
				'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
				'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
				'uid' => $this->session->userdata('id_petugas'),
			);
			$okesip = $this->db->insert('gl_journal_l',$data_line);
		}
		if($ok)
		{
			$this->db->trans_commit();
			return true;
		}
		else
		{
			$this->db->trans_rollback();
			return false;	
		}
	}
	public function addPembelian($data,$supplier)
	{
		$id_pemilik = $this->session->userdata('id_retail');			
		$this->db->reconnect();
		$this->db->trans_start();
		$insert = array(
			'id_purchasing' => $data['id_po'],
			'id_item' => $data['id_barang'],
			'jumlah' => $data['jumlah'],
			'hargaSatuan' => $data['harga'],
			'id_pemilik' =>$id_pemilik,
			'id_supplier' => $supplier,
			);
		$ok = $this->db->insert('detail_purchasing',$insert);
		if($ok)
		{
			$this->db->trans_commit();
			return true;
		}
		else
		{
			$this->db->trans_rollback();
			return false;
		}
	}
}
?>
