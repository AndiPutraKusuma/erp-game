<?php class Mbi extends CI_Model {

	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

    public function read($id){
        $this->db->reconnect();
        $this->db->select();
        $this->db->where('parent_id',$id);
        $this->db->from('core_company');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }

    public function create_rop($data){
        $this->db->reconnect();
        // $query=$this->db->query("CALL sp_input_petugas('$data[ktp]','$data[nama]','$data[email]','$data[passwd]')");
        $query = $this->db->query("SELECT `id`,`id_item`,`id_pemilik` FROM `bi_rop` WHERE `id_item` = '$data[id_item]' AND `id_pemilik` = '$data[id_pemilik]'");
        // print_r($query->num_rows());
        if($query->num_rows()==0){
            $ok = $this->db->insert('bi_rop',$data);
            // echo "yeay";
            return $ok; 
        }
        else{
            $child = $query->result();
            foreach ($child as $item) 
            {
                $id = $item->id;
            }
            $this->db->where('id',$id);
            $ok = $this->db->update('bi_rop',$data);
            return $ok;
            // echo "yaah";
            // return 0;
        }
        // $row=$query->row();
        // return $row->cek;
    }

    public function update($data,$id){
        $this->db->reconnect();
        $query = $this->db->query("SELECT `id` FROM `core_company` WHERE `company_code` = '$data[company_code]' AND `parent_id` = '$data[parent_id]' AND `id` <> '$id'");
        // print_r($query->num_rows());
        if($query->num_rows()==0){
            $this->db->where('id',$id);
            $ok = $this->db->update('core_company',$data);
            return $ok;
        } else {
            return 0;
        }
                
    }

    public function getId($email){
        // print_r($email);
        $this->db->reconnect();
        $query = $this->db->query("SELECT `company_id` FROM `petugas` where `email` = '$email'");
        // $query = "SELECT `company_id` FROM `petugas` where `email` = '$email'";
        // $this->db->select('company_id');
        // $this->db->where('email',$email);
        // $this->db->from('petugas');
        // $read = $this->db->get();
        // $child = $read->result();
        // $row = $read->num_rows();
        // var_dump($query);
        // if($row >0 )
        // {
        //     foreach ($child as $data) 
        //     {
        //         $result[] = $data;
        //     }        
        //     return $result;
        // }
        $row=$query->row();
        return $row->company_id;
    }

    public function getComp($id){
        $this->db->reconnect();
        // $this->db->select('');
        // $this->db->where('parent_id',$id);
        // $this->db->from('core_company');
        $read = $this->db->query("SELECT id, company_code, company_name FROM core_company WHERE id = '$id' OR parent_id = '$id'");
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }

    public function findCompany($id)
    {
        $where = 'id ='.$id.' OR parent_id = '.$id;
        $result = array();
        $this->db->reconnect();
        $this->db->select('id');
        $this->db->where($where);
        $read = $this->db->get('core_company');
        if($read->num_rows() > 0 )
        {    
            foreach ($read->result() as $data) 
            {
                $result[] = $data->id; 
            }
        }
        return $result;
    }
}
?>
