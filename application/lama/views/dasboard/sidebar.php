      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url() ?>assets/images/default/institusi.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Retail</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="<?php echo base_url()?>admin">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa pull-right"></i>
              </a>

            </li>
             <?php if ($menu)
             {              
               foreach ($menu as $data) 
               {
               ?>
                    <li class="treeview">
                        <a href="<?php  echo base_url().$data['parent']->controller ?>">
                          <i class="<?php  echo $data['parent']->icon_name ?>"></i> <span><?php $parent = $data['parent']; echo $parent->name;?></span>
                          <?php 
                                if(isset($data['child']))
                                  {  ?>
                          <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                              <?php
                                    foreach ($data['child'] as $child) { ?>
                                <li style="color: white"><a href = '<?php echo base_url().$child->controller; ?>'>  <i class="<?php echo $child->icon_name; ?>"></i> 
                                <?php echo $child->name; ?></a></li>
                        
                              <?php } 
                              echo '</ul>';
                              } else {
                                echo '</a>';
                                } ?>
                        
                     </li>

             <?php }} ?>

           <li class="">
              <a href="<?php echo base_url() ?>admin/logout">
                <i class="fa fa-power-off"></i> <span>Logout</span>

              </a>

            </li>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
