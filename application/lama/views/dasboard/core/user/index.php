<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          List User
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">User</a></li>
          <li class="active">List User</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <table id="user" class="easyui-datagrid" style="width:auto;height:400px"
                      url="<?php echo base_url() ?>user/data"
                      toolbar="#toolbar"
                      rownumbers="true" fitColumns="true" singleSelect="true" pagination="true">
                  <thead>
                      <tr>
                          <th field="id_petugas" width="50">No. ID</th>
                          <th field="nama" width="50" sortable="true">Nama</th>
                          <th field="email" width="50">Email</th>
                          <th field="hp" width="50">No. Telepon</th>
                          <th field="jabatan" width="50">Bagian</th>
                      </tr>
                  </thead>
                </table>
                <div id="toolbar">
                  <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">Add User</a>
                  <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit User</a>
                  <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove User</a>
                </div>
                <div id="UserForm" class="easyui-dialog" style="width:50%; height:75%; padding: 10px 20px" closed="true" buttons="#dialog-buttons">
                  <form id="form" method="post" novalidate>
                      <div class="form-item">
                          <label for="company_id" style="font-size: 16px; margin-top: 10px">Company</label><br />
                          <input class="easyui-combobox" name="company_id" data-options="valueField:'id', textField:'company_name', url:'<?php echo base_url() ?>company/getCompany', panelHeight:'auto', editable:false" required="true" style="width:85%;" />
                      </div>
                      <div class="form-item">
                          <label for="ktp" style="font-size: 16px; margin-top: 10px">No. KTP</label><br />
                          <input type="text" name="id_petugas" class="easyui-validatebox" required="true" style="width:85%;" maxlength="50" />
                      </div>
                      <div class="form-item">
                          <label for="name" style="font-size: 16px; margin-top: 10px">Nama</label><br />
                          <input type="text" name="nama" class="easyui-validatebox" required="true" style="width:85%;" maxlength="50" />
                      </div>
                      <div class="form-item">
                          <label for="jenkel" style="font-size: 16px; margin-top: 10px">Jenis Kelamin</label><br />
                          <select class="easyui-combobox" name="jenkel" style="width:85%;" required="true" data-options=" panelHeight:'auto', editable:false">
                              <option value="L">Laki - Laki</option>
                              <option value="P">Perempuan</option>
                          </select>
                      </div>
                       <div class="form-item">
                          <label for="alamat" style="font-size: 16px; margin-top: 10px">Alamat</label><br />
                          <input type="text" name="alamat" class="easyui-validatebox" required="true" style="width:85%;" maxlength="50" />
                      </div>
                      <div class="form-item">
                          <label for="hp" style="font-size: 16px; margin-top: 10px">No.Telephone</label><br />
                          <input type="text" name="hp" class="easyui-validatebox" required="true" style="width:85%;" maxlength="50" />
                      </div>
                      <div class="form-item">
                          <label for="email" style="font-size: 16px; margin-top: 10px">Email</label><br />
                          <input type="email" name="email" class="easyui-validatebox" required="true" style="width:85%;" maxlength="50" />
                      </div>
                      <div class="form-item">
                          <label for="passwd" style="font-size: 16px; margin-top: 10px">Password</label><br />
                          <input type="password" name="password" class="easyui-validatebox" required="true" style="width:85%;" maxlength="50" />
                      </div>
                      <div class="form-item">
                          <label for="role" style="font-size: 16px; margin-top: 10px;">Role</label><br />
                          <input class="easyui-combobox" name="privilege" data-options="valueField:'roleId', textField:'role', url:'<?php echo base_url() ?>role/data', panelHeight:'auto', editable:false" required="true" style="width:85%;" />
                      </div>
                  </form>
              </div>
               
              <!-- Dialog Button -->
              <div id="dialog-buttons">
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Simpan</a>
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#UserForm').dialog('close')">Batal</a>
              </div>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">
    function newUser(){
      $('#UserForm').dialog('open').dialog('setTitle','New User');
      $('#form').form('clear');
      url = '<?php echo base_url() ?>user/insert';
    }

    function editUser(){
      var row = $('#user').datagrid('getSelected');
      if (row){
          $('#UserForm').dialog('open').dialog('setTitle','Edit User');
          $('#form').form('load',row);
          url = '<?php echo base_url() ?>user/update/'+row.id_petugas;
      }
    }

    function save(){
        jQuery('#form').form('submit',{
            url: url,
            onSubmit: function(){
                return jQuery(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                    jQuery('#UserForm').dialog('close');
                    jQuery('#user').datagrid('reload');
                    $.messager.alert({
                        title: 'Berhasil',
                        msg: 'Berhasil memasukkan data!',
                        icon: 'info'
                    });
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
        });
    }

    function destroyUser(){
        var row = $('#user').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to delete this user?',function(r){
                if (r){
                    $.post('<?php echo base_url() ?>user/delete/'+row.id_petugas,'',function(result){
                        if (result.success){
                            $('#user').datagrid('reload');    // reload the user data
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                }
            });
        }
    }
    // function destroyUser(){
    //   var row = $('#user').datagrid('getSelected');
    //   if (row){
    //       // $('#UserForm').dialog('open').dialog('setTitle','Edit User');
    //       $.messager.confirm('Confirm','Are you sure you want to delete this user?',function(r){
    //         if (r){
    //           url = '<?php echo base_url() ?>user/delete/'+row.id_petugas;
    //         }
    //       // $('#form').form('load',row);
          
    //   }
    // }

  </script>
     