 <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
       <!-- Content Header (Page header) -->
       <section class="content-header">
         <h1>
           List Menu
         </h1>
         <ol class="breadcrumb">
           <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           <li><a href="#">Menu</a></li>
           <li class="active">List Menu</li>
         </ol>
       </section>
 
       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12 col-lg-12">
             <div class="box">
               <div class="box-body table-responsive no-padding">
                 <?php if($this->session->flashdata('pesan')){
                   echo $this->session->flashdata('pesan');
                 } ?>
                 <table id="menu_data" class="easyui-datagrid" style="width:auto;height:400px"
                       url="<?php echo base_url() ?>menu/listAllMenu"
                       toolbar="#toolbar"
                       rownumbers="true" fitColumns="true" singleSelect="true" pagination="true">
                   <thead>
                       <tr>
                           <th field="name" width="50">Menu Name</th>
                          <th field="parent" width="50">Parent</th>
                          <th field="controller" width="50">Menu Link</th>
                          <th field="icon_name" width="50">Icon</th>
                       </tr>
                   </thead>
                 </table>
                 <div id="toolbar">
                   <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newMenu()">New Menu</a>
                   <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editMenu()">Edit Menu</a>
                   <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyMenu()">Delete Menu</a>
 
                   <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Company</a> -->
                 </div>
                 <div id="formMenu" class="easyui-dialog" style="width:400px; height:400px; padding: 10px 20px" closed="true" buttons="#dialog-buttons">
                   <form id="form" method="post" novalidate>
                       <div class="form-item">
                           <label for="type" style="font-size: 16px; margin-top: 10px">Menu Name</label><br />
                           <input type="text" name="name" class="easyui-validatebox" required="true" size="40" maxlength="50" />
                       </div>
                       <div class="form-item">
                           <label for="type" style="font-size: 16px; margin-top: 10px">Parent</label><br />
                           <input class="easyui-combobox" 
                           name="parent_id"
                            size="35"
                            data-options="
                                 url:'<?php echo base_url() ?>menu/listAllParent',
                                  valueField:'id',
                                  textField:'name',
                                  panelHeight:'auto'
                            ">
                       </div>
                       <div class="form-item">
                           <label for="type" style="font-size: 16px; margin-top: 10px">Menu Link</label><br />
                          <input type="text" name="controller" class="easyui-validatebox" required="true" size="40" maxlength="50" placeholder="Ex : Controller/link" />
                       </div>
                       <div class="form-item">
                           <label for="type" style="font-size: 16px; margin-top: 10px">Menu Icon</label><br />
                          <input type="text" name="icon_name" class="easyui-validatebox" required="true" size="40" maxlength="50" placeholder="Ex : fa fa-icon" />
                       </div>
 
                   </form>
               </div>
                
               <!-- Dialog Button -->
               <div id="dialog-buttons">
                   <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Simpan</a>
                   <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#formMenu').dialog('close')">Batal</a>
               </div>
               </div><!-- /.box-body -->
             </div><!-- /.box -->
           <div class="row">
             <div class="col-md-12 text-center">
               <?php //echo $paging; ?>
             </div>
           </div>
         </div>
       </div>
     </section>
   </div>
 
   <script type="text/javascript">
     function newMenu(){
       $('#formMenu').dialog('open').dialog('setTitle','New Menu');
       $('#formMenu').form('clear');
       url = '<?php echo base_url() ?>Menu/addMenu_act';
       // alert(url);
     }
 
     function editMenu(){
       var row = $('#menu_data').datagrid('getSelected');
       if (row){
           $('#formMenu').dialog('open').dialog('setTitle','Edit Menu');
           $('#form').form('load',row);
           url = '<?php echo base_url() ?>Menu/updateMenu_act/'+row.id;
       }
     }
 
     function save(){
         jQuery('#form').form('submit',{
             url: url,
             onSubmit: function(){
                 return jQuery(this).form('validate');
             },
             success: function(result){
                 var result = eval('('+result+')');
                 if(result.success){
                     jQuery('#formMenu').dialog('close');
                     jQuery('#menu_data').datagrid('reload');
                     $.messager.alert({
                         title: 'Berhasil',
                         msg: 'Berhasil memasukkan data!',
                         icon: 'info'
                     });
                 } else {
                     $.messager.alert({
                         title: 'Error',
                         msg: result.msg,
                         icon: 'error'
                     });
                 }
             }
         });
     }
 
     function destroyMenu(){
        var row = $('#menu_data').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to delete this user?',function(r){
                if (r){
                    $.post('<?php echo base_url() ?>menu/delete/'+row.id,'',function(result){
                        if (result.success){
                            $('#menu_data').datagrid('reload');    // reload the user data
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                }
            });
        }
    }
   </script>
      