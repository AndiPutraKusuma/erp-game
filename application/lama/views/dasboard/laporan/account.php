<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Kode Akun
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Master Data</a></li>
          <li class="active">Kode Akun</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <p id="judul"></p>
                <table id="laporan" class="easyui-treegrid" style="width:auto;height:450px"
                      url="<?php echo base_url() ?>gl/account_data"
                      toolbar="#toolbar" method="get" idField="id" treeField="code" 
                      rownumbers="true" fitColumns="true" >
                  <thead>
                      <tr>
                          <th field="code" width="50">Akun</th>
                          <th field="name" width="50">Nama</th>
                      </tr>
                  </thead>
                </table>
                <div id="toolbar">
                  <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newAcc()">New Account</a>
                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editComp()">Edit Company</a> -->
                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Company</a> -->
                </div>
                <div id="accForm" class="easyui-dialog" style="width:50%; height:auto; padding: 10px 20px" closed="true" buttons="#dialog-buttons">
                  <form id="form" method="post" novalidate>
                      <div class="form-item">
                          <label for="acc_group" style="font-size: 16px; margin-top: 10px">Account Group</label><br />
                          <input class="easyui-combobox" name="acc_group" data-options="
                            valueField:'coa_number', 
                            textField:'nama', 
                            url:'<?php echo base_url() ?>gl/getGroup', 
                            panelHeight:'auto', 
                            editable:false, 
                            onSelect: function(rec){
                              var url = '<?php echo base_url() ?>gl/listAcc/'+rec.coa_number;
                              $('#acc_parent').combobox('reload', url);
                            }" required="true" style="width:100%;" />
                      </div>
                      <div class="form-item">
                          <label for="acc_parent" style="font-size: 16px; margin-top: 10px">Account Parent</label><br />
                          <input id="acc_parent" class="easyui-combobox" name="acc_parent" data-options="
                            valueField:'acc_code', 
                            textField:'acc_text', 
                            panelHeight:'auto', 
                            editable:false,
                            onSelect: function(rec){
                              $('#acc_code1').textbox('setValue', rec.acc_code);
                            }" required="true" style="width:100%;" />
                      </div>
                      <div class="form-item">
                          <label for="type" style="font-size: 16px; margin-top: 10px">Account Code</label><br />
                          <input id="acc_code1" type="text" name="acc_code1" class="easyui-textbox" required="true" maxlength="50" readonly="true" />
                          <input id="acc_code2" type="text" name="acc_code2" class="easyui-numerbox" required="true" maxlength="50" />
                      </div>
                      <div class="form-item">
                          <label for="type" style="font-size: 16px; margin-top: 10px">Account Name</label><br />
                          <input type="text" name="acc_name" class="easyui-validatebox" required="true" style="width:100%;" maxlength="50" />
                      </div>
                  </form>
                </div>
                 
                <!-- Dialog Button -->
                <div id="dialog-buttons">
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Save</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#accForm').dialog('close')">Cancel</a>
                </div>
               
              <!-- Dialog Button -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">

    function newAcc(){
      $('#accForm').dialog('open').dialog('setTitle','New Account');
      $('#form').form('clear');
      url = '<?php echo base_url() ?>gl/insertAcc';
    }

    function save(){
        jQuery('#form').form('submit',{
            url: url,
            onSubmit: function(){
                return jQuery(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                    jQuery('#accForm').dialog('close');
                    jQuery('#laporan').treegrid('reload');
                    $.messager.alert({
                        title: 'Berhasil',
                        msg: 'Berhasil memasukkan data!',
                        icon: 'info'
                    });
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
        });
    }

  </script>
     