<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aset extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_aset');
	}

	public function index()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$idPemilik= $this->session->userdata('id_retail');
			if($this->session->userdata('game'))
			{
				$idgame = $this->session->userdata('game');
			}
			else
			{
				$idgame = 0;
			}
			$data['isi']=$this->m_aset->list_aset($idPemilik,$idgame);
			//echo $data['isi'];
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/listAsset',$data);

			$this->load->view('dasboard/footer');
		}else{
			redirect('home');
		}
	}
	public function listAset()
	{
		$idPemilik= $this->session->userdata('id_retail');
			if($this->session->userdata('game'))
			{
				$idgame = $this->session->userdata('game');
			}
			else
			{
				$idgame = 0;
			}
		$result = $this->m_aset->list_aset($idPemilik,$idgame);
		if($result)
		{
			$data = json_encode($result);
		}
		else
		{
			$data = json_encode(array());
		}
		echo $data;
	}
	public function addAset()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$idPemilik = $this->session->userdata('id_retail');
			$dataAset = array(
							'asset_name' => $this->input->post('name'),
							'asset_duration' => $this->input->post('duration'),
							'asset_duration_type' => $this->input->post('duration_type'),
							'asset_capacity' => $this->input->post('capacity'),
							'asset_value' => $this->input->post('value'),
							'id_retail' => $idPemilik
						 );
			$query = $this->m_aset->addaset($dataAset);
			// Redirect('Aset');
			if($query){
				echo json_encode(array('success'=>true));
			}
			else {
	    		echo json_encode(array('msg'=>'Gagal memperbarui data.<br>Menu sudah tersedia.'));
	  		}
		}
	}

	public function updateAset()
	{
		$cek=$this->session->userdata('username');
		if($cek)
		{
			$id = $this->input->post('id');
			$data = array(
				'asset_name' => $this->input->post('namaAset'),
				);
			$ok = $this->m_aset->updateAset($data,$id);
			if($ok)
			{
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p>	<i class="icon fa fa-check"></i>'.$ok.' </p></div>');
			}
			else
			{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p>	<i class="icon fa fa-ban"></i>Update data gagal </p></div>');
			}
			redirect("aset");

		}	
	}

	public function delete($id)
	{
		$cek=$this->session->userdata('username');
		if($cek)
		{
			$data = array(
				'is_delete' => 1
				);
			$ok = $this->m_aset->updateAset($data,$id);
			if($ok)
			{
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-check"></i> Hapus data berhasil</p></div>');
				redirect("aset");
			}
			else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-ban"></i>Hapus data gagal </p></div>');
				redirect("aset");
			}
		}
	}


}
