<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_backup');
	}

	public function exportSQL()
	{
		$this->m_backup->Export_DatabaseSQL($this->session->userdata('id_retail'));
	}

	public function exportCSV()
	{
		$this->m_backup->Export_DatabaseCSV();
	}

}
