<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_game');
        $this->load->model('mpenjualan');

	}

	public function index()
	{
		$this->load->model('DefaultMenu');
		$this->DefaultMenu->defaultLayout2();
		$this->load->view('dasboard/game');
		$this->load->view('dasboard/footer');
	}

	public function tes($flag)
	{
		if($flag)
		{
			$this->session->set_userdata('game',1);
			$this->session->set_userdata('tanggal_sekarang',date("m/d/Y H:i:s"));
		}
		else
		{
			$this->session->unset_userdata('game');
		}
		redirect('admin');
	}
	// public function getPrice()
	// {
	// 	$price = $this->m_game->getInfo();
	// 	return $price;
	// }

	public function create()
	{
		$datagame = array(
			'game_name' => $this->input->post('game_name'),
			'game_maxRound' => $this->input->post('game_maxRound'),
			'game_creator' => $this->input->post('game_creator')
			);
		$game_id = $this->m_game->create($datagame);
		redirect('game/view/'.$game_id);
	}
	public function gameStart()
	{
		$tanggal = $this->session->userdata('tanggal');
		$cek = $this->m_game->cek($tanggal);
		if(!$cek)
		{
			$serapan = 250;
			$max = $serapan + (rand(-20,20)*$serapan/100);
			$price = $this->m_game->getInfo($max);

		}
		$this->session->set_userdata('tanggal_sekarang',date("m-d-Y H:i:s"));
		$datetime = new DateTime($this->session->userdata('tanggal'));
		$datetime->add(new DateInterval('P1D'));
		$tanggal =  $datetime->format('Y-m-d');
		$this->session->set_userdata('tanggal',$tanggal);
		echo json_encode(array('success'=>true));
	}

	public function MarketShare()
	{
		// $id =$this->m_game->MarketStart();
		$id =$this->m_game->MarketStartTest();
	

		// foreach ($id as $id_so) 
		// {
		// 	$this->mpenjualan->keluarGudang($id_so);			
		// }
		date_default_timezone_set("Asia/Jakarta");
		$game_round = $this->session->userdata('game_round');
		$to_time = strtotime($this->session->userdata('start_time'));
$from_time = strtotime(date('m-d-Y H:i:s'));
$jarak =  round(abs($to_time - $from_time) / 60,2);
		if($game_round > 30)
		{
			$this->session->unset_userdata('game_round');
			echo json_encode(array('success'=>true));
		}
		else
		{
			$this->session->set_userdata('tanggal_sekarang',date("M d Y H:i:s"));		
			$datetime = new DateTime($this->session->userdata('tanggal'));
			$datetime->add(new DateInterval('P1D'));
			$tanggal =  $datetime->format('Y-m-d');
			$this->session->set_userdata('tanggal',$tanggal);
			$game_round++;
			$this->session->set_userdata('game_round',$game_round);		
			
			echo json_encode(array('success'=>true));
		}
	}



}
