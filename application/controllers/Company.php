<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {

	public function index(){
		$cek=$this->session->userdata('username');
		$this->load->model('m_menu');
		$flag = $this->m_menu->checkAccess($this->session->userdata('roles'));
		if($flag){
			//data header
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			// echo $data['cek'];
			$this->load->view('dasboard/core/company/read');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	public function data(){
		$cek=$this->session->userdata('username');
		if($cek){
			$id = $this->session->userdata('company');
			$this->load->model('m_company');
			$data = $this->m_company->read($id);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	//add data
	public function add()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$data['id_petugas'] = $this->session->userdata('id_petugas');
			$data['company_id'] = $this->session->userdata('company');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			// $this->load->model('mpetugas');
			// $data['role']=$this->mpetugas->list_role();
			// $this->load->model('m_company');
			// $id_company = 0;
			// $data['company']=$this->m_company->read($id_company);
			//
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/core/company/add',$data);
			$this->load->view('dasboard/footer');
		}else{
			redirect('home');
		}
	}

	public function insert(){
		$data = array(
				'company_code' => $this->input->post('company_code'),
				'company_name' => $this->input->post('company_name'),
				'company_address' => $this->input->post('company_address'),
				'creation_user' => $this->session->userdata('id_petugas'),
				'parent_id' => $this->session->userdata('company'),
			);
		$this->load->model('m_company');
		$query=$this->m_company->create($data);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal memasukkan data.<br>Silahkan cek Company Code atau Company Name.'));
  		}
	}

	public function update($id){
		$data = array(
				'company_code' => $this->input->post('company_code'),
				'company_name' => $this->input->post('company_name'),
				'company_address' => $this->input->post('company_address'),
				'creation_user' => $this->session->userdata('id_petugas'),
				'parent_id' => $this->session->userdata('company'),
			);
		$this->load->model('m_company');
		$query=$this->m_company->update($data,$id);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal update data.<br> Terdapat data yang sama'));
  		}
	}

	public function getCompany(){
		$cek=$this->session->userdata('username');
		if($cek){
			$id = $this->session->userdata('company');
			$this->load->model('m_company');
			$data = $this->m_company->getComp($id);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}
}
?>