<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('mproduk');
		$this->load->model('mpembelian');
		$this->load->model('msupplier');

	}

	public $beli = array();
	public $total = array();
	public function index(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			// echo $data['cek'];
			$this->load->view('dasboard/order');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	// public function data(){
	// 	$cek=$this->session->userdata('username');
	// 	if($cek){
	// 		$id = $this->session->userdata('company');
	// 		$this->load->model('m_company');
	// 		$data = $this->m_company->read($id);
	// 		echo json_encode($data);
	// 	}else{

	// 		redirect('home');
	// 	}
	// }

	public function getSupp(){
		$cek=$this->session->userdata('username');
		if($cek){
			// $id = $this->session->userdata('id_petugas');
			$id = '0';
			$data = $this->msupplier->list_supplier($id);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function getItem($supp){
		$cek=$this->session->userdata('username');
		if($cek){
			// $id = $this->session->userdata('id_petugas');
			$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
			$rows = isset($_GET['rows']) ? intval($_GET['rows']) : 10;
			$offset = ($page-1)*$rows;
			$total = $this->mproduk->total_produk_perSuplier($supp); 
			$data = $this->mproduk->list_produk_perSuplier_pagination($supp,$offset,$rows);
			$result["total"] = $total;
			$result["rows"] = $data;
			echo json_encode($result);
			// print_r($data);
		}else{

			redirect('home');
		}
	}

	public function data($item){
		$cek=$this->session->userdata('username');
		if($cek){
				$variabel = explode('-', $item);
				$data = $this->mproduk->findItemPurchasing($variabel);
			// var_dump($data);
			echo json_encode($data);
			// print_r($item);
		}else{
			redirect('home');
		}
	}

	public function add()
	{
		 $collection = $this->input->post('collection');
		 $total = $this->input->post('total');
		 $tanggal = $this->input->post('tanggal');
		 $supplier = $this->input->post('supplier');
		 // $tanggal2 = date_format('Y-mm-dd',$tanggal);
		 echo '<script> console.log('.$supplier.' '.$tanggal.') </script>';
		 $flag = 1;
       	 //$id_po	= uniqid("PO");
$id_pemilik = $this->session->userdata('id_retail');
       	 $query = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_po),16),4,'0') AS maxi FROM purchasing WHERE id_pemilik = ".$id_pemilik." and game_id = ".$this->session->userdata('game_id')." and tanggal_po = '".$tanggal."'");
		                $query2 = $query->result();
		                $tes = $query2[0]->maxi;
		                $index = '0';
		                if(!isset($tes))
		                {
		                    $index =  '0001';
		                }
		                else
		                {
		                    // $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_item),4)+1,4,'0') AS sem FROM item_master WHERE id_pemilik = '$id_pemilik' ");
		                    $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_po),16,4)+1,4,'0') AS sem FROM purchasing WHERE id_pemilik = ".$id_pemilik." and game_id = ".$this->session->userdata('game_id')." and tanggal_po = '".$tanggal."'");
		                    $temp2 = $temp->result();
		                    $index = $temp2[0]->sem;
		                }
		                //echo $index.' ';
		                $tanggalA = str_replace("/", "", $tanggal);   
		                $id_po = 'PO'.$this->session->userdata('game_id').$id_pemilik.$tanggalA.$index;

		 foreach ($collection as $data) 
		 {
		 	$arr_detail = array(
		 		'id_po' =>$id_po,
		 		'id_barang' => $data['barcode'],
		 		'jumlah'=> $data['quantity'],
		 		'harga' => $data['price'],
				'game_id' => $this->session->userdata('game_id'),
		 		);
		 	$hehe = $this->mpembelian->addPembelian($arr_detail,$supplier);
		 	if(!$hehe)
		 	{
		 		break;
		 		$flag = 0;
		 	}
		 }
		 if($flag)
		 {
		 	$date=date_create($tanggal);
			$tanggalbaru =  date_format($date,"Y-m-d");
			 $head = array(
			 		'id_po' =>$id_po,
			 		'id_petugas' => $this->session->userdata('id_retail'),
			 		'id_suplier'=> $supplier,
			 		'tanggal_po' => $tanggalbaru,
			 		'totalHarga' => $total,
			 		'id_pemilik' => $this->session->userdata('id_retail'),
			 		'ongkir' => 5000,
					'game_id' => $this->session->userdata('game_id'),
			 		);
			 $ok2 =  $this->mpembelian->purchasing_header($head);
			 if($ok2)
			 {
		 		echo true;
			 }
		 }
      // if(is_array($collection)) {
      //      print_r($collection);
      //  }
      //  else
      //  {
      //      var_dump($collection);
      //  }
		// echo json_encode(array('succes'=>true,'response'=>$data));
	}

	// public function add()
	// {
	// 	$data = $this->input->post('collection');
	// 	$param = array();
 //        $id_so	= uniqid("SO");
	// 	foreach ($data as $dataItem) 
	// 	{
	// 		array_push(
 //                    $param,
 //                    array(
 //                    	'id_so' =>$id_so,
 //                        'id_barang' => $dataItem['barcode'],
	// 					'butuh' => $dataItem['quantity'],
 //                    )
 //            ); 
	// 	}
	// 	foreach ($param as $dataaa) 
	// 	{
	// 		$hehe = $this->mpenjualan->GetItem($dataaa);	
	// 	}
	// }
}
?>