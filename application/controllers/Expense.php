<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_expense');
		$this->load->model('m_menu');

	}

	public function listExpense()
	{
		$data = array(
			'id_pemilik'=> $this->session->userdata('id_retail'),
			'game_id'=> $this->session->userdata('game_id'),

			);
		$list = $this->m_expense->listExpense($data);
		// print_r($list);
		//return $list;
		echo json_encode($list);
	}

	public function addExpense()
	{
		if($this->input->post('expense_jumlah'))
		{
			$jumlah = $this->input->post('expense_jumlah');
		}
		else
		{
			$jumlah = 1;
		}
		$data = array(
            'expense_code' => $this->input->post('expense_code'),
            'expense_jumlah' => $jumlah,
            'expense_value' => $this->input->post('expense_value') ,
            'id_pemilik' => $this->input->post('id_pemilik') ,
            'game_id' => $this->input->post('game_id') ,
            // 'game_id' => $this->session->userdata('game_id'),
            );
		$query = $this->m_expense->addExpense($data);
		//redirect('Expense/listExpense');
		if($query)
		{
			echo json_encode(array('success'=>true));
		}
		else {
    		echo json_encode(array('msg'=>'Gagal memasukkan data.<br>data expense sudah tersedia.'));
  		}
	}

	public function update()
	{
		// if($this->input->post('expense_jumlah'))
		// {
		// 	$jumlah = $this->input->post('expense_jumlah');
		// }
		// else
		// {
		// 	$jumlah = 1;
		// }

		/*$dataiklan = array(
			'expense_code' => 1,
			'expense_jumlah' => 1;
			'expense_value' => $this->input->post('iklan');
			'id_pemilik' => $this->session->userdata('id_retail') ,
            'game_id' => $this->session->userdata('game_id') ,            
			);
		$datagaji = array(
			'expense_code' => 2,
			'expense_jumlah' => $this->input->post('jumlah_pekerja');
			'expense_value' => $this->input->post('gaji_pekerja');
			'id_pemilik' => $this->session->userdata('id_retail') ,
            'game_id' => $this->session->userdata('game_id') , 
			);
		$dataair = array(
			'expense_code' => 3,
			'expense_jumlah' => 1;
			'expense_value' => $this->input->post('air');
			'id_pemilik' => $this->session->userdata('id_retail') ,
            'game_id' => $this->session->userdata('game_id') , 
			);
		$datalistrik = array(
			'expense_code' => 4,
			'expense_jumlah' => 1;
			'expense_value' => $this->input->post('listrik');
			'id_pemilik' => $this->session->userdata('id_retail') ,
            'game_id' => $this->session->userdata('game_id') , 
			);
		$dataasuransi = array(
			'expense_code' => 5,
			'expense_jumlah' => 1;
			'expense_value' => $this->input->post('asuransi');
			'id_pemilik' => $this->session->userdata('id_retail') ,
            'game_id' => $this->session->userdata('game_id') , 
			);
		$datadepresiasi = array(
			'expense_code' => 6,
			'expense_jumlah' => 1;
			'expense_value' => $this->input->post('depresiasi');
			'id_pemilik' => $this->session->userdata('id_retail') ,
            'game_id' => $this->session->userdata('game_id') , 
			);
*/
		$id_pemilik = $this->session->userdata('id_retail');
		$game_id = $this->session->userdata('game_id');
		// $id_pemilik = 1;
		// $game_id = 4;


		$data = array();
		array_push(
                $data,
                array(
					'expense_code' => 1,
					'expense_jumlah' => 1,
					'expense_value' => $this->input->post('iklan'),
					'id_pemilik' => $id_pemilik ,
		            'game_id' => $game_id ,            
					),
                array(
					'expense_code' => 2,
					'expense_jumlah' => $this->input->post('jumlah_pekerja'),
					'expense_value' => $this->input->post('gaji_pekerja'),
					'id_pemilik' => $id_pemilik ,
		            'game_id' => $game_id , 
					),
                array(
					'expense_code' => 3,
					'expense_jumlah' => 1,
					'expense_value' => $this->input->post('air'),
					'id_pemilik' => $id_pemilik ,
		            'game_id' => $game_id , 
					),
                array(
					'expense_code' => 4,
					'expense_jumlah' => 1,
					'expense_value' => $this->input->post('listrik'),
					'id_pemilik' => $id_pemilik ,
		            'game_id' => $game_id , 
					),
                array(
					'expense_code' => 5,
					'expense_jumlah' => 1,
					'expense_value' => $this->input->post('asuransi'),
					'id_pemilik' => $id_pemilik ,
		            'game_id' => $game_id , 
					),
                array(
					'expense_code' => 6,
					'expense_jumlah' => 1,
					'expense_value' => $this->input->post('depresiasi'),
					'id_pemilik' => $id_pemilik ,
		            'game_id' => $game_id , 
					),
                array(
					'expense_code' => 7,
					'expense_jumlah' => 1,
					'expense_value' => $this->input->post('amortisasi'),
					'id_pemilik' => $id_pemilik ,
		            'game_id' => $game_id , 
					)



        );
		$query = $this->m_expense->update($data);
		// $query = $this->m_expense->updateExpense($data);
		// redirect('Expense/listExpense');	
		if($query)
		{
			echo json_encode(array('success'=>true));
		}
		else {
    		echo json_encode(array('msg'=>'Gagal memperbarui data.'));
  		}
	}

	public function delete()
	{
		$data = array(
            'expense_id' => $this->input->post('expense_id'),
            'is_delete' => 1 ,            
            // 'game_id' => $this->session->userdata('game_id'),
            );
		$query = $this->m_expense->updateExpense($data);
		//redirect('Expense/listExpense');
		if($query)
		{
			echo json_encode(array('success'=>true));
		}
		else {
    		echo json_encode(array('msg'=>'Gagal memnghapus data.'));
  		}	
	}
}
?>
