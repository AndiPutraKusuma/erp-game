<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bi extends CI_Controller {

	// public function index(){
	// 	$cek=$this->session->userdata('username');
	// 	if ($cek) {
	// 		$idPemilik=$this->session->userdata('id_retail');
	// 		$this->load->model('mgl');
	// 		$data = $this->mgl->tato($idPemilik);
	// 		var_dump($data);
	// 	}
	// }

	public function index()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$idPemilik= $this->session->userdata('id_retail');
			if($this->session->userdata('game'))
			{
				$idgame = $this->session->userdata('game');
			}
			else
			{
				$idgame = 0;
			}
			// $data['isi']=$this->m_aset->list_aset($idPemilik,$idgame);
			//echo $data['isi'];
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/bobotSupplier');
			$this->load->view('dasboard/footer');
		}else{
			redirect('home');
		}
	}

	public function dashboard()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$idPemilik= $this->session->userdata('id_retail');
			if($this->session->userdata('game'))
			{
				$idgame = $this->session->userdata('game');
			}
			else
			{
				$idgame = 0;
			}
			$this->load->model('mgl');
			$this->load->model('mbi');
			$data['profit']=$this->mbi->chart_data();
			$data['revenue']=$this->mbi->chart_data();
			$data['roa']=$this->mbi->roa($idPemilik);
			$data['roi']=$this->mbi->roi($idPemilik);
			$data['roe']=$this->mbi->roe($idPemilik);
			$data['dapat']=$this->mbi->revenue($idPemilik);
			$data['dapat2']=$this->mbi->revenue_bulan($idPemilik);
			$data['expenses']=$this->mbi->expenses($idPemilik);
			$data['margin']=$this->mbi->avg_real_mc();
			$data['hpp']=$this->mbi->hpp($idPemilik);
			$data['hpp2']=$this->mbi->hpp_bulan($idPemilik);
			$data['ped']=$this->mbi->avg_ped($idPemilik);
			$data['iklan']=$this->mbi->iklan_bulan($idPemilik);
			$data['liability']=$this->mbi->liability($idPemilik);
			// $this->load->model('mbi');
			// $data['isi']=$this->mbi->trendBulanan();
			//echo $data['isi'];
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/adminbi',$data);
			$this->load->view('dasboard/footer');
		}else{
			redirect('home');
		}
	}

	public function insertBiSupp()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$id = $this->session->userdata('company');
			$this->load->model('mbi');
			$data = $this->mbi->insert_data();
			echo json_encode($data);
		}else{
			redirect('home');
		}
	}

	public function biforecast()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$uid = $this->session->userdata('id_retail');
			// $game_id = $this->session->userdata('game_id');
			$game_id = 15;
			$this->load->model('mbi');
			$data = $this->mbi->item();
			foreach ($data as $item) {
				// echo $item->id_item;
				$command = 'python /var/www/html/erp-game/assets/test.py '.$uid.' '.$game_id.' '.$item->id_item;
				// $command2 = (string)$command;
				//$command = 'python /var/www/html/erp-game/assets/hello.py';
				// var_dump($uid);
				// var_dump($game_id);
				$output = shell_exec($command);
				echo $output;
				// $forecast = system("python , $retval);
				// var_dump($retval);
			}
			return true;

			// echo json_encode($data);
		}else{
			redirect('home');
		}
	}

	public function data(){
		$cek=$this->session->userdata('username');
		if($cek){
			$id = $this->session->userdata('company');
			$this->load->model('mproduk');
			$data = $this->mproduk->stockItem(1,50,$id);
			echo $data;
		}else{

			redirect('home');
		}
	}

	//add data
	public function supplier_rule_data(){
		$cek=$this->session->userdata('username');
		if($cek){
			$idPemilik=$this->session->userdata('id_retail');
			$this->load->model('mproduk');
			$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
			$rows = isset($_GET['rows']) ? intval($_GET['rows']) : 5;
			// $rows = 5;
			$offset = ($page-1)*$rows;
			$total = $this->mproduk->countstock($idPemilik);
			$this->load->model('mbi');
			$data = $this->mbi->supplier_rule($offset,$rows,$idPemilik);
			$result["total"] = $total;
			$result["rows"] = $data;
			echo json_encode($result);
		}else{
			redirect('home');
		}
	}

	public function insert($item){
		$this->load->model('mbi');
		$query=$this->mbi->create_rop($item);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal memasukkan data ROP.'));
  		}
	}

	public function awak(){
		$this->load->model('mbi');
		$uid = $this->session->userdata('id_retail');
		// echo $uid;
		$query=$this->mbi->searchHargaMax('1200011121');
		$query2=$this->mbi->searchHargaMin('1200011121');
		$query3=$this->mbi->real_mc('1200011121');
		// $query=$this->mbi->profit();
		// if($query){
			// echo json_encode(array('success'=>true));
			// echo $this->session->userdata('game_id');
			print_r($query);
			print_r($query2);
			print_r($query3);
		// }else {
    		// echo json_encode(array('msg'=>'Gagal update data.<br> Terdapat data yang sama'));
  		// }
	}

	public function insert_ped(){
		$this->load->model('mbi');
		// $data = array(
		// 	'id_item' => '1400071121',
		// 	'price' => 7000,
		// 	'quantity' => 100,
		// );
		$uid=$this->session->userdata('id_retail');
		// $query=$this->mbi->ped($uid, $data);
		$query=$this->mbi->ins_ped();
		if($query){
			// echo json_encode(array('success'=>true));
			print_r($query);
		}else {
    		echo json_encode(array('msg'=>'Gagal update data.<br> Terdapat data yang sama'));
  		}
	}

	public function update(){
		$this->load->model('mbi');
		$query=$this->mbi->update();
		if($query){
			// echo json_encode(array('success'=>true));
			print_r($query);
		}else {
    		echo json_encode(array('msg'=>'Gagal update data.<br> Terdapat data yang sama'));
  		}
	}

	public function update2(){
		$this->load->model('mbi');
		$query=$this->mbi->update2();
		if($query){
			// echo json_encode(array('success'=>true));
			print_r($query);
		}else {
    		echo json_encode(array('msg'=>'Gagal update data.<br> Terdapat data yang sama'));
  		}
	}

	public function getCompany(){
		$cek=$this->session->userdata('username');
		if($cek){
			$id = $this->session->userdata('company');
			$this->load->model('m_company');
			$data = $this->m_company->getComp($id);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function trendBarangHari()
    {
    	$cek=$this->session->userdata('username');
		if($cek){
			$idPemilik= $this->session->userdata('id_retail');
			if($this->session->userdata('game'))
			{
				$idgame = $this->session->userdata('game');
			}
			else
			{
				$idgame = 0;
			}	
			// $this->load->model('mbi');
			// $data['isi']=$this->mbi->trendBulanan();
			//echo $data['isi'];
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/listTrendHari');
			$this->load->view('dasboard/footer');
		}else{
			redirect('home');
		}
    }

    public function trendBarangHari_data(){
		$cek=$this->session->userdata('username');
		if($cek){
			$tanggal = isset($_GET['tanggal']) ? ($_GET['tanggal']) : NULL;
			$order = isset($_GET['order']) ? ($_GET['order']) : 'desc';
			$sort = isset($_GET['sort']) ? ($_GET['sort']) : 'total';
			// $tanggal = date('-m-d',$tanggal);
			$date = DateTime::createFromFormat('m/d/Y', $tanggal);
			$date2 =  $date->format('Y-m-d');
			// ?$tanggal2 = str_replace('/', '-', $date2);
			$this->load->model('mbi');
			$data = $this->mbi->trendHarian($date2, $order, $sort);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function list_data_revenue(){
		$cek=$this->session->userdata('username');
		if($cek){
			$sort = 'dapat';
			// $tanggal = date('-m-d',$tanggal);
			$this->load->model('mbi');
			$data = $this->mbi->list_dashboard_data($sort);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function list_data_profit(){
		$cek=$this->session->userdata('username');
		if($cek){
			$sort = 'profit';
			// $tanggal = date('-m-d',$tanggal);
			$this->load->model('mbi');
			$data = $this->mbi->list_dashboard_data($sort);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function trendBarangBulan()
    {
    	$cek=$this->session->userdata('username');
		if($cek){
			$idPemilik= $this->session->userdata('id_retail');
			if($this->session->userdata('game'))
			{
				$idgame = $this->session->userdata('game');
			}
			else
			{
				$idgame = 0;
			}	
			// $this->load->model('mbi');
			// $data['isi']=$this->mbi->trendBulanan();
			//echo $data['isi'];
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/listTrendBulan');
			$this->load->view('dasboard/footer');
		}else{
			redirect('home');
		}
    }

    public function trendBarangBulan_data(){
		$cek=$this->session->userdata('username');
		if($cek){
			$tanggal = isset($_GET['tanggal']) ? ($_GET['tanggal']) : NULL;
			$order = isset($_GET['order']) ? ($_GET['order']) : 'desc';
			$sort = isset($_GET['sort']) ? ($_GET['sort']) : 'total';
			// $tanggal = date('-m-d',$tanggal);
			$date = DateTime::createFromFormat('Y-m', $tanggal);
			$date2 =  $date->format('Y-m-d');
			// ?$tanggal2 = str_replace('/', '-', $date2);
			$this->load->model('mbi');
			$data = $this->mbi->trendBulanan($date2, $order, $sort);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function trendPerBarang()
    {
    	$cek=$this->session->userdata('username');
		if($cek){
			$idPemilik= $this->session->userdata('id_retail');
			if($this->session->userdata('game'))
			{
				$idgame = $this->session->userdata('game');
			}
			else
			{
				$idgame = 0;
			}	
			// $this->load->model('mbi');
			// $data['isi']=$this->mbi->trendBulanan();
			//echo $data['isi'];
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/listTrendBarang');
			$this->load->view('dasboard/footer');
		}else{
			redirect('home');
		}
    }

    public function trendPerBarang_data(){
		$cek=$this->session->userdata('username');
		if($cek){
			$tanggal = isset($_GET['tanggal']) ? ($_GET['tanggal']) : NULL;
			$id_item = isset($_GET['id_item']) ? ($_GET['id_item']) : NULl;
			$order = isset($_GET['order']) ? ($_GET['order']) : 'desc';
			$sort = isset($_GET['sort']) ? ($_GET['sort']) : 'total';
			// $tanggal = date('-m-d',$tanggal);
			$date = DateTime::createFromFormat('Y-m', $tanggal);
			$date2 =  $date->format('Y-m-d');
			// ?$tanggal2 = str_replace('/', '-', $date2);
			$this->load->model('mbi');
			$data = $this->mbi->trendBarang($date2, $id_item, $order, $sort);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function getItem_data(){
		$cek=$this->session->userdata('username');
		$uid = $this->session->userdata('id_retail');
		if($cek){
			$page = isset($_GET['page']) ? ($_GET['page']) : 1;
			$rows = isset($_GET['rows']) ? ($_GET['rows']) : 10;
			// $order = isset($_GET['order']) ? ($_GET['order']) : 'desc';
			// $sort = isset($_GET['sort']) ? ($_GET['sort']) : 'total';
			// $tanggal = date('-m-d',$tanggal);
			// $date = DateTime::createFromFormat('Y-m', $tanggal);
			// $date2 =  $date->format('Y-m-d');
			// ?$tanggal2 = str_replace('/', '-', $date2);
			$this->load->model('mproduk');
			$total = $this->mproduk->countstock($uid);
			$data = $this->mproduk->stockItemAll($page, $rows, $uid);
			$result['total'] = $total;
			$result['rows'] = $data;
			echo json_encode($result);
		}else{

			redirect('home');
		}
	}
}
?>