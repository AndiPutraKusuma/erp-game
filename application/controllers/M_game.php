<?php 
// ini_set('max_execution_time', 0); 
// ini_set('memory_limit','4096M');
// ini_set('memory_limit','2048M');
set_time_limit(90000000); // 15 minutes

class M_game extends CI_Model {

    public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
    public function searchGame()
    {
        $this->db->reconnect();
        $this->db->Select('game_detail.game_id');
        $this->db->where('game_player',$this->session->userdata('id_retail'));
        $this->db->from('game_detail');
        $this->db->join('game','game.game_id = game_detail.game_id and game.game_status = 1');
        $read = $this->db->get();
        foreach ($read->result() as $data) 
        {
            $game_id = $data->game_id;
        }
        return $game_id;
    }
    public function searchRound()
    {
        $this->db->reconnect();
        $this->db->Select('game_round');
        $this->db->where('game_id',$this->session->userdata('game_id'));
        $read = $this->db->get('game');
        foreach ($read->result() as $data) 
        {
            $game_round = $data->game_round;
        }
        return $game_round;
    }
    public function searchtimeStart()
    {
        $this->db->reconnect();
        $this->db->Select('game_start_time');
        $this->db->where('game_id',$this->session->userdata('game_id'));
        $read = $this->db->get('game');
        foreach ($read->result() as $data) 
        {
            $game_round = $data->game_start_time;
        }
        return $game_round;
    }
    
    public function searchUser($game_id)
    {
        $this->db->reconnect();
        $this->db->select('game_player');
        $this->db->where('game_id',$game_id);
        $read = $this->db->get('game_detail');        
        foreach ($read->result() as $data) 
            {
                $user[] = $data->game_player;
            }
        return $user;
    }

    public function getStock($id_barang,$pemilik)
    {
        // print_r($id_barang.' '.$pemilik);
        $stock = array();
        $this->db->reconnect();
        $this->db->select('sum(jumlah) as stock');
        $this->db->where('id_item',$id_barang);
        $this->db->where('id_pemilik',$pemilik);
         // 18:35:49
        $this->db->where("DATE(tanggal_masuk) <= '".$this->session->userdata('tanggal')."'");
       $this->db->where('game_id',$this->session->userdata('game_id'));
         //$this->db->where("DATE(tanggal_masuk) <= '2018-07-22'");
         //$this->db->where('game_id',11);
        $read = $this->db->get('gudang');
        if($read->num_rows())
        {
            foreach ($read->result() as $data) 
            {
                // array_push(
                //         $stock,
                //         array(
                //             'stock'=>$data->stock,
                //         )
                // ); 
                if ($data->stock < 1)
                {
                    $stock = 0;
                }
                else
                {
                    $stock = $data->stock;
                }

            }
        }
        else
        {
            // array_push(
            //             $stock,
            //             array(
            //                 'stock'=>0,
            //                 // 'supplier_id'=>$data->supplier_id,
            //             )
            //     ); 
            $stock = 0;
        }
        // echo ' rows :'.$read->num_rows().' stock : '.$stock.'<br>'; 
        // print_r($stock);
        return $stock;
    }
      
    // public function getInfo($max)
    // {
    //     $game_id = $this->session->userdata('company');
    //     $idPet=$this->session->userdata('id_retail');
    //     $homina = array();
    //     $this->db->reconnect();
    //     $this->db->select('id_petugas');
    //     $this->db->where('game_id',$game_id);
    //     $this->db->where('privilege = 17');
    //     $read = $this->db->get('petugas');
    //     foreach ($read->result() as $data) 
    //     {
    //         $users[] = $data->id_petugas;
    //     }
    //     // print_r($users);
    //     // $this->db->select('id_item , item_harga , item_marketing ,id_pemilik');
    //     // $this->db->distinct('id_item');
    //     $temp = array();
    //     $this->db->select('tipe');
    //     $this->db->where_in('id_pemilik',$users);
    //     $readItem = $this->db->get('item_master');
    //     foreach ($readItem->result() as $datacek) 
    //     {
    //         if(!in_array($datacek->tipe, $temp))
    //         {
    //             $temp[] = $datacek->tipe; 
    //         }
    //     }
    //     foreach ($temp as $data2) 
    //     {   
    //         $this->db->select('sum(item_harga) as maxHarga , sum(item_marketing) as maxMarket');
    //         $this->db->where('tipe ',$data2);
    //         $this->db->where_in('id_pemilik',$users);
    //         $total = $this->db->get('item_master');
    //         foreach ($total->result() as $data) 
    //         {
    //             $totalHarga = $data->maxHarga;
    //             $totalMarket = $data->maxMarket;
    //         }
    //         $this->db->select('id_item, item_harga , item_marketing ,id_pemilik');
    //         $this->db->where('tipe ',$data2);
    //         $this->db->where_in('id_pemilik',$users);
    //         // $this->db->group_by('tipe');
    //         $readUser = $this->db->get('item_master');
    //         foreach ($readUser->result() as $data3) 
    //         {
    //             $id_so = uniqid("SO");
    //             $serapHarga = ($totalHarga - $data3->item_harga ) / $totalHarga;
    //             // $serapHarga = $data3->item_harga / $totalHarga;
    //             $serapMarket = $data3->item_marketing / $totalMarket;
    //             $serapan = floor(((0.7 * $serapHarga) + (0.3 * $serapMarket))*$max);
    //             $total = $data3->item_harga * $serapan;
    //             if($serapan > 0)
    //             {
    //                 $cekstock = $this->getStock($data3->id_item,$data3->id_pemilik);
    //                 $this->db->trans_start();
    //                 // var_dump($cekstock);
    //                 if($cekstock[0]['stock'] > 0)
    //                 {
    //                     echo $cekstock[0]['stock'].' ';
    //                     foreach ($cekstock as $param) 
    //                     {
    //                         if($serapan <= $param['stock'])
    //                         {
    //                             $dataDetail = array(
    //                                 'id_so' => $id_so,
    //                                 'id_item' => $data3->id_item,
    //                                 'jumlah' => $serapan,
    //                                 'harga' => $data3->item_harga,
    //                                 'id_suplier' =>$param['supplier_id'],
    //                                 'id_pemilik' =>$data3->id_pemilik,
    //                             );
    //                             $stock = $param['stock'] - $serapan;       
    //                         }
    //                         else
    //                         {
    //                             $dataDetail = array(
    //                                 'id_so' => $id_so,
    //                                 'id_item' => $data3->id_item,
    //                                 'jumlah' => $param['stock'],
    //                                 'harga' => $data3->item_harga,
    //                                 'id_suplier' =>$param['supplier_id'],
    //                                 'id_pemilik' =>$data3->id_pemilik,
    //                             );
    //                             $serapan = $serapan - $param['stock'];
    //                             $stock = 0;

    //                         }
    //                         $ok2 = $this->db->insert('detail_penjualan',$dataDetail);
    //                         if($ok2)
    //                         {
    //                             $update = array(
    //                             'stock' => $stock,
    //                             'last_mod_user' => $data3->id_pemilik,
    //                             'last_mod_time' => $this->session->userdata('tanggal'),
    //                             );
    //                             $this->db->where('id_barang',$data3->id_item);
    //                             $this->db->where('supplier_id',$param['supplier_id']);
    //                             $this->db->where('id_retail',$data3->id_pemilik);
    //                             $this->db->update('item_stock',$update);
    //                         }
    //                         if($total == 0)
    //                         {
    //                             break;
    //                         }
    //                     }
    //                     if($ok2)
    //                     {
    //                         $hasil = array(
    //                                     'id_so' => $id_so, 
    //                                     'id_petugas' => $data3->id_pemilik,
    //                                     'id_customer' => 1,
    //                                     'tanggal' =>  $this->session->userdata('tanggal'),
    //                                     'total' => $total ,
    //                                     'kurir' => 1,
    //                                     'status' => 0,
    //                                     'tanggal_keluar' => $this->session->userdata('tanggal'),
    //                                     'id_pemilik' => $data3->id_pemilik,
    //                                 );
                           
    //                         $ok = $this->db->insert('penjualan',$hasil);
    //                         if ($ok)
    //                         {
    //                             $this->db->trans_commit();
    //                         }
    //                         else
    //                         {
    //                             $this->db->trans_rollback();
    //                         }
    //                     }
    //                     else
    //                     {
    //                         $this->db->trans_rollback();
    //                     }
    //                 }
    //                 // else
    //                 // {
    //                 //     echo 'stock kosong';
    //                 // }
    //             }
    //             else
    //             {
    //                 echo 'total 0';
    //             }
    //         }
    //     }
    //             // if($ok)
    //             // {
    //             //     $dataDetail = array(
    //             //             'id_so' => $id_so,
    //             //             'id_item' => $data3->id_item,
    //             //             'jumlah' => $serapan,
    //             //             'harga' => $data3->item_harga,
    //             //             'id_suplier' =>1,
    //             //             'id_pemilik' =>$data3->id_pemilik,
    //             //         );
    //             //     $ok2 = $this->db->insert('detail_penjualan',$dataDetail);
    //             //     if($ok2)
    //             //     {
    //             //         $this->db->trans_commit();
    //             //     }
    //             //     else
    //             //     {
    //             //         $this->db->trans_rollback();
    //             //         break;
    //             //     }
    //             // }
    //             // else
    //             // {
    //             //     $this->db->trans_rollback();
    //             //     break;
    //             // }
    //     //     }
    //     // }
    //     return true;
    // }
    public function MarketStart()
    {
        // $id_sos = array();
        $game_id = $this->session->userdata('game_id');
        // $game_id = 1;
        // $idPet=$this->session->userdata('id_retail');
        $homina = array();
        $this->db->reconnect();

        $this->db->where('game_id',$this->session->userdata('game_id'));
        $this->db->where('id_pemilik',$this->session->userdata('id_retail'));
        $read = $this->db->get('market_share');
        if(!$read->num_rows())
        {  
//           echo '1 rows'.$read->num_rows(); 
            $this->db->select('game_player');
            $this->db->where('game_id',$game_id);
            // $this->db->where('privilege = 17');
            $read = $this->db->get('game_detail');
            $jumlah_player = 0;
            foreach ($read->result() as $data) 
            {
                $users[] = $data->game_player;
                $jumlah_player++;
            }
            $temp = array();
            // print_r($users);
            $marketShare = array();
            $this->db->select('id_item');
            // $this->db->where_in('id_pemilik',$users);
            $readItem = $this->db->get('item_master');
            foreach ($readItem->result() as $datacek) 
            {
                    $temp[] = $datacek->id_item;
            }
            $ambil = mt_rand(5,25);
            // echo 'ambil : '.$ambil.'<br>';
            for ($i=0; $i < $ambil ; $i++) 
            { 
                $index = mt_rand(0,29);
                // echo 'index : '.$index.' ';
                $random[] = $temp[$index];
            }
            // echo '<br>';
            // print_r($temp);
            // echo '<br><br>';
            // print_r($random);
            // echo 'start market share :<br>';
            foreach ($random as $data2) 
            {  
                // echo 'item : '.$data2.'<br>';
                $this->db->select('sum(item_price) as maxHarga , sum(item_marketing) as maxMarket');
                $this->db->where('id_item ',$data2);
                $this->db->where('game_id',$game_id);
                $this->db->where_in('game_player',$users);
                $readread = $this->db->get('player_detail');
                foreach ($readread->result() as $data) 
                {
                    $totalHarga = $data->maxHarga;
                    $totalMarket = $data->maxMarket;
                }
               // echo 'totalHarga : '.$totalHarga. ' totalMarket : '.$totalMarket.'<br>';
                $this->db->select('max_serapan');
                $this->db->where('id_item',$data2);
                $read = $this->db->get('item_master');
                foreach ($read->result() as $data) 
                {
                   $tmp = $data->max_serapan *$jumlah_player;
                   $max = floor(rand(-20,20)*$tmp/100) + $tmp;

                }
                // echo 'id_item : '.$data2.' serapan : '.$max.'<br>';

                // buat normalisasi
                $this->db->select('id_item, item_price , item_marketing ,game_player as id_pemilik');
                $this->db->where('id_item ',$data2);
                $this->db->where_in('game_player',$users);            
                $readUser2 = $this->db->get('player_detail');
                $normalisasi = 0;
                foreach ($readUser2->result() as $normal) 
                {
                    $serapHarga = ($totalHarga - $normal->item_price ) / $totalHarga;
                    // $serapHarga = $data3->item_harga / $totalHarga;
                    $serapMarket = $normal->item_marketing / $totalMarket;
                    $normalisasi = $normalisasi + ((0.7 * $serapHarga) + (0.3 * $serapMarket));
                }
                // echo $max.' '.$data2.'<br>';
                $this->db->select('id_item, item_price , item_marketing ,game_player as id_pemilik');
                $this->db->where('id_item ',$data2);
                $this->db->where('game_id',$game_id);
                $this->db->where_in('game_player',$users);
                // $this->db->group_by('tipe');
                $readUser = $this->db->get('player_detail');
                $total =0;
                foreach ($readUser->result() as $data3) 
                {
                    //ambil max serapan
                    $id_so = uniqid("SO");
                    $serapHarga = ($totalHarga - $data3->item_price ) / $totalHarga;
                    $serapMarket = $data3->item_marketing / $totalMarket;
                    $tes = ((0.7 * $serapHarga) + (0.3 * $serapMarket))/$normalisasi;
                    $serapan = floor($tes*$max);

                    $total = $total + ($data3->item_price * $serapan);
                    if($serapan > 0)
                    { 
                        $cekstock = $this->getStock($data3->id_item,$data3->id_pemilik);
                        //echo 'item : '.$data3->id_item.' stock : '.$cekstock.' pemilik : '.$data3->id_pemilik.'<br>';
                        if($cekstock > 0)
                        {
                            $datagudang = array();
                            $this->db->select('barcode_barang , jumlah,id_supplier,id_pemilik');
                            $this->db->where('id_pemilik ',$data3->id_pemilik);
                            $this->db->where('jumlah > 0');
                            $this->db->where('id_item',$data3->id_item);
                            $read2 = $this->db->get('gudang');
                            {
                                foreach ($read2->result() as $data2) 
                                {
                                    $datagudang[] = $data2;
                                }
                            }
                            foreach ($datagudang as $datadata) 
                            {   //coment if ini
                                if($data3->id_pemilik = $this->session->userdata('id_retail'))
                                {
                                    if($serapan <= $datadata->jumlah)
                                    {
                                        //array_push(
                                          //      $marketShare,
                                            $marketshaaare = array(
                                                    // 'id_so' => $id_so,
                                                    'id_item' => $data3->id_item,
                                                    'barcode_barang' => $datadata->barcode_barang,
                                                    'jumlah' => $serapan,
                                                    'harga' => $data3->item_price,
                                                    'id_suplier' =>$datadata->id_supplier,
                                                    'id_pemilik' =>$data3->id_pemilik,
                                                    'game_id'=> $game_id,
                                                    'stock' => $datadata->jumlah - $serapan,
                                                );
                                        // );
                                            $this->db->insert('market_share',$marketshaaare);
                                            // print_r($marketshaaare);
                                            // $stock = $datadata->jumlah - $serapan;
                                            $serapan = 0; 
                                            break;                                              
                                        // echo 'barcode_barang :'.$datadata->barcode_barang.'  jumlah : '.$serapan.' pemilik : '.$data3->id_pemilik.'<br>';
                                    }
                                    else
                                    {
                                        // array_push(
                                        //         $marketShare,
                                        $marketshaaare  =  array(
                                                    // 'id_so' => $id_so,
                                                    'id_item' => $data3->id_item,
                                                    'barcode_barang' => $datadata->barcode_barang,
                                                    'jumlah' => $datadata->jumlah,
                                                    'harga' => $data3->item_price,
                                                    'id_suplier' =>$datadata->id_supplier,                                        
                                                    'id_pemilik' =>$data3->id_pemilik,
                                                    'game_id'=> $game_id,
                                                    'stock' => 0,
                                                );
                                        // );
                                        $serapan = $serapan - $datadata->jumlah;
                                        // echo 'barcode_barang :'.$datadata->barcode_barang.' jumlah : '.$datadata->jumlah.' pemilik : '.$data3->id_pemilik.'<br>';
                                    $this->db->insert('market_share',$marketshaaare);

                                    }
                                } // sama sini
                                // echo '<br>';
                                // echo '<br>';
                            }
                        }
                        
                    }
                    
                }
            // echo '-- ganti item -- <br><br>';
            }
        }
        // echo '<br>marketShare : <br>';
       // print_r($marketShare);
        $pemilik = $this->session->userdata('id_retail');
        $this->db->where('game_id',$game_id);
        $this->db->where('id_pemilik',$this->session->userdata('id_retail'));
        $this->db->order_by("id_pemilik", "asc"); 
        $ok = $this->db->get('market_share');
        $total = 0;
        //$id_so = uniqid("SO");
$query = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_so),16),4,'0') AS maxi FROM penjualan WHERE id_pemilik = ".$this->session->userdata('id_retail')." and game_id = ".$this->session->userdata('game_id')." and tanggal = '".$this->session->userdata('tanggal')."'");

        $query2 = $query->result();
        $tes = $query2[0]->maxi;
        $index = '0';
        if(!isset($tes))
        {
            $index =  '0001';
        }
        else
        {
            // $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_item),4)+1,4,'0') AS sem FROM item_master WHERE id_pemilik = '$id_pemilik' ");
            $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_so),16,4)+1,4,'0') AS sem FROM penjualan WHERE id_pemilik = ".$this->session->userdata('id_retail')." and game_id = ".$this->session->userdata('game_id')." and tanggal = '".$this->session->userdata('tanggal')."'");

            $temp2 = $temp->result();
            $index = $temp2[0]->sem;
        }
         $id_so = 'SO'.$this->session->userdata('game_id').$this->session->userdata('id_retail').date('Ymd').$index;
        foreach ($ok->result() as $readytoInsert) 
        {
                $total = $total + ($readytoInsert->harga * $readytoInsert->jumlah);
                $dataDetail = array(
                    'id_so' => $id_so,
                    'id_item' => $readytoInsert->id_item,
                    'barcode_barang' => $readytoInsert->barcode_barang,
                    'jumlah' => $readytoInsert->jumlah,
                    'harga' => $readytoInsert->harga,
                    'id_suplier' =>$readytoInsert->id_suplier,                                        
                    'id_pemilik' =>$pemilik,
                    'game_id'=> $readytoInsert->game_id,
                );
                $ok2 = $this->db->insert('detail_penjualan',$dataDetail);
                $update = array(
                        'jumlah' => $readytoInsert->stock,
                );
                $this->db->where('barcode_barang',$readytoInsert->barcode_barang);
                $this->db->where('id_pemilik',$readytoInsert->id_pemilik);
                $this->db->where('game_id',$readytoInsert->game_id);
                $this->db->update('gudang',$update);
        }
        if ($total)
        {
            $hasil = array(
                        'id_so' => $id_so, 
                        'id_petugas' => $this->session->userdata('id_retail'),
                        'id_customer' => 1,
                        // 'tanggal' =>  date('d-m-Y'),
                        'tanggal' =>  $this->session->userdata('tanggal'),
                        'total' => $total ,
                        'kurir' => 1,
                        'status' => 0,
                        // 'tanggal_keluar' => $this->session->userdata('tanggal'),
                        'id_pemilik' => $this->session->userdata('id_retail'),
                        'game_id'=> $readytoInsert->game_id,

                    );
           
            $ok = $this->db->insert('penjualan',$hasil);
            $total = 0;

            //gl
            $this->load->model('mgudang');
                $dataupdate = array(
                    // 'idTransaksi' => $id_so,
                    'status' => 3
                );
                 $this->db->where('id_so',$id_so);
                $ok5 = $this->db->update('penjualan',$dataupdate);
                //$ok5 = $this->mpenjualan->updateStatus($dataupdate);

            $detail = $this->db->query("SELECT SUM(c.harga*c.jumlah) AS jumlahjual , SUM(a.hargaSatuan*c.jumlah) AS jumlahbeli
                    FROM gudang a 
                    INNER JOIN detail_penjualan AS c ON c.`barcode_barang` = a.`barcode_barang`
                    where a.id_pemilik='$pemilik' and c.id_so='$id_so'");
                $jual = $detail->row();
                $game_id =  6;

            $query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES('$hasil[tanggal]',CONCAT('Pembayaran Invoice ','$id_so'),'$pemilik',$game_id)");
            $this->load->model('mgl');
            $schema = $this->mgl->schema_line(2);
            $searchArray = array("HB", "HJ", "0");
            $replaceArray = array($jual->jumlahbeli,$jual->jumlahjual,0);
            $haha = $this->mpenjualan->cek2();
            $idjurnalH = $haha->apa;
            // echo 'id jurnal : '.$idjurnalH.'<br>';
            foreach ($schema as $line) 
            {
                // echo 'insert data to gl <br>';
                $data_line = array(
                    'journal_id' => $idjurnalH,
                    'acc_id' => $line->acc_id,
                    'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
                    'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
                    'uid' => $pemilik,
                    'game_id' => $this->session->userdata('game_id')
                );
                $ok2 = $this->db->insert('gl_journal_l',$data_line);
            }
            // echo 'gl selesai <br>';
            //if($query2)
            //{
              // $this->load->model('mpenjualan');
                $this->load->model('mgudang');
                $data2 = $this->mgudang->rincianViewSO($id_so);
                $dataSO = $data2[0];
                // print_r($dataSO);
                $datagudang = array(
                        'idSO' => $id_so,
                        'idTransaksi' => uniqid("ISSUE"),
                        'total' => $dataSO->total,
                        'tgl' => $dataSO->tanggal,
                        'kurir' => 1,
                        'idCustomer' => $dataSO->id_customer,
                        'email' => 'hello',
                        'kode' => 1,
                        'id_pemilik' =>$this->session->userdata('id_retail')
                    );
                // // // print_r($datagudang);
                $ok4 = $this->mgudang->keluarGudang($datagudang);
            // echo 'selesai keluar barang<br>';  
            
        }
        // echo 'insert penjualan <br>';
        //insert header
        // foreach ($ok->result() as $data) 
        // {
        //     $yeay[] = $data;
        // }

        // print_r($yeay);

        $this->db->where('game_id', $game_id);
        $this->db->where('id_pemilik', $this->session->userdata('id_retail'));
        $this->db->delete('market_share'); 
        return true;
    } 


    public function MarketStartTest()
    {        
        $game_id = $this->session->userdata('game_id');
        $this->load->model('mbi');
        $this->db->where('game_id',$game_id);
        $this->db->delete('market_share');        
        $homina = array();
        $this->db->reconnect();

        $this->db->select('game_player');
        $this->db->where('game_id',$game_id);
        $read = $this->db->get('game_detail');
        $jumlah_player = 0;
        
        foreach ($read->result() as $data) 
            {
                $users[] = $data->game_player;
                $jumlah_player++;
            }
            $temp = array();
            $marketShare = array();
            $this->db->select('id_item');
            $readItem = $this->db->get('item_master');
            foreach ($readItem->result() as $datacek) 
            {
                    $temp[] = $datacek->id_item;
            $random[] = $datacek->id_item;
            }

            $ambil = mt_rand(20,30);
        // $ambil = 30;
            // echo 'ambil : '.$ambil.'<br>';
            //for ($i=0; $i < $ambil ; $i++) 
            //{ 
              //$index = mt_rand(0,29);
              //$random[] = $temp[$index];
            //}
            //echo 'start market share :<br>';
            // foreach ($temp as $temps) 
            // {
            //     if (in_array($temps, $random) == false)
            //     {
            //         $kurang[] = $temps;
            //     } 
            // }
            foreach ($random as $data2) 
            {  
                //echo 'item : '.$data2.'<br>';
                $this->db->select('sum(item_price) as maxHarga , sum(item_marketing_value) as maxMarket');
                $this->db->where('id_item ',$data2);
                $this->db->where('game_id',$game_id);
                $this->db->where_in('game_player',$users);
                $readread = $this->db->get('player_detail');
                foreach ($readread->result() as $data) 
                {
                    $totalHarga = $data->maxHarga;
                    $totalMarket = $data->maxMarket;
                }
   //            echo 'totalHarga : '.$totalHarga. ' totalMarket : '.$totalMarket.'<br>';
                $this->db->select('max_serapan');
                $this->db->where('id_item',$data2);
                $read = $this->db->get('item_master');
                foreach ($read->result() as $data) 
                {
                   $tmp = $data->max_serapan *$jumlah_player;
                   //$max = floor(rand(-20,20)*$tmp/100) + $tmp;
		   $max = $tmp;

                }
     //           echo 'id_item : '.$data2.' serapan : '.$max.'<br>';

                // buat normalisasi
                $this->db->select('id_item, item_price , item_marketing_value as item_marketing ,game_player');
                $this->db->where('game_id',$game_id);
                $this->db->where('id_item ',$data2);
                $this->db->where_in('game_player',$users);
                $this->db->group_by('game_player');

                $readUser2 = $this->db->get('player_detail');
                $normalisasi = 0;
                foreach ($readUser2->result() as $normal) 
                {
                    $serapHarga = ($totalHarga - $normal->item_price ) / $totalHarga;
                    // $serapHarga = $data3->item_harga / $totalHarga;
                        if($totalMarket)
                            {
                                $serapMarket = $normal->item_marketing / $totalMarket;
                            }
                        else
                            {
                                     $serapMarket = 0;
                            }
                        $normalisasi = $normalisasi + ((0.7 * $serapHarga) + (0.3 * $serapMarket));
                }
                // echo $max.' '.$data2.'<br>';
                $this->db->select('id_item, item_price , item_marketing_value as item_marketing ,game_player');
                $this->db->where('id_item ',$data2);
                $this->db->where('game_id',$game_id);
                $this->db->where_in('game_player',$users);
                $this->db->group_by('game_player');
                $readUser = $this->db->get('player_detail');
                $total =0;
                foreach ($readUser->result() as $data3) 
                {
                    //ambil max serapan
                    //$id_so = uniqid("SO");
                    $serapHarga = ($totalHarga - $data3->item_price ) / $totalHarga;
                    if($totalMarket)
                    {
                                        $serapMarket = $data3->item_marketing / $totalMarket;
                    }
                    else
                    {
                                        $serapMarket = 0;
                    }
                    $tes = ((0.7 * $serapHarga) + (0.3 * $serapMarket))/$normalisasi;
                    $serapan = floor($tes*$max);

                    $total = $total + ($data3->item_price * $serapan);
                    if($serapan > 0)
                    { 
                        $cekstock = $this->getStock($data3->id_item,$data3->game_player);
                        //echo 'item : '.$data3->id_item.' stock : '.$cekstock.' pemilik : '.$data3->id_pemilik.'<br>';
                        if($cekstock > 0)
                        {
                            $datagudang = array();
                            $this->db->select('barcode_barang , jumlah,id_supplier,id_pemilik');
                            $this->db->where('game_id',$this->session->userdata('game_id'));                          
                            $this->db->where('id_pemilik ',$data3->game_player);
                            $this->db->where('jumlah > 0');
                            $this->db->where('id_item',$data3->id_item);
                            $read2 = $this->db->get('gudang');
                            {
                                foreach ($read2->result() as $data2) 
                                {
                                    $datagudang[] = $data2;
                                }
                            }
                            foreach ($datagudang as $datadata) 
                            {   //coment if ini
                                //if($data3->id_pemilik = $this->session->userdata('id_retail'))
                                //{
                                    if($serapan <= $datadata->jumlah)
                                    {
                                        //array_push(
                                          //      $marketShare,
                                            $marketshaaare = array(
                                                //    'id_so' => $id_so,
                                                    'id_item' => $data3->id_item,
                                                    'barcode_barang' => $datadata->barcode_barang,
                                                    'jumlah' => $serapan,
                                                    'harga' => $data3->item_price,
                                                    'id_suplier' =>$datadata->id_supplier,
                                                    'id_pemilik' =>$data3->game_player,
                                                    'game_id'=>$game_id,
                                                    'stock' => $datadata->jumlah - $serapan,
                                                );
                                        // );
                                            $this->db->insert('market_share',$marketshaaare);
                                            // print_r($marketshaaare);
                                            // $stock = $datadata->jumlah - $serapan;
                                            $serapan = 0; 
                                            break;                                              
                                        //echo 'barcode_barang :'.$datadata->barcode_barang.'  jumlah : '.$serapan.' pemilik : '.$data3->id_pemilik.'<br>';
                                    }
                                    else
                                    {
                                        // array_push(
                                        //         $marketShare,
                                        $marketshaaare  =  array(
                                                  //  'id_so' => $id_so,
                                                    'id_item' => $data3->id_item,
                                                    'barcode_barang' => $datadata->barcode_barang,
                                                    'jumlah' => $datadata->jumlah,
                                                    'harga' => $data3->item_price,
                                                    'id_suplier' =>$datadata->id_supplier,                                        
                                                    'id_pemilik' =>$data3->game_player,
                                                    'game_id'=>$game_id,
                                                    'stock' => 0,
                                                );
                                        // );
                                        $serapan = $serapan - $datadata->jumlah;
                                        //echo 'barcode_barang :'.$datadata->barcode_barang.' jumlah : '.$datadata->jumlah.' pemilik : '.$data3->id_pemilik.'<br>';
                                    $this->db->insert('market_share',$marketshaaare);

                                    }
                                //} // sama sini
                                // echo '<br>';
                                // echo '<br>';
                            }
                        }
                        
                    }
                    
                }
            // echo '-- ganti item -- <br><br>';
            }
            // foreach ($kurang as $data2) 
            // {
            //      $this->db->select('id_item, item_price , item_marketing_value as item_marketing ,game_player');
            //     $this->db->where('id_item ',$data2);
            //     $this->db->where('game_id',$game_id);
            //     $this->db->where_in('game_player',$users);
            //     $this->db->group_by('game_player');
            //     $readUser = $this->db->get('player_detail');
            //     $total =0;
            //     foreach ($readUser->result() as $data3) 
            //     {
            //         $cekstock = $this->getStock($data2,$data3->game_player);
            //                 // echo ' ini cekstock : '.$cekstock.' <br>'
            //         if($cekstock > 0)
            //         {
            //             $datagudang = array();
            //             $this->db->select('barcode_barang , jumlah,id_supplier,id_pemilik');
            //             $this->db->where('game_id',$game_id);                          
            //             $this->db->where('id_pemilik ',$data3->game_player);
            //             $this->db->where('jumlah > 0');
            //             $this->db->where('id_item',$data3->id_item);
            //             $this->db->limit(1);
            //             $read2 = $this->db->get('gudang');
                        
            //             foreach ($read2->result() as $datagudangs) 
            //             {
            //                  $marketshaaare  =  array(
            //                                         'id_item' => $data3->id_item,
            //                                         'barcode_barang' => $datagudangs->barcode_barang,
            //                                         'jumlah' => 0,
            //                                         'harga' => $data3->item_price,
            //                                         'id_suplier' =>$datagudangs->id_supplier,                                        
            //                                         'id_pemilik' =>$data3->game_player,
            //                                         'game_id'=>$game_id,
            //                                         'stock' => $datagudangs->jumlah,
            //                                     );
            //                 $this->db->insert('market_share',$marketshaaare);
            //             }

            //         }
            //         // else
            //         // {
            //         //     $marketshaaare  =  array(
            //         //                                 'id_item' => $data3->id_item,
            //         //                                 'barcode_barang' => 0,
            //         //                                 'jumlah' => 0,
            //         //                                 'harga' => $data3->item_price,
            //         //                                 'id_suplier' =>0,                                        
            //         //                                 'id_pemilik' =>$data3->game_player,
            //         //                                 'game_id'=>$game_id,
            //         //                                 // 'stock' => $datagudangs->jumlah,
            //         //                                 'stock' => 20,

            //         //                             );
            //         //     $this->db->insert('market_share',$marketshaaare);
            //         // }
            //     }

            // }
        //}
        // echo '<br>marketShare : <br>';
       // print_r($marketShare);
        // $pemilik = $this->session->userdata('id_retail');
        foreach ($users as $userlho) 
        {
             $pemilik = $userlho;
            $this->db->where('game_id',$game_id);
            $this->db->where('id_pemilik',$pemilik);
            $this->db->order_by("id_pemilik", "asc"); 
            $ok = $this->db->get('market_share');
            $total = 0;
            $query = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_so),16),4,'0') AS maxi FROM penjualan WHERE id_pemilik = ".$pemilik." and game_id = ".$this->session->userdata('game_id')." and tanggal = '".$this->session->userdata('tanggal')."'");
            //echo $pemilik.' ';
                $query2 = $query->result();
                $tes = $query2[0]->maxi;
                $index = '0';
                if(!isset($tes))
                {
                    $index =  '0001';
                }
                else
                {
                    // $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_item),4)+1,4,'0') AS sem FROM item_master WHERE id_pemilik = '$id_pemilik' ");
                    $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_so),16,4)+1,4,'0') AS sem FROM penjualan WHERE id_pemilik = ".$pemilik." and game_id = ".$this->session->userdata('game_id')." and tanggal = '".$this->session->userdata('tanggal')."'");
                    $temp2 = $temp->result();
                    $index = $temp2[0]->sem;
                    //echo 'masuk sini lho ';
                }
                //echo $index.' ';
                $tanggalA = str_replace("-", "", $this->session->userdata('tanggal'));   
                 $id_so = 'SO'.$this->session->userdata('game_id').$pemilik.$tanggalA.$index;
            //echo $id_so.'<br>';
            foreach ($ok->result() as $readytoInsert) 
            {
                    // $id_so = $readytoInsert->id_so;
                    $total = $total + ($readytoInsert->harga * $readytoInsert->jumlah);
                    $dataDetail = array(
                        'id_so' => $id_so,
                        'id_item' => $readytoInsert->id_item,
                        'barcode_barang' => $readytoInsert->barcode_barang,
                        'jumlah' => $readytoInsert->jumlah,
                        'harga' => $readytoInsert->harga,
                        'id_suplier' =>$readytoInsert->id_suplier,                                        
                        // 'id_pemilik' =>$pemilik,
                        'id_pemilik' =>$readytoInsert->id_pemilik,

                        'game_id'=>$game_id,
                    );
                    $ok2 = $this->db->insert('detail_penjualan',$dataDetail);
                    $update = array(
                            'jumlah' => $readytoInsert->stock,
                    );
                    $this->db->where('barcode_barang',$readytoInsert->barcode_barang);
                    $this->db->where('id_pemilik',$readytoInsert->id_pemilik);
                    $this->db->where('game_id',$readytoInsert->game_id);
                    $this->db->update('gudang',$update);

                    //PED
                    //$this->load->model('mbi');
                    // $dataped = array(
                    //   'id_item' => $readytoInsert->id_item,
                    //   'price' => $readytoInsert->harga,
                    //   'quantity' => $readytoInsert->jumlah,
                    // );
                    // $query=$this->mbi->ped($readytoInsert->id_pemilik, $dataped);
                    //SAMPE SINI
            // $this->mbi->update1($readytoInsert->id_pemilik,$readytoInsert->id_item);
                // $this->mbi->update12($readytoInsert->id_pemilik,$readytoInsert->id_item);    
            }
            if ($total)
            {
                $hasil = array(
                            'id_so' => $id_so, 
                            'id_petugas' => $pemilik,
                            'id_customer' => 1,
                            // 'tanggal' =>  date('d-m-Y'),
                            'tanggal' =>  $this->session->userdata('tanggal'),
                            'total' => $total ,
                            'kurir' => 1,
                            'status' => 0,
                            // 'tanggal_keluar' => $this->session->userdata('tanggal'),
                            // 'id_pemilik' => $this->session->userdata('id_retail'),
                            'id_pemilik' => $pemilik,
                            'game_id'=>$game_id,

                        );
               
                $ok = $this->db->insert('penjualan',$hasil);
                $total = 0;

                //gl
                $this->load->model('mgudang');
                    $dataupdate = array(
                        // 'idTransaksi' => $id_so,
                        'status' => 3
                    );
                     $this->db->where('id_so',$id_so);
                    $ok5 = $this->db->update('penjualan',$dataupdate);
                    //$ok5 = $this->mpenjualan->updateStatus($dataupdate);

                $detail = $this->db->query("SELECT SUM(c.harga*c.jumlah) AS jumlahjual , SUM(a.hargaSatuan*c.jumlah) AS jumlahbeli
                        FROM gudang a 
                        INNER JOIN detail_penjualan AS c ON c.`barcode_barang` = a.`barcode_barang`
                        where a.id_pemilik='$pemilik' and c.id_so='$id_so'");
                    $jual = $detail->row();
                    //$game_id =  $this->session->userdata('game_id');

                $this->load->model('mgl');
                
                $dataggl = array(
                    'tgl'=> $hasil['tanggal'],
                    'idTransaksi' =>$id_so,
                    'id_pemilik' =>$pemilik,
                    'game_id' =>$game_id,
                    'hargabeli' =>$jual->jumlahbeli,
                    'hargajual' =>$jual->jumlahjual,

                    );
                $this->mgl->bayar_penjualan($dataggl);


                //echo 'gl selesai <br>';
                //if($query2)
                //{
                  // $this->load->model('mpenjualan');
                    $this->load->model('mgudang');
                    $data2 = $this->mgudang->rincianViewSO($id_so);
                    $dataSO = $data2[0];
                    // print_r($dataSO);
                    $datagudang = array(
                            'idSO' => $id_so,
                            'idTransaksi' => uniqid("ISSUE"),
                            'total' => $dataSO->total,
                            'tgl' => $dataSO->tanggal,
                            'kurir' => 1,
                            'idCustomer' => 0,
                            'email' => 'hello',
                            'kode' => 1,
                            'id_pemilik' =>$pemilik
                        );
                    // // // print_r($datagudang);
                    $ok4 = $this->mgudang->keluarGudang($datagudang);
                // echo 'selesai keluar barang<br>';  
                
            }
            //$this->m_game->biforecast($userlho);
        }

         
        return true;
    }

            public function getPrice($id_item)
    {
        $this->db->reconnect();
        $this->db->select('harga');
        $this->db->where('id_item',$id_item);
        $this->db->where('id_suplier = 1');
        $read = $this->db->get('detail_suplier');
        foreach ($read->result() as $data) 
        {
           $harga = $data->harga;
        }
        return $harga;
    }

public function initializePD($game_id)
    {
        $item = array('1200011121','1200021121','1200031121','1200091121','1200101121','1200111121','1400011121','1400021121','1400031121','1400041121','1400051121','1400061121','1400071121','1500011121','1500031121','1500091121','1500101121','1700011121','1700021121','1700031121','1700041121','1700051121','2100011121','2100021121','2100031121','2100151121','2200041121','2200051121','2200061121','2200111121');        
        //$user = $this->searchUser($game_id);
    $user = array('1111','1112','1113');
        foreach ($user as $users) 
        {
            foreach ($item as $items) 
            {
                
                 $hargaawal = $this->getPrice($items);
                 $harga = 1.25*$hargaawal;
                 $data = array(
                     'id_item' => $items,
                     // 'jumlah' => rand(15,30),
                     'item_price' => $harga,
                     'item_marketing' => $hargaawal/100,
                     'game_player' => $users,
                     // 'barcode_barang' => $barcode_barang,
                     // 'id_supplier' => rand(1,5),
                     'game_id' =>$game_id,
                     );
                 $this->db->insert('player_detail',$data);

                 $dataconfig = array(
                     'id_item' => $items,
                     'id_pemilik' => $users,
                     'game_id' => $game_id,
                     'jumlah' => 150,
                     'supplier' => 1,
                     'boundary' => 600,
                     );
                 $this->db->insert('config_product',$dataconfig);
            }
           //$data = array(
             //           'jenis_modal' => 2,
               //         'jumlah' => 2000000000,
                 //       'id_pemilik' => $users,
                   //     'game_id' => $game_id
                   // );
            $data2 = array(
                        'jenis_modal' => 1,
                        'jumlah' => 5000000000,
                        'id_pemilik' => $users,
                        'game_id' => $game_id,
                        'rate' => 15,
                        'lamaPinjam' => 48,
                    );

              $this->load->model('mmodal');
              $this->load->model('mpenjualan');
              $this->load->model('m_expense');
              $this->load->model('m_aset');
            $haha = $this->mpenjualan->cek2();
            $idjurnalH = $haha->apa;
            $this->mmodal->addModal($data2,$idjurnalH,$game_id);
        $haha2 = $this->mpenjualan->cek2();
            $idjurnalH2 = $haha2->apa;
        $this->mmodal->addModal($data2,$idjurnalH2,$game_id);
            $id_pemilik = $users;
            // $idPemilik = $this->session->userdata('id_retail');
            $dataAset = array();
            array_push($dataAset,
                        array(
                            'asset_name' => 'Gudang',
                            'asset_duration' => 15,
                            'asset_duration_type' => 'y',
                            'asset_capacity' => 2000,
                            'asset_type' => 1,
                            'asset_value' => 2000000000,
                            'id_retail' => $id_pemilik,
                            'game_id' => $game_id,
                            ),
                        array(
                            'asset_name' => 'Tanah',
                            'asset_duration' => 100,
                            'asset_duration_type' => 'y',
                            'asset_capacity' => 2000,
                            'asset_type' => 2,
                            'asset_value' => 2000000000,
                            'id_retail' => $id_pemilik,
                            'game_id' => $game_id,
                            ),
                        array(
                            'asset_name' => 'Perlengkapan toko',
                            'asset_duration' => 5,
                            'asset_duration_type' => 'y',
                            'asset_capacity' => 2000,
                            'asset_type' => 1,
                            'asset_value' => 500000000,
                            'id_retail' => $id_pemilik,
                            'game_id' => $game_id,
                            ),
                        array(
                            'asset_name' => 'Franchise',
                            'asset_duration' => 10,
                            'asset_duration_type' => 'y',
                            'asset_capacity' => 2000,
                            'asset_type' => 1,
                            'asset_value' => 100000000,
                            'id_retail' => $id_pemilik,
                            'game_id' => $game_id,
                            )
                );
            $query = $this->m_aset->addaset2($dataAset);
            $total = 0;
            $depresiasi = 0;
            $this->db->select('asset_value,asset_duration,asset_duration_type');
            $this->db->where('game_id',$game_id);
            $this->db->where('id_retail',$id_pemilik);
            $this->db->where('asset_type',1);
            $read22 =  $this->db->get('asset');
            foreach ($read22->result() as $data) {
                $value = $data->asset_value;
                $duration = $data->asset_duration;
                $duration_type = $data->asset_duration_type;
                if($duration_type == 'y')
                {
                    $dep_temp = round($value/($duration*12));
                }
                elseif ($duration_type == 'm') 
                {
                    $dep_temp = round($value/$duration);     
                }
                $total = $total+$value;
                $depresiasi = $depresiasi + $dep_temp;
            }
            $asuransi = round((2/1000)*$total/12);
            $dataexpense = array();

            array_push(
                    $dataexpense,
                    array(
                        'expense_code' => 1,
                        'expense_jumlah' => 1,
                        'expense_value' => 0,
                        'id_pemilik' => $id_pemilik ,
                        'game_id' => $game_id ,            
                        ),
                    array(
                        'expense_code' => 2,
                        'expense_jumlah' => 2,
                        'expense_value' => 3000000,
                        'id_pemilik' => $id_pemilik ,
                        'game_id' => $game_id , 
                        ),
                    array(
                        'expense_code' => 3,
                        'expense_jumlah' => 1,
                        'expense_value' => 500000,
                        'id_pemilik' => $id_pemilik ,
                        'game_id' => $game_id , 
                        ),
                    array(
                        'expense_code' => 4,
                        'expense_jumlah' => 1,
                        'expense_value' => 500000,
                        'id_pemilik' => $id_pemilik ,
                        'game_id' => $game_id , 
                        ),
                    array(
                        'expense_code' => 5,
                        'expense_jumlah' => 1,
                        'expense_value' => $asuransi,
                        'id_pemilik' => $id_pemilik ,
                        'game_id' => $game_id , 
                        ),
                    array(
                        'expense_code' => 6,
                        'expense_jumlah' => 1,
                        'expense_value' => $depresiasi,
                        'id_pemilik' => $id_pemilik ,
                        'game_id' => $game_id , 
                        )
            );
            $query = $this->m_expense->update($dataexpense);
        }
        return true;
    }

public function normalisasiIklan($game_id)
    {
        $this->db->reconnect();
        $total = array();
        $this->db->select('game_player,sum(item_marketing) as total');
        $this->db->where('player_detail.game_id',$game_id);
        $this->db->group_by('game_player');
        $read = $this->db->get('player_detail');
        foreach ($read->result() as $data) 
        {
            $total[$data->game_player] = $data->total;
            // print_r($data->total);
        }
//        print_r($total);
        $this->db->select('game_player,id_item,game_id,item_marketing');
        $this->db->where('game_id',$game_id);
        $read2 = $this->db->get('player_detail');
        foreach ($read2->result() as $datas) 
        {
            $this->db->select('expense_value');
            $this->db->where('expense_code',1);
            $this->db->where('id_pemilik',$datas->game_player);
            $this->db->where('game_id',$game_id);
            $read3 = $this->db->get('expense');
            foreach ($read3->result() as $datasdatas) 
            {
                $iklan = $datasdatas->expense_value;
            }
            $marketing = round(100*$datas->item_marketing/$total[$datas->game_player],0);
            $value = $marketing*$iklan/100;
            // echo '<br>'.$datas->game_player.' '.$datas->id_item.' '.$datas->item_marketing.' '.$marketing;
            $dataupdate=array(
                'item_marketing' => $marketing,
                'item_marketing_value' =>$value,
                );
            $this->db->where('game_player',$datas->game_player);
            $this->db->where('id_item',$datas->id_item);
            $this->db->where('game_id',$game_id);
            $this->db->update('player_detail',$dataupdate);
            // $datawal[] = $datas;
        }
    }

    public function biforecast($cek)
    {
        //$cek=$this->session->userdata('username');
        if($cek){
            $uid = $cek;
            $game_id = $this->session->userdata('game_id');
            // $game_id = 12;
            $this->load->model('mbi');
            $data = $this->mbi->item();
            foreach ($data as $item) {
                // echo $item->id_item;
                $command = 'python /var/www/html/erp-game/assets/test.py '.$uid.' '.$game_id.' '.$item->id_item;
                // $command2 = (string)$command;
                // $command = 'python /var/www/html/erp-game/assets/hello.py';
                // var_dump($uid);
                // var_dump($game_id);
                $output = shell_exec($command);
                // echo $output;
                // $forecast = system("python , $retval);
                // var_dump($retval);
            }
            return true;
            // echo json_encode($data);
        }
    }


    public function marketShare()
    {
        $game_id = $this->session->userdata('game_id');
        // $game_id = 12;

        $this->load->model('mbi');
        $this->db->reconnect();
        $homina = array();

        $this->db->select('game_player');
        $this->db->where('game_id',$game_id);
        $readplayer = $this->db->get('game_detail');
        $jumlah_player = 0;
        
        foreach ($readplayer->result() as $dataplayer) 
            {
                // $users[] = $data->game_player;
                $users[] = $dataplayer->game_player;
                $jumlah_player++;
            }
            $temp = array();
            $marketShare = array();
            $this->db->select('id_item');
            $readItem = $this->db->get('item_master');
            foreach ($readItem->result() as $datacek) 
            {
                    $temp[] = $datacek->id_item;
                    //$random[] = $datacek->id_item;
            }
            $ambil = mt_rand(20,30);
            //$ambil = 30;
            // echo 'ambil : '.$ambil.'<br>';
            for ($i=0; $i < $ambil ; $i++) 
            { 
              $index = mt_rand(0,29);
              //echo $index.' ';
              $random[] = $temp[$index];
            }

            // foreach ($temp as $temps) 
            // {
            //     if (in_array($temps, $random) == false)
            //     {
            //         $kurang[] = $temps;
            //     } 
            // }

            //echo 'start market share :<br>';
            foreach ($random as $data2) 
            {  
                //echo 'item : '.$data2.'<br>';
                $this->db->select('sum(item_price) as maxHarga , sum(item_marketing_value) as maxMarket');
                $this->db->where('id_item ',$data2);
                $this->db->where('game_id',$game_id);
                $this->db->where_in('game_player',$users);
                $readread = $this->db->get('player_detail');
                foreach ($readread->result() as $data) 
                {
                    $totalHarga = $data->maxHarga;
                    $totalMarket = $data->maxMarket;
                }
   //            echo 'totalHarga : '.$totalHarga. ' totalMarket : '.$totalMarket.'<br>';
                $this->db->select('max_serapan');
                $this->db->where('id_item',$data2);
                $read = $this->db->get('item_master');
                foreach ($read->result() as $data) 
                {
                   $tmp = $data->max_serapan *$jumlah_player;
                   $max = floor(rand(-20,20)*$tmp/100) + $tmp;

                }
     //           echo 'id_item : '.$data2.' serapan : '.$max.'<br>';

                // buat normalisasi
                $this->db->select('id_item, item_price , item_marketing_value as item_marketing ,game_player');
                $this->db->where('game_id',$game_id);
                $this->db->where('id_item ',$data2);
                $this->db->where_in('game_player',$users);
                $this->db->group_by('game_player');

                $readUser2 = $this->db->get('player_detail');
                $normalisasi = 0;
                foreach ($readUser2->result() as $normal) 
                {
                    $serapHarga = ($totalHarga - $normal->item_price ) / $totalHarga;
                    // $serapHarga = $data3->item_harga / $totalHarga;
                        if($totalMarket)
                            {
                                $serapMarket = $normal->item_marketing / $totalMarket;
                            }
                        else
                            {
                                     $serapMarket = 0;
                            }
                        $normalisasi = $normalisasi + ((0.7 * $serapHarga) + (0.3 * $serapMarket));
                }
                // echo $max.' '.$data2.'<br>';
                $this->db->select('id_item, item_price , item_marketing_value as item_marketing ,game_player');
                $this->db->where('id_item ',$data2);
                $this->db->where('game_id',$game_id);
                $this->db->where_in('game_player',$users);
                $this->db->group_by('game_player');
                $readUser = $this->db->get('player_detail');
                $total =0;
                // foreach ($readUser->result() as $data) 
                // {
                //     $dataplayerdetail[] = $data;
                // }
                // print_r($dataplayerdetail);
                foreach ($readUser->result() as $data3) 
                {
                    //ambil max serapan
                    //$id_so = uniqid("SO");
                    $serapHarga = ($totalHarga - $data3->item_price ) / $totalHarga;
                    if($totalMarket)
                    {
                                        $serapMarket = $data3->item_marketing / $totalMarket;
                    }
                    else
                    {
                                        $serapMarket = 0;
                    }
                    $tes = ((0.7 * $serapHarga) + (0.3 * $serapMarket))/$normalisasi;
                    $serapan = floor($tes*$max);

                    $total = $total + ($data3->item_price * $serapan);
                    
                    //ini baru dikomen
                    //if($serapan > 0)
                    //{ 
                        $cekstock = $this->getStock($data3->id_item,$data3->game_player);
                        // echo ' ini cekstock : '.$cekstock.' <br>'
                        if($cekstock > 0)
                        {
                            $datagudang = array();
                            $this->db->select('barcode_barang , jumlah,id_supplier,id_pemilik');
                            $this->db->where('game_id',$game_id);                          
                            $this->db->where('id_pemilik ',$data3->game_player);
                            $this->db->where('jumlah > 0');
                            $this->db->where('id_item',$data3->id_item);
                            $read2 = $this->db->get('gudang');
                            foreach ($read2->result() as $data2) 
                            {
                                $datagudang[] = $data2;
                            }
                            foreach ($datagudang as $datadata) 
                            {   //coment if ini
                                //if($data3->id_pemilik = $this->session->userdata('id_retail'))
                                //{
                                    if($serapan <= $datadata->jumlah)
                                    {
                                        //array_push(
                                          //      $marketShare,
                                            $marketshaaare = array(
                                                //    'id_so' => $id_so,
                                                    'id_item' => $data3->id_item,
                                                    'barcode_barang' => $datadata->barcode_barang,
                                                    'jumlah' => $serapan,
                                                    'harga' => $data3->item_price,
                                                    'id_suplier' =>$datadata->id_supplier,
                                                    'id_pemilik' =>$data3->game_player,
                                                    'game_id'=>$game_id,
                                                    'stock' => $datadata->jumlah - $serapan,
                                                );
                                        // );
                                            $this->db->insert('market_share',$marketshaaare);
                                            // print_r($marketshaaare);
                                            // $stock = $datadata->jumlah - $serapan;
                                            $serapan = 0; 
                                            break;                                              
                                        //echo 'barcode_barang :'.$datadata->barcode_barang.'  jumlah : '.$serapan.' pemilik : '.$data3->id_pemilik.'<br>';
                                    }
                                    else
                                    {
                                        // array_push(
                                        //         $marketShare,
                                        $marketshaaare  =  array(
                                                  //  'id_so' => $id_so,
                                                    'id_item' => $data3->id_item,
                                                    'barcode_barang' => $datadata->barcode_barang,
                                                    'jumlah' => $datadata->jumlah,
                                                    'harga' => $data3->item_price,
                                                    'id_suplier' =>$datadata->id_supplier,                                        
                                                    'id_pemilik' =>$data3->game_player,
                                                    'game_id'=>$game_id,
                                                    'stock' => 0,
                                                );
                                        // );
                                        $serapan = $serapan - $datadata->jumlah;
                                        //echo 'barcode_barang :'.$datadata->barcode_barang.' jumlah : '.$datadata->jumlah.' pemilik : '.$data3->id_pemilik.'<br>';
                                    $this->db->insert('market_share',$marketshaaare);

                                    }
                                //} // sama sini
                                // echo '<br>';
                                // echo '<br>';
                            }
                        }
                        // else
                        // {
                        //     $marketshaaare  =  array(
                        //                             'id_item' => $data3->id_item,
                        //                             'barcode_barang' => 0,
                        //                             'jumlah' => 0,
                        //                             'harga' => $data3->item_price,
                        //                             'id_suplier' =>0,                                        
                        //                             'id_pemilik' =>$data3->game_player,
                        //                             'game_id'=>$game_id,
                        //                             'stock' => 0,
                        //                         );
                        //     $this->db->insert('market_share',$marketshaaare);
                        // }   
                    //}
                    
                }
            // echo '-- ganti item -- <br><br>';
            }

            // foreach ($kurang as $data2) 
            // {
            //      $this->db->select('id_item, item_price , item_marketing_value as item_marketing ,game_player');
            //     $this->db->where('id_item ',$data2);
            //     $this->db->where('game_id',$game_id);
            //     $this->db->where_in('game_player',$users);
            //     $this->db->group_by('game_player');
            //     $readUser = $this->db->get('player_detail');
            //     $total =0;
            //     foreach ($readUser->result() as $data3) 
            //     {
            //         $cekstock = $this->getStock($data2,$data3->game_player);
            //                 // echo ' ini cekstock : '.$cekstock.' <br>'
            //         if($cekstock > 0)
            //         {
            //             $datagudang = array();
            //             $this->db->select('barcode_barang , jumlah,id_supplier,id_pemilik');
            //             $this->db->where('game_id',$game_id);                          
            //             $this->db->where('id_pemilik ',$data3->game_player);
            //             $this->db->where('jumlah > 0');
            //             $this->db->where('id_item',$data3->id_item);
            //             $this->db->limit(1);
            //             $read2 = $this->db->get('gudang');
                        
            //             foreach ($read2->result() as $datagudangs) 
            //             {
            //                  $marketshaaare  =  array(
            //                                         'id_item' => $data3->id_item,
            //                                         'barcode_barang' => $datagudangs->barcode_barang,
            //                                         'jumlah' => 1,
            //                                         'harga' => $data3->item_price,
            //                                         'id_suplier' =>$datagudangs->id_supplier,                                        
            //                                         'id_pemilik' =>$data3->game_player,
            //                                         'game_id'=>$game_id,
            //                                         'stock' => $datagudangs->jumlah,
            //                                     );
            //                 $this->db->insert('market_share',$marketshaaare);
            //             }

            //         }
            //         // else
            //         // {
            //         //     $marketshaaare  =  array(
            //         //                                 'id_item' => $data3->id_item,
            //         //                                 'barcode_barang' => 0,
            //         //                                 'jumlah' => 0,
            //         //                                 'harga' => $data3->item_price,
            //         //                                 'id_suplier' =>0,                                        
            //         //                                 'id_pemilik' =>$data3->game_player,
            //         //                                 'game_id'=>$game_id,
            //         //                                 // 'stock' => $datagudangs->jumlah,
            //         //                                 'stock' => 20,

            //         //                             );
            //         //     $this->db->insert('market_share',$marketshaaare);
            //         // }
            //     }

            // }

        $dataupdateplayer = array(
            'player_input' => 1,
            );
        $this->db->where('game_id',$game_id);
        $this->db->update('game_detail',$dataupdateplayer);
    }

    public function getinput($id_pemilik,$game_id)
    {
        $flag = 0;
        $this->db->reconnect();
        $this->db->select('player_input');
        $this->db->where('game_player',$id_pemilik);
        $this->db->where('game_id',$game_id);
        $read = $this->db->get('game_detail');
        foreach ($read->result() as $data) 
        {
            $flag = $data->player_input;
        }
        return $flag;
    }

    public function sales($pemilik)
    {
        // $pemilik = $userlho;
        $game_id = $this->session->userdata('game_id');
        $query = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_so),17),4,'0') AS maxi FROM penjualan WHERE id_pemilik = ".$pemilik." and game_id = ".$this->session->userdata('game_id')." and tanggal = '".$this->session->userdata('tanggal')."'");
        //echo $pemilik.' ';
        $query2 = $query->result();
        $tes = $query2[0]->maxi;
        $index = '0';
        if(!isset($tes))
        {
            $index =  '0001';
        }
        else
        {
            // $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_item),4)+1,4,'0') AS sem FROM item_master WHERE id_pemilik = '$id_pemilik' ");
            $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_so),17,4)+1,4,'0') AS sem FROM penjualan WHERE id_pemilik = ".$pemilik." and game_id = ".$this->session->userdata('game_id')." and tanggal = '".$this->session->userdata('tanggal')."'");
            $temp2 = $temp->result();
            $index = $temp2[0]->sem;
            //echo 'masuk sini lho ';
        }
        //echo $index.' ';
        $tanggalA = str_replace("-", "", $this->session->userdata('tanggal'));   
        $id_so = 'SO'.$this->session->userdata('game_id').$pemilik.$tanggalA.$index;
        //echo '<br>'.$id_so.'<br>';
        $this->db->where('game_id',$game_id);
        $this->db->where('id_pemilik',$pemilik);
        $this->db->order_by("id_pemilik", "asc"); 
        $ok = $this->db->get('market_share');
        $total = 0;
        //echo $id_so.'<br>';
        foreach ($ok->result() as $readytoInsert) 
        {
                // $id_so = $readytoInsert->id_so;
                $total = $total + ($readytoInsert->harga * $readytoInsert->jumlah);
                $dataDetail = array(
                    'id_so' => $id_so,
                    'id_item' => $readytoInsert->id_item,
                    'barcode_barang' => $readytoInsert->barcode_barang,
                    'jumlah' => $readytoInsert->jumlah,
                    'harga' => $readytoInsert->harga,
                    'id_suplier' =>$readytoInsert->id_suplier,                                        
                    // 'id_pemilik' =>$pemilik,
                    'id_pemilik' =>$readytoInsert->id_pemilik,

                    'game_id'=>$game_id,
                );
                $ok2 = $this->db->insert('detail_penjualan',$dataDetail);

                //tambahan if
                if($readytoInsert->jumlah)
                {
                    $update = array(
                            'jumlah' => $readytoInsert->stock,
                    );
                    $this->db->where('barcode_barang',$readytoInsert->barcode_barang);
                    $this->db->where('id_pemilik',$readytoInsert->id_pemilik);
                    $this->db->where('game_id',$readytoInsert->game_id);
                    $this->db->update('gudang',$update);
                }
                //sampe sini

                //PED
                //$this->load->model('mbi');
                // $dataped = array(
                //   'id_item' => $readytoInsert->id_item,
                //   'price' => $readytoInsert->harga,
                //   'quantity' => $readytoInsert->jumlah,
                // );
                // $query=$this->mbi->ped($readytoInsert->id_pemilik, $dataped);
                //SAMPE SINI
//            $this->mbi->update1($readytoInsert->id_pemilik,$readytoInsert->id_item);
  //          $this->mbi->update12($readytoInsert->id_pemilik,$readytoInsert->id_item);   
        }
        if ($total)
        {
            $hasil = array(
                        'id_so' => $id_so, 
                        'id_petugas' => $pemilik,
                        'id_customer' => 1,
                        // 'tanggal' =>  date('d-m-Y'),
                        'tanggal' =>  $this->session->userdata('tanggal'),
                        'total' => $total ,
                        'kurir' => 1,
                        'status' => 0,
                        // 'tanggal_keluar' => $this->session->userdata('tanggal'),
                        // 'id_pemilik' => $this->session->userdata('id_retail'),
                        'id_pemilik' => $pemilik,
                        'game_id'=>$game_id,

                    );
           
            $ok = $this->db->insert('penjualan',$hasil);
            $total = 0;

            //gl
            $this->load->model('mgudang');
                $dataupdate = array(
                    // 'idTransaksi' => $id_so,
                    'status' => 3
                );
                 $this->db->where('id_so',$id_so);
                $ok5 = $this->db->update('penjualan',$dataupdate);
                //$ok5 = $this->mpenjualan->updateStatus($dataupdate);

            $detail = $this->db->query("SELECT SUM(c.harga*c.jumlah) AS jumlahjual , SUM(a.hargaSatuan*c.jumlah) AS jumlahbeli
                    FROM gudang a 
                    INNER JOIN detail_penjualan AS c ON c.`barcode_barang` = a.`barcode_barang`
                    where a.id_pemilik='$pemilik' and c.id_so='$id_so'");
                $jual = $detail->row();
                //$game_id =  $this->session->userdata('game_id');

            $this->load->model('mgl');
            
            $dataggl = array(
                'tgl'=> $hasil['tanggal'],
                'idTransaksi' =>$id_so,
                'id_pemilik' =>$pemilik,
                'game_id' =>$game_id,
                'hargabeli' =>$jual->jumlahbeli,
                'hargajual' =>$jual->jumlahjual,

                );
            $this->mgl->bayar_penjualan($dataggl);


            //echo 'gl selesai <br>';
            //if($query2)
            //{
              // $this->load->model('mpenjualan');
                $this->load->model('mgudang');
                $data2 = $this->mgudang->rincianViewSO($id_so);
                $dataSO = $data2[0];
                // print_r($dataSO);
                $datagudang = array(
                        'idSO' => $id_so,
                        'idTransaksi' => uniqid("ISSUE"),
                        'total' => $dataSO->total,
                        'tgl' => $dataSO->tanggal,
                        'kurir' => 1,
                        'idCustomer' => 0,
                        'email' => 'hello',
                        'kode' => 1,
                        'id_pemilik' =>$pemilik
                    );
                // // // print_r($datagudang);
                $ok4 = $this->mgudang->keluarGudang($datagudang);
            // echo 'selesai keluar barang<br>';  
            
        }
        $dataupdateplayer = array(
            'player_input' => 0,
            );
        $this->db->where('game_id',$game_id);
        $this->db->where('game_player',$pemilik);
        $this->db->update('game_detail',$dataupdateplayer);

        //$this->m_game->biforecast($pemilik);
        return true;
    }

    public function searchmarketshare($game_id)
    {
        $this->db->reconnect();
        $row = 0;
        $this->db->select('count(1) as jumlah');
        $this->db->where('game_id',$game_id);
        $read = $this->db->get('market_share');
        foreach ($read->result() as $data) 
        {
            $row = $data->jumlah;
        }
        return $row;
    }

    public function searchtotalInput($game_id)
    {
        $this->db->reconnect();
        $row = 0;
        $this->db->select('sum(player_input) as jumlah');
        $this->db->where('game_id',$game_id);
        $this->db->where('player_status = 1');
        $read = $this->db->get('game_detail');
        foreach ($read->result() as $data) 
        {
            $row = $data->jumlah;
        }
        return $row;   
    }

    public function deletemarket($game_id)
    {
        $this->db->reconnect();
        $this->db->where('game_id',$game_id);
        $this->db->delete('market_share');        
    }

    public function updatedetailInput($id_pemilik,$game_id,$update)
    {
        $this->db->reconnect();
        $dataupdate= array('player_status' => $update,);
        $this->db->where('game_player',$id_pemilik);
        $this->db->where('game_id',$game_id);
        $this->db->update('game_detail',$dataupdate);
    }

    public function prosesbi()
    {
        $this->db->reconnect();

        $this->load->model('mbi');
        $this->m_game->biforecast($this->session->userdata('id_retail'));
        $this->db->select('id_item,avg(harga),sum(jumlah)');
        $this->db->where('id_pemilik',$this->session->userdata('id_retail'));
        $this->db->where('game_id',$this->session->userdata('game_id'));
        $this->db->group_by('id_so,id_item');
        $read = $this->db->get('detail_penjualan');
        foreach ($read->result() as $data) 
        {
                // PED
                $dataped = array(
                  'id_item' => $data->id_item,
                  'price' => $data->harga,
                  'quantity' => $data->jumlah,
                );
                $query=$this->mbi->ped($this->session->userdata('id_retail'), $dataped);
                // SAMPE SINI
           $this->mbi->update1($this->session->userdata('id_retail'),$data->id_item);
           $this->mbi->update12($this->session->userdata('id_retail'),$data->id_item);   
        }
    }

    public function prosesbiAll()
    {
        $this->db->reconnect();
        $this->db->select('game_player');
        $this->db->where('game_id',$this->session->userdata('game_id'));
        $read2 = $this->db->get('game_detail');
        foreach ($read2->result() as $data2) 
        {
            $this->load->model('mbi');
            $this->biforecast($data2->game_player);
        echo 'selesai forecast '.$data2->game_player.'<br>';
            $this->db->select('id_item,avg(harga) as harga,sum(jumlah) as jumlah');
            $this->db->where('id_pemilik',$data2->game_player);
            $this->db->where('game_id',$this->session->userdata('game_id'));
            $this->db->group_by('id_item');
            $read = $this->db->get('detail_penjualan');
            foreach ($read->result() as $data) 
            {
                    // PED
                    $dataped = array(
                      'id_item' => $data->id_item,
                      'price' => $data->harga,
                      'quantity' => $data->jumlah,
                    );
                    $query=$this->mbi->ped($data2->game_player, $dataped);
//echo 'selesai ped <br>';
                    // SAMPE SINI
               $this->mbi->update1($data2->game_player,$data->id_item);
               $this->mbi->update12($data2->game_player,$data->id_item);   
//echo 'selesai update <br>';
            }
        }
    }

public function user()
    {
        $this->db->reconnect();
        $this->db->select('game_player');
        $this->db->where('game_id',$this->session->userdata('game_id'));
        $read2 = $this->db->get('game_detail');
        foreach ($read2->result() as $data) 
        {
            $hasil[] = $data->game_player ;
        }
        return $hasil;
    }
}
?>
