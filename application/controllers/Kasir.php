<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasir extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('mproduk');
		$this->load->model('mpenjualan');
		$this->load->model('mbi');

	}

	public $beli = array();
	public $total = array();
	public function index(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			// echo $data['cek'];
			$this->load->view('dasboard/kasir');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	// public function data(){
	// 	$cek=$this->session->userdata('username');
	// 	if($cek){
	// 		$id = $this->session->userdata('company');
	// 		$this->load->model('m_company');
	// 		$data = $this->m_company->read($id);
	// 		echo json_encode($data);
	// 	}else{

	// 		redirect('home');
	// 	}
	// }

	public function getItem(){
		$cek=$this->session->userdata('username');
		if($cek){
			$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
			$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
			$offset = ($page-1)*$rows;
			$id = $this->session->userdata('id_petugas');
			$data = $this->mproduk->list_produk_pagination($id,$rows,$offset);
			echo json_encode($data);
		}else{

			redirect('home');
		}
	}

	public function data($item){
		$cek=$this->session->userdata('username');
		if($cek){
				$data = $this->mproduk->findItem($item);
			// var_dump($data);
			echo json_encode($data);
			// print_r($item);
		}else{
			redirect('home');
		}
	}

	public function add()
	{
		 $collection = $this->input->post('collection');
		 $total = $this->input->post('total');

		 // $yeye = array();
       	 // $id_so	= 'SOcobacobacuy';
       	 //$id_so	= uniqid("SO");
$query = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_so),16),4,'0') AS maxi FROM penjualan WHERE id_pemilik = ".$this->session->userdata('id_retail')." and game_id = ".$this->session->userdata('game_id')." and tanggal = '".$this->session->userdata('tanggal')."'");

        $query2 = $query->result();
        $tes = $query2[0]->maxi;
        $index = '0';
        if(!isset($tes))
        {
            $index =  '0001';
        }
        else
        {
            // $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_item),4)+1,4,'0') AS sem FROM item_master WHERE id_pemilik = '$id_pemilik' ");
            $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_so),16,4)+1,4,'0') AS sem FROM penjualan WHERE id_pemilik = ".$this->session->userdata('id_retail')." and game_id = ".$this->session->userdata('game_id')." and tanggal = '".$this->session->userdata('tanggal')."'");

            $temp2 = $temp->result();
            $index = $temp2[0]->sem;
        }
       	 $id_so	= 'SO'.$this->session->userdata('game_id').$this->session->userdata('id_retail').date('Ymd').$index;
	
      // print_r($collection);
       	 foreach ($collection as $data) 
       	 {
		 	$stock = $this->mpenjualan->cekStock($data['barcode']);
		 	if($stock >= $data['quantity'])
       	 	{
       	 		$flag = 1;
       	 	}
       	 	else
       	 	{
       	 		$flag = 0;
       	 		break;
       	 	}
       	 }
       	 if($flag)
       	 {
       	 	foreach ($collection as $data) 
			{
			 	$yeye = array(
			 		'id_so' =>$id_so,
			 		'id_barang' => $data['barcode'],
			 		'butuh'=> $data['quantity'],
			 		'harga' => $data['price'],
			 		);
			 	$hehe = $this->mpenjualan->GetItem($yeye);
			}
			$header = array(
			 		'id_so' =>$id_so,
			 		'id_petugas' => $this->session->userdata('id_retail'),
			 		'id_customer'=> 0,
			 		'tanggal' => date('Y-m-d'),
			 		'total' => $total,
			 		'kurir' => 1,
			 		'id_pemilik' => $this->session->userdata('id_retail'),
			 		'ongkir' => 0,
					'game_id' => $this->session->userdata('game_id'),
			 		);
			$oke = $this->mpenjualan->sales_header($header);
			foreach ($collection as $data) {
				$data_rop = array(
			 		'id_item' => $data['barcode'],
			 		'id_pemilik' => $this->session->userdata('id_retail'),
			 		'id_so' => $id_so,
			 		);
			 	$rop = $this->mbi->update_rop($data_rop);
			}
		 	if($oke)
			{
		 		echo true;
			}
       	 }
      // if(is_array($collection)) {
      //      print_r($collection);
      //  }
      //  else
      //  {
      //      var_dump($collection);
      //  }
		// echo json_encode(array('succes'=>true,'response'=>$data));
	}

	// public function add()
	// {
	// 	$data = $this->input->post('collection');
	// 	$param = array();
 //        $id_so	= uniqid("SO");
	// 	foreach ($data as $dataItem) 
	// 	{
	// 		array_push(
 //                    $param,
 //                    array(
 //                    	'id_so' =>$id_so,
 //                        'id_barang' => $dataItem['barcode'],
	// 					'butuh' => $dataItem['quantity'],
 //                    )
 //            ); 
	// 	}
	// 	foreach ($param as $dataaa) 
	// 	{
	// 		$hehe = $this->mpenjualan->GetItem($dataaa);	
	// 	}
	// }
}
?>