 <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
       <!-- Content Header (Page header) -->
       <section class="content-header">
         <h1>
           List Aset
         </h1>
         <ol class="breadcrumb">
           <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           <li class="active">Aset</li>
         </ol>
       </section>
 
       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12 col-lg-12">
             <div class="box">
               <div class="box-body table-responsive no-padding">
                 <?php if($this->session->flashdata('pesan')){
                   echo $this->session->flashdata('pesan');
                 } ?>
                 <table id="menu_data" style="width:auto;height:400px"
                       toolbar="#toolbar" idField="asset_id"
                       rownumbers="true" fitColumns="true" singleSelect="true" pagination="true">
                   <thead>
                       <tr>
                          <th field="name" width="30%" editor="textbox">Nama Aset</th>
                          <th field="capacity" width="10%" align="center" formatter="formatPrice" editor="numberbox">Kapasitas Aset</th>
                          <th field="value" width="15%" align="right" formatter="formatPrice" editor="numberbox">Harga Aset</th>
                          <th field="duration" width="15%" align="center" formatter="formatPrice" editor="numberbox">Durasi Pemakaian</th>
                          <th field="duration_type" align="center" width="10%" formatter="formatSatuan" 
                          editor="{type:'combobox',
                                  options:{
                                    valueField:'value',
                                    textField:'label',
                                    data:[{
                                        value: 'y',
                                        label: 'Tahun'
                                      },{
                                        value: 'm',
                                        label: 'Bulan'
                                      }],
                                    panelHeight:'auto',

                                  }}">Satuan Durasi</th>
                          <th field="status" width="10%" align="center" formatter="formatStatus">Status</th>
                          <th field="action" width="10%" align="center" formatter="formatAction">Action</th>
                       </tr>
                   </thead>
                 </table>
                 <div id="toolbar">
                   <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newAset()">New Asset</a> -->
                   <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editAset()">Edit Asset</a>
                   <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyAset()">Delete Aset</a> -->
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#menu_data').edatagrid('addRow')">New Asset</a>
                  <a id="destroy" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#menu_data').edatagrid('destroyRow')">Delete Asset</a>
                  <a id="save" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#menu_data').edatagrid('saveRow')">Save</a>
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#menu_data').edatagrid('cancelRow')">Cancel</a>
                 </div>
               </div><!-- /.box-body -->
             </div><!-- /.box -->
           <div class="row">
             <div class="col-md-12 text-center">
               <?php //echo $paging; ?>
             </div>
           </div>
         </div>
       </div>
     </section>
   </div>
 
   <script type="text/javascript">

    $(function(){
      // var row = $('#menu_data').edatagrid('getSelected');
        $('#menu_data').edatagrid({
            url:'<?php echo base_url() ?>aset/listAset',
            saveUrl: '<?php echo base_url() ?>aset/addAset',
            updateUrl: '<?php echo base_url() ?>aset/updateAset',
            destroyUrl: '<?php echo base_url() ?>aset/delete',
            destroyMsg:{
              norecord:{  // when no record is selected
                title:'Warning',
                msg:'No record is selected.'
              },
              confirm:{ // when select a row
                title:'Confirm',
                msg:'Are you sure you want to delete?'
              }
            },
            onDblClickRow: function(index,row){
              var edName =$(this).datagrid('getEditor',{index:index,field:'name'});
              var edCapacity =$(this).datagrid('getEditor',{index:index,field:'capacity'});
              var edValue =$(this).datagrid('getEditor',{index:index,field:'value'});
              var edDuration =$(this).datagrid('getEditor',{index:index,field:'duration'});
              var edType =$(this).datagrid('getEditor',{index:index,field:'duration_type'});
              if(row.status == 1){
                  $(edName.target).textbox('disable');
                  $(edCapacity.target).numberbox('disable');
                  $(edValue.target).numberbox('disable');
                  $(edDuration.target).numberbox('disable');
                  $(edType.target).combobox('disable');
                  $('#destroy').linkbutton('disable');
              }
            },
            onClickRow: function(index,row){
              if(row.status == 1){
                  $('#destroy').linkbutton('disable');
                  $('#save').linkbutton('disable');
              }
              else{
                  $('#destroy').linkbutton('enable');
                  $('#save').linkbutton('enable');
              }
            },
            // autoSave: true,
        });
    });

    function fix(index){
      $.messager.confirm('Confirm','Are you sure you want to save permanently this aset?',function(r){
        if (r){
          $.post('<?php echo base_url() ?>aset/fix/'+index,'',function(result){
            if (result.success){
                $('#menu_data').datagrid('reload');    // reload the user data
            } else {
                $.messager.show({    // show error message
                    title: 'Error',
                    msg: result.errorMsg
                });
            }
          },'json');
        }
      });
      // console.log(index);
    }

    function formatPrice(val,row){
      var x = parseInt(val);
      return x.toLocaleString('ind');
      // console.log(typeof val);
    }

    function formatSatuan(val,row){
      if (val == 'y') {
        return 'Tahun';
      } else if (val == 'm') {
        return 'Bulan';
      }
      return 'tanpa satuan';
      // console.log(typeof val);
    }

    function formatStatus(val,row){
      if (val == 0) {
        return 'Belum Fix';
      } else if (val == 1) {
        return 'Sudah Fix';
      }
      return 'tidak diketahui';
      // console.log(typeof val);
    }

    function formatAction(value,row,index){
      if (row.status == 0) {
        var e = '<a href="#" onclick="fix('+row.asset_id+')">Fix</a> ';
        return e;
      }
    }


   </script>
      