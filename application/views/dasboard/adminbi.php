<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard BI
    		<?php if($this->session->flashdata('pesan')){
		  		echo $this->session->flashdata('pesan');
  			} ?>
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">BI</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-line-chart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Return on Assets</span>
              <span class="info-box-number"><?php foreach($roa as $item){
                if ($item->totalc!=0) {
                  echo number_format(($item->net/$item->totalc)*100,3);
                } else {
                  echo 0;
                }                
                }?><small>% /Total</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Return on Investment</span>
              <span class="info-box-number"><?php foreach($roi as $item2){
              if ($item2->totalc!=0) {
                 echo number_format(($item2->net/$item2->totalc)*100,3);
               } else {
                 echo 0;
               }
              }?><small>% /Total</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-shopping-cart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Return on Equity</span>
              <span class="info-box-number"><?php foreach($roe as $item3){
              if ($item3->totalc!=0) {
                 echo number_format(($item3->net/$item3->totalc)*100,3);
               } else {
                 echo 0;
               }
                }?><small>% /Total</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-building"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Liability</span>
              <span class="info-box-number"><?php foreach($liability as $item){
                if ($item->credit!=0) {
                  echo number_format(($item->debit/$item->credit)*100,3);
                } else {
                  echo 0;
                }                
                }?><small>% /Total</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- /.col -->
      </div>
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Rangkuman Keuangan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-7">
                  <h4 class="text-left">
                    Status Perusahaan: 
                    <?php if ((($roi[0]->net/$roi[0]->totalc)*100)-(($liability[0]->debit/$liability[0]->credit)*100)<0) {?>
                      <big class="label bg-red">Diperkirakan Bangkrut. Segera Perbaiki!</big>
                  </h4>
                  <div class="col-md-6">
                    <h5>Alasan :</h5>
                    <ul>
                      Kondisi keuangan tidak cukup untuk membayar pinjaman.
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <h5>Saran :</h5>
                    <ul>
                      <li>
                        Naikkan harga barang
                      </li>
                      <li>Naikkan biaya iklan</li>
                      <li>Gunakan Rekomendasi</li>
                    </ul>
                  </div>
                    <?php } elseif ((($roi[0]->net/$roi[0]->totalc)*100)-(($liability[0]->debit/$liability[0]->credit)*100)<=3) {?>
                      <big class="label bg-yellow">Diperkirakan Bertahan Namun Dengan Keuntungan yang Kecil.</big>
                    </h4>
                  <div class="col-md-6">
                    <h5>Alasan :</h5>
                    <ul>
                      Kondisi keuangan cukup untuk membayar pinjaman dengan profit yang tipis.
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <h5>Saran :</h5>
                    <ul>
                      <li>
                        Naikkan sedikit harga barang
                      </li>
                      <li>Naikkan sedikit biaya iklan</li>
                      <li>Gunakan Rekomendasi</li>
                    </ul>
                  </div>
                    <?php } else { ?>
                     <big class="label bg-green">Diperkirakan Bertahan Dengan Keuntungan yang Melimpah</big>
                     </h4>
                  <div class="col-md-6">
                    <h5>Alasan :</h5>
                    <ul>
                      Kondisi keuangan cukup untuk membayar pinjaman dan masih untung banyak.
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <h5>Saran :</h5>
                    <ul>
                      <li>
                        Pertahankan seperti ini.
                      </li>
                    </ul>
                  </div>
                     <?php } ?>

                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-5">
                  <p class="text-center">
                    <strong>Keuangan</strong>
                  </p>

                  <div class="progress-group">
                    <span class="progress-text">Pendapatan Bulan Terakhir</span>
                    <span class="progress-number"><?php echo 'Rp '.number_format($dapat2->total).' (100 %) ' ?></span>

                    <!-- <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                    </div> -->
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Pengeluaran Bulan Ini</span>
                    <span class="progress-number"><?php echo 'Rp '.number_format($expenses->expenses+$hpp2->total).' ('.number_format((($expenses->expenses+$hpp2->total)/$dapat2->total)*100,3).' % ) ' ?></span>

                    <!-- <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width:100%"></div>
                    </div> -->
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Total Net Profit Bulan Ini</span>
                    <span class="progress-number"><?php if ($dapat->total!=0) {
                         echo 'Rp '.number_format($dapat2->total-($expenses->expenses+$hpp2->total)).' ('.number_format((($dapat2->total-($expenses->expenses+$hpp2->total))/$dapat2->total)*100,3).' %)'; 
                        } else {
                          echo 'Rp 0';
                        }
                        ?></span>

                    <!-- <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 100%"></div>
                    </div> -->
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Biaya Iklan Bulan Ini</span>
                    <span class="progress-number"><?php if ($iklan->total!=0) {
                         echo 'Rp '.number_format($iklan->total).' ('.number_format(($iklan->total/$dapat2->total)*100,3).' %)'; 
                        } else {
                          echo 'Rp 0';
                        }
                        ?></span>

                    <!-- <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 100%"></div>
                    </div> -->
                  </div>
                  <!-- /.progress-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header"><?php echo "Rp ".number_format($margin,2); ?></h5>
                    <span class="description-text">AVERAGE MARGINAL COST</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header"><?php if ($hpp->total!=0) {
                         echo number_format((($dapat->total-($expenses->expenses+$hpp->total))/$hpp->total)*100,2);
                        } else {
                          echo 0;
                        }
                        ?>%</h5>
                    <span class="description-text">AVERAGE MARKUP (PROFIT TERHADAP COGS)</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
<!--                     <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span> -->
                    <h5 class="description-header"><?php if ($ped->avg!=0) {
                         echo number_format($ped->avg,3); 
                          // echo number_format($margin->total);
                        } else {
                          echo 0;
                        }
                        ?></h5>
                    <span class="description-text">AVERAGE PRICE ELASTICITY OF DEMAND</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <!-- <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span> -->
                    <h5 class="description-header"><?php if ($ped->avg>=0) {
                         echo "perfectly inelastic";
                        } elseif ($ped->avg<0 && $ped->avg> -1) {
                          echo "inelastic";
                        } elseif ($ped->avg==-1) {
                          echo "unitary elastic";
                        } elseif ($ped->avg<-1) {
                          echo "elastic";
                        } else {
                          echo "perfectly elastic";
                        }
                        ?></h5>
                    <span class="description-text">AVERAGE ELASTICITY</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">
          <!-- BAR CHART -->
  			  <div class="box box-success">
            <!-- <div id="chartContainer1" style="width: 90%; height: 300px;display: inline-block;"></div> -->
            <!-- <canvas id="myChart"></canvas> -->
    				<div class="box-header with-border">
    				  <h3 class="box-title">Total Pendapatan</h3>
              <br />
              <!-- <div id="chartContainer3" style="width: 45%; height: 300px;display: inline-block;"></div>
              <div id="chartContainer4" style="width: 45%; height: 300px;display: inline-block;"></div> -->
    				  <!-- <div class="box-tools pull-right"> -->
    					
    				  <!-- </div> -->
    				</div>
    				<div class="box-body">
    				  <div class="chart">
      					<canvas id="myChart"></canvas>
    				  </div>
    				</div>
  				  <!-- /.box-body -->
  			  </div>
          <!-- <div class="box box-success">
          <div id="chartContainer2" style="width: 90%; height: 300px;display: inline-block;"></div></div> -->
            <!-- /.box -->
        </section><!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-6 connectedSortable">
        <!-- AREA CHART -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Total Profit</h3>
              <br />
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="myChart2"></canvas>
              </div>
            </div>
          </div>
				  <!-- <div class="box box-primary">
  					<div class="box-header with-border">
  					  <h3 class="box-title">Limit Stok</h3>

  					  <div class="box-tools pull-right">
  							
  					  </div>
  					</div>
  					<div class="box-body">
  					  <div class="">
                <table class="table">
  							<?php 
                // if(!empty($alert)){
  						  // foreach($alert as $baris){ ?>
                  <tr>
    								<td>
    									<?php 
                      // echo $baris->nama_item; ?>
    								</td>
  								  <td>
                      <span class="label label-warning"><?php 
                      // echo $baris->jumlah; ?></span>
  								  </td>
  						    </tr>
  							<?php 
                // }} ?>
  							</table>
  					  </div>
  				  </div>
					</div> -->
			  </section><!-- right col -->
      </div>

      <div class="row">
        <div class="col-md-6">
          <!-- DIRECT CHAT -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">List Pendapatan Tertinggi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="rev" class="easyui-datagrid" style="width:auto;" 
                url="<?php echo base_url() ?>bi/list_data_revenue"
                pagination="false" idField="id_item"
                rownumbers="true" fitColumns="true">
                <thead>
                  <tr>
                    <th field="nama_item" width="50%">Nama Item</th>
                    <th field="total" width="25%" formatter="formatPrice" >Total Terjual</th>
                    <th field="dapat" width="25%" formatter="formatPrice" >Pendapatan</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <!--/.direct-chat -->
        </div>
        <!-- /.col -->

        <div class="col-md-6">
          <!-- USERS LIST -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">List Profit Tertinggi</h3>
            </div>
            <div class="box-body">
              <table id="rev" class="easyui-datagrid" style="width:auto;" 
                url="<?php echo base_url() ?>bi/list_data_profit"
                pagination="false" idField="id_item"
                rownumbers="true" fitColumns="true">
                <thead>
                  <tr>
                    <th field="nama_item" width="50%">Nama Item</th>
                    <th field="total" width="25%" formatter="formatPrice" >Total Terjual</th>
                    <th field="profit" width="25%" formatter="formatPrice" >Profit</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <!--/.box -->
        </div>
        <!-- /.col -->
      </div>

      <div class="row">
        <!-- Left col -->
        <section class="col-lg-8 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <!-- <div class="nav-tabs-custom"> -->
            <!-- Tabs within a box -->
            <!-- <ul class="nav nav-tabs pull-right">
              <li class="pull-left header"><i class="fa fa-inbox"></i> Grafik Forecast Penjualan</li>
            </ul> -->
            <!-- <div class="tab-content no-padding"> -->
              <!-- Morris chart - Sales -->
              <!-- <div style="position: relative; height: 300px;">
                <div class="box-body">
                 	
                </div>
              </div>
            </div>
          </div> -->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="pull-left header"><i class="fa fa-inbox"></i> Rangkuman Keuangan</li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div style="position: relative; height: 300px;">
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <tbody>
                      <tr>
                        <td>Pendapatan</td>
                        <td><?php echo 'Rp '.number_format($dapat->total).' (100 %) ' ?></td>
                      </tr>
                      <tr>
                        <td>Pengeluaran</td>
                        <td><?php echo 'Rp '.number_format($expenses->expenses+$hpp->total).' ('.number_format((($expenses->expenses+$hpp->total)/$dapat->total)*100,3).' % ) ' ?></td>
                      </tr>
                      <tr>
                        <td>Total Net Profit (After Tax)</td>
                        <td><?php if ($dapat->total!=0) {
                         echo 'Rp '.number_format($dapat->total-($expenses->expenses+$hpp->total)).' ('.number_format((($dapat->total-($expenses->expenses+$hpp->total))/$dapat->total)*100,3).' %)'; 
                        } else {
                          echo 'Rp 0';
                        }
                        ?></td>
                      </tr>
                      <tr>
                        <td>Rekomendasi Budget Iklan Minimal</td>
                        <td><?php $rev = $dapat->total;
                              $markup = (($dapat->total-($expenses->expenses+$hpp->total))/$hpp->total);
                              $min_iklan = (0.05*$rev)*$markup;
                              echo 'Rp '.number_format($min_iklan) ?></td>
                      </tr>
                      <tr>
                        <td>Rekomendasi Budget Iklan Maksimal</td>
                        <td><?php $rev = $dapat->total;
                              $markup = (($dapat->total-($expenses->expenses+$hpp->total))/$hpp->total);
                              $min_iklan = (0.1*$rev)*$markup;
                              echo 'Rp '.number_format($min_iklan) ?></td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
              </div>
            </div>
          </div>
          <!-- /.nav-tabs-custom -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-4 connectedSortable">

          <!-- Map box -->
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-tag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Average Marginal Cost</span>
              <span class="info-box-number"><?php echo "Rp ".number_format($margin,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-money"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Average Markup<br>(Profit terhadap COGS)</span>
              <span class="info-box-number"><?php if ($hpp->total!=0) {
                         echo number_format((($dapat->total-($expenses->expenses+$hpp->total))/$hpp->total)*100,2);
                        } else {
                          echo 0;
                        }
                        ?>%</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-line-chart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Average Price Elasticity of Demand</span>
              <span class="info-box-number"><?php if ($ped->avg!=0) {
                         echo number_format($ped->avg,3); 
                          // echo number_format($margin->total);
                        } else {
                          echo 0;
                        }
                        ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Average Elasticity</span>
              <span class="info-box-number"><?php if ($ped->avg==0) {
                         echo "perfectly inelastic";
                        } elseif ($ped->avg<0 && $ped->avg> -1) {
                          echo "inelastic";
                        } elseif ($ped->avg==-1) {
                          echo "unitary elastic";
                        } elseif ($ped->avg<-1) {
                          echo "elastic";
                        } else {
                          echo "perfectly elastic";
                        }
                        ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.box -->
        </section>
        <!-- right col -->
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
	  <!-- ChartJS 1.0.1 -->
  <script src="<?php echo base_url() ?>assets/js/Chart.min.js"></script>
  <script>
    $(document).ready(function(){
		var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ['','<?php if(!empty($revenue)){
                  foreach($revenue as $item){
              echo $item->month.' - '.$item->tahun; ?>',
              '<?php }} else {
                echo " ";
              }?>'],
        datasets: [{
          label: 'Pendapatan (Rp)',
          data: [0,<?php if(!empty($revenue)){
                  foreach($revenue as $item){
              echo $item->dapat ?>,
              <?php }} else {
                echo 0;
              } ?>],
          backgroundColor: "green",
          borderColor: "green",
        }]
      }
    });

    var ctx2 = document.getElementById('myChart2').getContext('2d');
    var myChart2 = new Chart(ctx2, {
      type: 'bar',
      data: {
        labels: ['','<?php if(!empty($profit)){
                  foreach($profit as $item){
              echo $item->month.' - '.$item->tahun; ?>',
              '<?php }} else {
                echo " ";
              }?>'],
        datasets: [{
          label: 'Total Profit (Rp)',
          data: [0,<?php if(!empty($profit)){
                  foreach($profit as $item){
              echo $item->profit ?>,
              <?php }} else {
                echo 0;
              } ?>],
          backgroundColor: "orange",
          borderColor: "orange",
        }]
      }
    });
   
    });
    function formatPrice(val,row){
      var x = parseInt(val);
      return x.toLocaleString('ind');
      // console.log(typeof val);
    };
  </script>