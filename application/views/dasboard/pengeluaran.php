 <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
       <!-- Content Header (Page header) -->
       <section class="content-header">
         <h1>
           Biaya Operasional Bulanan
         </h1>
         <ol class="breadcrumb">
           <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           <li class="active">Operasional</li>
         </ol>
       </section>
 
       <!-- Main content -->
       <section class="content">
         <div class="row">
           <div class="col-xs-12 col-lg-12">
             <div class="box">
               <div class="box-body table-responsive no-padding">
                 <?php if($this->session->flashdata('pesan')){
                   echo $this->session->flashdata('pesan');
                 } ?>
                  <div class="easyui-panel" title="Biaya Operasional" style="width:100%;">
                    <form id="ff" method="post">
                        <div style="margin-bottom:20px; margin-top: 20px; margin-left: 20px">
                            <input class="easyui-numberbox" name="iklan" style="width:95%" groupSeparator="." data-options="label:'Iklan:',required:true, prompt:'Biaya Iklan'">
                        </div>
                        <div style="margin-bottom:20px; margin-top: 20px; margin-left: 20px">
                            <input class="easyui-numberbox" name="jumlah_pekerja" style="width:30%" data-options="label:'Pekerja:',required:true, prompt:'Banyak Pekerja'">
                            <input class="easyui-numberbox" name="gaji_pekerja" style="width:65%" groupSeparator="." data-options="required:true, prompt:'Gaji Pekerja'">
                        </div>
                        <div style="margin-bottom:20px; margin-top: 20px; margin-left: 20px">
                            <input class="easyui-numberbox" name="air" style="width:95%" groupSeparator="." data-options="label:'Air:',required:true, prompt:'Biaya Air'">
                        </div>
                        <div style="margin-bottom:20px; margin-top: 20px; margin-left: 20px">
                            <input class="easyui-numberbox" name="listrik" style="width:95%" groupSeparator="." data-options="label:'Listrik:',required:true, prompt:'Biaya Listrik'">
                        </div>
                        <div style="margin-bottom:20px; margin-top: 20px; margin-left: 20px">
                            <input class="easyui-numberbox" name="asuransi" style="width:95%" groupSeparator="." data-options="label:'Asuransi:', prompt:'Biaya Asuransi'" >
                        </div>
                        <div style="margin-bottom:20px; margin-top: 20px; margin-left: 20px">
                            <input class="easyui-numberbox" name="depresiasi" style="width:95%" groupSeparator="." data-options="label:'Depresiasi:', prompt:'Biaya Depresiasi'" >
                        </div>
                        <div style="margin-bottom:20px; margin-top: 20px; margin-left: 20px">
                            <input class="easyui-numberbox" name="amortisasi" style="width:95%" groupSeparator="." data-options="label:'Amortisasi:', prompt:'Biaya Amortisasi'" >
                        </div>
                      </form>
                    <div style="text-align:center;padding:5px 0">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">Submit</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</a>
                    </div>
                </div>
               </div><!-- /.box-body -->
             </div><!-- /.box -->
           <div class="row">
             <div class="col-md-12 text-center">
               <?php //echo $paging; ?>
             </div>
           </div>
         </div>
       </div>
     </section>
   </div>
 
   <script type="text/javascript">

    $(function(){
      // var row = $('#menu_data').edatagrid('getSelected');
        $('#ff').form('load', '<?php echo base_url() ?>expense/listExpense');
    });

    function fix(index){
      $.messager.confirm('Confirm','Are you sure you want to save permanently this expenses?',function(r){
        if (r){
          $.post('<?php echo base_url() ?>aset/fix/'+index,'',function(result){
            if (result.success){
                $('#menu_data').datagrid('reload');    // reload the user data
            } else {
                $.messager.show({    // show error message
                    title: 'Error',
                    msg: result.errorMsg
                });
            }
          },'json');
        }
      });
      // console.log(index);
    }

    function formatPrice(val,row){
      var x = parseInt(val);
      return x.toLocaleString('ind');
      // console.log(typeof val);
    }

    function formatSatuan(val,row){
      if (val == 'y') {
        return 'Tahun';
      } else if (val == 'm') {
        return 'Bulan';
      }
      return 'tanpa satuan';
      // console.log(typeof val);
    }

    function formatStatus(val,row){
      if (val == 0) {
        return 'Belum Fix';
      } else if (val == 1) {
        return 'Sudah Fix';
      }
      return 'tidak diketahui';
      // console.log(typeof val);
    }

    function formatAction(value,row,index){
      if (row.status == 0) {
        var e = '<a href="#" onclick="fix('+row.asset_id+')">Fix</a> ';
        return e;
      }
    }

    function submitForm(){
        jQuery('#ff').form('submit',{
            url: '<?php echo base_url() ?>expense/update',
            onSubmit: function(){
                return jQuery(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                    $.messager.alert({
                        title: 'Berhasil',
                        msg: 'Berhasil memasukkan data!',
                        icon: 'info'
                    });
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
        });
    }

   </script>
      