  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Neraca Lajur            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Keuangan</a></li>
            <li class="active">Neraca Lajur</li>
          </ol>
        </section>
      
        <!-- Main content -->
        <section class="content">
         
          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <div class="box">
                <div class="box-header">
                   <div class="row">
					<div class="co-lg-12">
                   <form method="post" action="<?php echo base_url(),"gl/neracaLajur" ?>" enctype="multipart/form-data">
				   <div class="col-lg-1">
						<label for="exampleInputEmail1">Filter</label>                   
					</div>
                    <div class="col-lg-2">
						<input name="tgl_awal" type="text" class="form-control datepicker"  placeholder="Periode" data-date-format="yyyy-mm" required>
                    </div>
					<!-- <div class="col-lg-2">
						<input name="tgl_akhir" type="text" class="form-control datepicker"  placeholder="Tanggal akhir" data-date-format="yyyy-mm-dd" required>
                    </div> -->
					<div class="col-lg-1">
						 <button type="submit" class="btn btn-primary">Filter</button>
					</div>
					 </form>
					 <div class="col-lg-1">
					 <form method="post" action="<?php echo base_url(),"gl/printBsheet" ?>" enctype="multipart/form-data">
					 <input name="tgl_awal" type="hidden" value="<?php echo $tgl_tampil; ?>">
					 <!-- <input name="tgl_akhir" type="hidden"  value="<?php echo $tgl_akhir; ?>">  -->
						 <button type="submit" class="btn btn-info"><li class="fa fa-print"></li>Print</button>
					</form>
					 </div>
                  </div>
               </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                <h4>Periode : <?php echo $tgl_tampil; ?></h4>
                  <table class="table table-hover table-striped">
                    <thead>
                      <tr>
                        <th rowspan="2"><b>NAMA AKUN</b></th>
                        <th colspan="2">NERACA SALDO</th>
                        <th colspan="2">LABA RUGI</th>
                        <th colspan="2">NERACA</th>
                      </tr>
                      <tr>
                        <th>Debit</th>
                        <th>Kredit</th>
                        <th>Debit</th>
                        <th>Kredit</th>
                        <th>Debit</th>
                        <th>Kredit</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- neraca saldo -->
                      <?php 
                        $total_crd_ns=0;
                        $total_dbt_ns=0;
                        $total_crd_lr=0;
                        $total_dbt_lr=0;
                        $total_crd_nl=0;
                        $total_dbt_nl=0;                        
                          if(!empty($neraca)){ 
                            foreach ($neraca as $rows){
                          ?>
                      <tr>
                        <td><?php echo $rows->acc_id.' --- '.$rows->nama; ?></td>
                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt,0,'','.').",-";?></td>
                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_crd,0,'','.').",-";?></td>
                        <td></td>
                        <td></td>
                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt,0,'','.').",-";
                                                        $total_dbt_nl=$total_dbt_nl+($rows->total_dbt)?></td>
                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_crd,0,'','.').",-";
                                                        $total_crd_nl=$total_crd_nl+($rows->total_crd)?></td>
                      </tr>
                        <?php $total_dbt_ns=$total_dbt_ns+($rows->total_dbt);
                              $total_crd_ns=$total_crd_ns+($rows->total_crd);
                        }}
                          if(!empty($laba)){ 
                            foreach ($laba as $rows){
                        ?>
                      <tr>
                        <td><?php echo $rows->acc_id.' --- '.$rows->nama; ?></td>
                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt,0,'','.').",-";?></td>
                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_crd,0,'','.').",-";?></td>
                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt,0,'','.').",-";
                                                        $total_dbt_lr=$total_dbt_lr+($rows->total_dbt)?></td>
                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_crd,0,'','.').",-";
                                                        $total_crd_lr=$total_crd_lr+($rows->total_crd)?></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <?php $total_dbt_ns=$total_dbt_ns+($rows->total_dbt);
                            $total_crd_ns=$total_crd_ns+($rows->total_crd);
                       }} ?>    
                      <tr>
                        <td><b><i>Laba Bersih</i></b></td>
                        <td></td>
                        <td></td>
                        <?php if ($total_dbt_lr-$total_crd_lr<0) { ?>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_crd_lr-$total_dbt_lr,0,'','.').",-";?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format(0,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format(0,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_crd_lr-$total_dbt_lr,0,'','.').",-";
                                                            $total_crd_nl=$total_crd_nl+($total_crd_lr-$total_dbt_lr);
                                                            $total_dbt_lr=$total_dbt_lr+($total_crd_lr-$total_dbt_lr); ?></i></b></td>
                        <?php } elseif ($total_dbt_lr-$total_crd_lr>0) { ?>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format(0,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_dbt_lr-$total_crd_lr,0,'','.').",-";?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_dbt_lr-$total_crd_lr,0,'','.').",-";
                                                            $total_dbt_nl=$total_dbt_nl+($total_dbt_lr-$total_crd_lr);
                                                            $total_crd_lr=$total_crd_lr+($total_dbt_lr-$total_crd_lr); ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format(0,0,'','.').",-"; ?></i></b></td>
                        <?php } else {?>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format(0,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format(0,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format(0,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format(0,0,'','.').",-"; ?></i></b></td>
                        <?php } ?>
                      </tr>
                      <tr>
                        <td><b><i>Jumlah</i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_dbt_ns,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_crd_ns,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_dbt_lr,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_crd_lr,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_dbt_nl,0,'','.').",-"; ?></i></b></td>
                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_crd_nl,0,'','.').",-"; ?></i></b></td>
                      </tr>
                    </tbody>    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     