<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          List Company
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Company</a></li>
          <li class="active">List Company</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <table id="company" class="easyui-datagrid" style="width:auto;height:400px"
                      url="<?php echo base_url() ?>company/data"
                      toolbar="#toolbar"
                      rownumbers="true" fitColumns="true" singleSelect="true" pagination="true">
                  <thead>
                      <tr>
                          <th field="company_code" width="50">Company Code</th>
                          <th field="company_name" width="50">Company Name</th>
                          <th field="company_address" width="50">Company Address</th>
                      </tr>
                  </thead>
                </table>
                <div id="toolbar">
                  <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newCompany()">New Company</a>
                  <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editComp()">Edit Company</a>
                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Company</a> -->
                </div>
                <div id="editCompany" class="easyui-dialog" style="width:400px; height:300px; padding: 10px 20px" closed="true" buttons="#dialog-buttons">
                  <form id="form" method="post" novalidate>
                      <div class="form-item">
                          <label for="type" style="font-size: 16px; margin-top: 10px">Company Code</label><br />
                          <input type="text" name="company_code" class="easyui-validatebox" required="true" size="53" maxlength="50" />
                      </div>
                      <div class="form-item">
                          <label for="type" style="font-size: 16px; margin-top: 10px">Company Name</label><br />
                          <input type="text" name="company_name" class="easyui-validatebox" required="true" size="53" maxlength="50" />
                      </div>
                      <div class="form-item">
                          <label for="type" style="font-size: 16px; margin-top: 10px">Company Address</label><br />
                          <input type="text" name="company_address" class="easyui-validatebox" required="true" size="53" maxlength="50" />
                      </div>
                  </form>
              </div>
               
              <!-- Dialog Button -->
              <div id="dialog-buttons">
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Simpan</a>
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#editCompany').dialog('close')">Batal</a>
              </div>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">
    function newCompany(){
      $('#editCompany').dialog('open').dialog('setTitle','New Company');
      $('#form').form('clear');
      url = '<?php echo base_url() ?>company/insert';
    }

    function editComp(){
      var row = $('#company').datagrid('getSelected');
      if (row){
          $('#editCompany').dialog('open').dialog('setTitle','Edit Company');
          $('#form').form('load',row);
          url = '<?php echo base_url() ?>company/update/'+row.id;
      }
    }

    function save(){
        jQuery('#form').form('submit',{
            url: url,
            onSubmit: function(){
                return jQuery(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                    jQuery('#editCompany').dialog('close');
                    jQuery('#company').datagrid('reload');
                    $.messager.alert({
                        title: 'Berhasil',
                        msg: 'Berhasil memasukkan data!',
                        icon: 'info'
                    });
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
        });
    }

    function destroyUser(){
        var row = $('#dg').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to destroy this user?',function(r){
                if (r){
                    $.post('destroy_user.php',{id:row.id},function(result){
                        if (result.success){
                            $('#dg').datagrid('reload');    // reload the user data
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                }
            });
        }
    }
  </script>
     