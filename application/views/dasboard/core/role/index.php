<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          List Roles
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Roles</a></li>
          <li class="active">List Roles</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <table id="role_data" class="easyui-datagrid" style="width:auto;height:400px"
                      url="<?php echo base_url() ?>role/data"
                      toolbar="#toolbar"
                      rownumbers="true" fitColumns="true" singleSelect="true" pagination="true">
                  <thead>
                      <tr>
                          <th field="role" width="50">Role Name</th>
                      </tr>
                  </thead>
                </table>
                <div id="toolbar">
                  <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newRole()">New Role</a>
                  <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editRole()">Edit Role</a>
                  <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editPrivilege()">Edit Privilege</a>

                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Company</a> -->
                </div>
                <div id="formRole" class="easyui-dialog" style="width:400px; height:300px; padding: 10px 20px" closed="true" buttons="#dialog-buttons">
                  <form id="form" method="post" novalidate>
                      <div class="form-item">
                          <label for="type" style="font-size: 16px; margin-top: 10px">Role Name</label><br />
                          <input type="text" name="role" class="easyui-validatebox" required="true" size="53" maxlength="50" />
                      </div>
                  </form>
              </div>
              <div id="formPrivilege" class="easyui-dialog" style="width:360px; height:400px; padding: 10px 20px" closed="true" buttons="#dialog-buttons2">
                <form id="#form2" method="post">
                  <table id="privilege" title="Checkbox Select" class="easyui-datagrid" style="width:300px;height:350px"
                    idField="id" pagination="true"
                    iconCls="icon-save">
                </table>
              </div>
               
              <!-- Dialog Button -->
              <div id="dialog-buttons">
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Simpan</a>
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#formRole').dialog('close')">Batal</a>
              </div>

              <div id="dialog-buttons2">
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="savePrivilege()">Simpan</a>
              </div>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">
    function newRole(){
      $('#formRole').dialog('open').dialog('setTitle','New Role');
      $('#form').form('clear');
      url = '<?php echo base_url() ?>role/insert';
    }

    function editRole(){
      var row = $('#role_data').datagrid('getSelected');
      if (row){
          $('#formRole').dialog('open').dialog('setTitle','Edit Role');
          $('#form').form('load',row);
          url = '<?php echo base_url() ?>role/update/'+row.roleId;
      }
    }


    function editPrivilege()
    {
      var role = $('#role_data').datagrid('getSelected');   
       $('#formPrivilege').dialog('open').dialog('setTitle','New Role');
      // $('#form').form('clear');
      $('#privilege').datagrid({
          url:'<?php echo base_url() ?>Menu/listAllMenu',
          columns:[[
              {field:'access',title:'Checkbox',width:100,checkbox:'true'},
              {field:'name',title:'Name',width:250},
          ]],
          onLoadSuccess: function (data) {
              for (i = 0; i < data.rows.length; ++i) {
                  if (data.rows[i]['access'] == 1) $(this).datagrid('checkRow', i);
              }
          }


      });
      url = '<?php echo base_url() ?>Menu/addPrivilege_act/'+role.roleId;
    }

    function savePrivilege() 
    {
      var rows = [];
      var dg = $('#privilege');
      $.map(dg.datagrid('getChecked'), function(row){
        // var index = dg.datagrid('getRowIndex', row);
        // var editors = dg.datagrid('getEditors', index);
        // if (editors.length){
          rows.push({
            privilege_list: row.id,
          });
        // }
      });
      $.ajax({
        // url:'<?php echo base_url() ?>menu/addPrivilege_act/'.role.roleId,
        url : url,
        type: 'POST',
        data: {data: JSON.stringify(rows)},
        cache: false,
        success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                    jQuery('#formPrivilege').dialog('close');
                    $.messager.alert({
                        title: 'Berhasil',
                        msg: 'Berhasil memasukkan data!',
                        icon: 'info'
                    });
                    window.location.replace('<?php echo base_url(); ?>role');
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
      });
    }


    function save(){
        jQuery('#form').form('submit',{
            url: url,
            onSubmit: function(){
                return jQuery(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                    jQuery('#formRole').dialog('close');
                    jQuery('#role_data').datagrid('reload');
                    $.messager.alert({
                        title: 'Berhasil',
                        msg: 'Berhasil memasukkan data!',
                        icon: 'info'
                    });
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
        });
    }

    function destroyUser(){
        var row = $('#dg').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to destroy this user?',function(r){
                if (r){
                    $.post('destroy_user.php',{id:row.id},function(result){
                        if (result.success){
                            $('#dg').datagrid('reload');    // reload the user data
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg
                            });
                        }
                    },'json');
                }
            });
        }
    }
  </script>
     