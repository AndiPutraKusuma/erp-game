  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Add Menu
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><i class="fa fa-list"></i> Menu</li>
            <li class="active">Add Menu</li>
          </ol>
        </section>
      
        <!-- Main content -->
        <section class="content">
         
          <div class="row">
			<div class="col-md-6">
				 <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Menu</h3>
				  
				<?php if($this->session->flashdata('pesan')){
					  echo $this->session->flashdata('pesan');
				  } ?>
               
				
				<!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?php echo base_url() ?>Menu/addMenu_act" method="post" enctype="multipart/form-data">
                  <div class="box-body">
					           <div class="form-group">
                        <label>Parent</label>
                        <select name="parent" class="form-control select2" style="width: 100%;">
                           <option>- Pilih Salah Satu -</option>
                           <option value="0">Null</option>
                            
                            <?php foreach ($menu as $data) 
                            { ?>
                              <option value="<?php echo $data->menu_id ?>"><?php echo $data->name ?></option>
                              
                            <?php  } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Menu</label>
                        <input name="nama" type="text" class="form-control" placeholder="Nama Menu" required>
                    </div>
                    <div class="form-group">
                        <label>Link</label>
                        <input name="link" type="text" class="form-control" placeholder="Controller/Method , Ex : Modal/listModal" required>
                    </div>
                    <div class="form-group">
                        <label>Icon</label>
                        <input name="icon" type="text" class="form-control" placeholder="ex : fa fa-globe" required>
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

			</div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     