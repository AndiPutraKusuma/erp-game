<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Trend Penjualan Barang Bulanan
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">BI</a></li>
          <li class="active">Trend Penjualan Barang Bulanan</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <p id="judul"></p>
                <table id="trend" class="easyui-treegrid" style="width:auto;" 
                  title="Trend Penjualan Bulanan"
                  url="<?php echo base_url() ?>gl/bulanan_data"
                  pagination="false" idField="code" toolbar="#toolbar" method="get"
                  rownumbers="true" fitColumns="true" treeField="code">
                  <thead>
                    <tr>
                      <th field="code" width="15%">Kode</th>
                      <th field="name" width="25%">Nama</th>
                      <th id="bulan1" field="bulan1" width="20%" formatter="formatPrice">Bulan Sebelumnya</th>
                      <th id="bulan2" field="bulan2" width="20%" formatter="formatPrice">Bulan Ini</th>
                      <th id="bulan3" field="bulan3" width="20%" formatter="formatPrice">Bulan Selanjutnya</th>
                    </tr>
                  </thead>
                </table>  
                <div id="toolbar">
                  <span>Periode :</span>
                    <input id="tgl" type="text" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" style="line-height:26px;border:1px solid #ccc">
                  <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="doSearch()">Search</a>

                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Company</a> -->
                </div>
               
              <!-- Dialog Button -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">

    function doSearch(){
      $('#judul').text('Laporan Laba Rugi Periode : '+ $('#tgl').val());
      $('#trend').treegrid('load',{
        tanggal: $('#tgl').val(),
      });
      console.log($('#tgl').val());
    }

    function formatPrice(val,row){
      var x = parseInt(val);
      return x.toLocaleString('ind');
      // console.log(typeof val);
    }

    function myformatter(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        return y+'-'+m;
    }
    function myparser(s){
        if (!s) return new Date();
        var ss = (s.split('-'));
        var y = parseInt(ss[0],10);
        var m = parseInt(ss[1],10);
        // console.log(m);
        // var d = parseInt(ss[2],10);
        if (!isNaN(y) && !isNaN(m)){
            return new Date(y,m-1);
        } else {
            return new Date();
        }
    }

  </script>