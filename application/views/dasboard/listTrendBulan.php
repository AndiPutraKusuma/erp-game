<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Trend Penjualan Barang Bulanan
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">BI</a></li>
          <li class="active">Trend Penjualan Barang Bulanan</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <p id="judul"></p>
                <table id="trend" class="easyui-datagrid" style="width:auto;" 
                  title="Trend Penjualan Bulanan"
                  url="<?php echo base_url() ?>bi/trendBarangBulan_data"
                  pagination="false" idField="id_item" toolbar="#toolbar" method="get"
                  rownumbers="true" fitColumns="true" singleSelect="false">
                  <thead>
                    <tr>
                      <th field="id_item" width="10%">Id Item</th>
                      <th field="nama_item" width="20%">Nama Item</th>
                      <th field="total" width="10%" formatter="formatPrice" sortable="true">Total Terjual</th>
                      <th field="hj" width="15%" formatter="formatPrice">Harga Jual</th>
                      <th field="dapat" width="15%" formatter="formatPrice" sortable="true">Pendapatan</th>
                      <th field="hb" width="15%" formatter="formatPrice">Harga Beli</th>
                      <th field="profit" width="15%" formatter="formatPrice" sortable="true">Profit</th>
                    </tr>
                  </thead>
                </table>  
                <div id="toolbar">
                  <span>Periode :</span>
                    <input id="tgl" type="text" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" style="line-height:26px;border:1px solid #ccc">
                  <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="doSearch()">Search</a>

                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Company</a> -->
                </div>
               
              <!-- Dialog Button -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">

    function doSearch(){
      $('#judul').text('Trend Bulanan Periode : '+ $('#tgl').val());
      $('#trend').datagrid('load',{
        tanggal: $('#tgl').val(),
      });
      console.log ($('#tgl').val());
    }

    function formatPrice(val,row){
      var x = parseInt(val);
      return x.toLocaleString('ind');
      // console.log(typeof val);
    }

    function myformatter(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        return y+'-'+m;
    }
    function myparser(s){
        if (!s) return new Date();
        var ss = (s.split('-'));
        var y = parseInt(ss[0],10);
        var m = parseInt(ss[1],10);
        // console.log(m);
        // var d = parseInt(ss[2],10);
        if (!isNaN(y) && !isNaN(m)){
            return new Date(y,m-1);
        } else {
            return new Date();
        }
    }

  </script>