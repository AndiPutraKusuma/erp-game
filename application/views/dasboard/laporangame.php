  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Ranking Game
            
          </h1>
        
        </section>
      
        <!-- Main content -->
        <section class="content">
         
          <div class="row">
            <div class="col-xs-12 col-lg-10">
              <div class="box">
                <div class="box-header">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover table-striped">
                                <?php $i=0; 
                                foreach ($row as $datas) {  $i++; ?>
                                        <thead>
                                            <tr>
                                            <th><b><i>Rank #<?php echo $i; ?></i></b></th></tr>
                                        </thead>
                                        <tbody>
                                            <th bgcolor="gray"><b><?php echo $datas['id_pemilik']; ?></b></th><th bgcolor="gray"></th>
                                              <tr>
                                                  <td>Pendapatan</td>
                                                    <td style="text-align:right;"><?php echo "Rp ".number_format($datas['revenue'],0,'','.').",-"; ?></td>
                                              </tr>                                            
                                              <tr>
                                                <td>Keuntungan</td>
                                                <td style="text-align:right;"><?php echo "Rp ".number_format($datas['profit'],0,'','.').",-"; ?></td>
                                              </tr>
                                         
                                      </tbody>    
                                <?php } ?>
                                      
                                       
                                    </table>
                                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     