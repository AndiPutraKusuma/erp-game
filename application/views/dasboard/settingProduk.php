<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Pengaturan Produk
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Master Data</a></li>
          <li class="active">Pengaturan produk</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <table id="schema_line" title="Pengaturan Produk" style="width:100%;height:auto"
                  toolbar="#tb" pagination="true" idField="id_item"
                  rownumbers="true" fitColumns="true" singleSelect="true">
                  <thead>
                    <tr>
                      <th field="nama_item" width="15%">Nama</th>
                      <!-- th field="stock" width="10%">Stock</th-->
                      <th field="hb" width="6%" formatter="formatPrice" >Harga Beli</th>
                      <th id="profit" field="profit" width="6%" formatter="formatPersen" editor="{type:'numberspinner',options:{
                            min:-100, max:100, suffix:'%'}
                          }">Profit (%)</th>
                      <th field="harga" width="6%" formatter="formatPrice" >Harga Jual</th>
                      <th field="iklan" width="6%" formatter="formatPersen" editor="{type:'numberspinner',options:{
                            max:100, suffix:'%'}
                          }">Iklan</th>
                      <th field="pesan" width="8%" formatter="formatPrice" editor="numberbox">Jumlah Pesan</th>
                      <th field="supplier" width="8%" 
                          editor="{type:'combobox',options:{
                            valueField:'id_suplier', 
                            textField:'nama_suplier', 
                            url:'<?php echo base_url() ?>order/getSupp', 
                            panelHeight:'auto'}
                          }">
                        Supplier</th>
                      <th field="rop_set" width="7%" formatter="formatPrice" editor="numberbox">Batas Stock</th>
                      <th field="action_profit" width="10%" align="center" formatter="formatAction_profit">Recomended Profit</th>
                      <th field="action_eoq" width="10%" align="center" formatter="formatAction_eoq">Recomended Jumlah</th>
                      <th field="action_sup" width="10%" align="center" formatter="formatAction_supp">Recomended Supp</th>
                      <th field="action_rop" width="10%" align="center" formatter="formatAction_rop">Recomended Batas</th>
                      <th field="rop" align="center" hidden="true">ROP</th>
                      <th field="eoq" align="center" hidden="true">EOQ</th>
                      <th field="het" align="center" hidden="true">HET</th>

                    </tr>
                  </thead>
                </table>               
              <div id="tb" style="height:auto">
                <!-- <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#schema_line').edatagrid('addRow')">New</a> -->
                <!-- <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#schema_line').edatagrid('destroyRow')">Destroy</a> -->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#schema_line').edatagrid('saveRow');setTimeout($('#schema_line').edatagrid('reload'), 2000); ">Save</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#schema_line').edatagrid('cancelRow')">Cancel</a>
              </div>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">

    $(function(){
      // var row = $('#schema_data').datagrid('getSelected');
        $('#schema_line').edatagrid({
            url: '<?php echo base_url() ?>Produk/setproduk_data',
            // saveUrl: '<?php //echo base_url() ?>gl/schema_insert/'+row.id,
            updateUrl: '<?php echo base_url() ?>produk/config',

            // destroyUrl: '<?php //echo base_url() ?>gl/schema_delete',
        });
    });

    function formatPrice(val,row){
      var x = parseInt(val);
      return x.toLocaleString('ind');
      // console.log(typeof val);
    }

    function formatPersen2(val,row, index){
      var n = $('td#profit');
      var woq = $('#schema_line').edatagrid('getRows')[index].het; 
      // n.numberbox('options').max = woq;
      // n.numberspinner('setValue', n.numberspinner('getValue'));
      var x = Math.round(val);
      //return x+"%";
      console.log(woq);
    }

    function formatPersen(val,row){
      var x = Math.round(val);
      return x+"%";
      // console.log(typeof val);
    }

    function formatAction_supp(value,row,index){
      var v = $('#schema_line').edatagrid('getRows')[index].rop;
      if (row.supplier == row.suppsel) {
      	var e = '<a href="#"><span class="btn btn-primary btn-xs disabled">Menyala</span></a>';	
      } else {
      	var e = '<a href="#" onclick="upRowSupp('+index+')"><span class="btn btn-primary btn-xs ">Hidupkan</span></a>';
      }
      return e;
    }

    function formatAction_eoq(value,row,index){
      var v = $('#schema_line').edatagrid('getRows')[index].rop;
      if (row.pesan == Math.round(row.eoq)) {
      	var e = '<a href="#"><span class="btn btn-info btn-xs disabled">Menyala</span></a>';	
      } else {
      	var e = '<a href="#" onclick="upRowEoq('+index+')"><span class="btn btn-info btn-xs ">Hidupkan</span></a>';
      }
      return e;
    }

    function formatAction_rop(value,row,index){
      var v = $('#schema_line').edatagrid('getRows')[index].rop;
      if (row.rop_set == Math.round(row.rop)) {
      	var e = '<a href="#"><span class="btn btn-warning btn-xs disabled">Menyala</span></a>';	
      } else {
      	var e = '<a href="#" onclick="upRowRop('+index+')"><span class="btn btn-warning btn-xs ">Hidupkan</span></a>';
      }
      return e;
    }

    function formatAction_profit(value,row,index){
      var v = $('#schema_line').edatagrid('getRows')[index].op;
      if (Math.round(row.profit) == Math.round(row.set_profit) || v==null || v==0) {
        var e = '<a href="#"><span class="btn btn-warning btn-xs disabled">Menyala</span></a>';  
      } else {
        var e = '<a href="#" onclick="upRowProfit('+index+')"><span class="btn btn-warning btn-xs ">Hidupkan</span></a>';
      }
      return e;
    }

    function upRowSupp(ind){
      var mbuh = [];
      var dg = $('#schema_line');
      // $.map(dg.edatagrid('getRows')[ind], function(row){
	  var e = $('#schema_line').edatagrid('getRows')[ind].suppsel;
	  var id_item = $('#schema_line').edatagrid('getRows')[ind].id_item;        
		// var editors = dg.datagrid('getEditors', index);
        // if (editors.length){
          mbuh.push({
            id_item: id_item,
            item_hb: $('#schema_line').edatagrid('getRows')[ind].hb,
            item_profit: $('#schema_line').edatagrid('getRows')[ind].profit,
            item_price: $('#schema_line').edatagrid('getRows')[ind].harga,
            item_marketing: $('#schema_line').edatagrid('getRows')[ind].iklan,
            jumlah: $('#schema_line').edatagrid('getRows')[ind].pesan,
            supplier: e,
            boundary: $('#schema_line').edatagrid('getRows')[ind].rop_set,
            het: $('#schema_line').edatagrid('getRows')[ind].het
          });
        // }
      	// console.log(e);  
      // });
      console.log(mbuh);
      $.ajax({
        url:'<?php echo base_url() ?>produk/up_recomend',
        // url : url,
        type: 'POST',
        data: {data: JSON.stringify(mbuh)},
        // data: mbuh,
        cache: false,
        success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
      });
      $('#schema_line').edatagrid('reload');
    }

    function upRowEoq(ind){
      var mbuh = [];
      var dg = $('#schema_line');
      // $.map(dg.edatagrid('getRows')[ind], function(row){
	  var e = $('#schema_line').edatagrid('getRows')[ind].eoq;
	  var id_item = $('#schema_line').edatagrid('getRows')[ind].id_item;        
		// var editors = dg.datagrid('getEditors', index);
        // if (editors.length){
          mbuh.push({
            id_item: id_item,
            item_hb: $('#schema_line').edatagrid('getRows')[ind].hb,
            item_profit: $('#schema_line').edatagrid('getRows')[ind].profit,
            item_price: $('#schema_line').edatagrid('getRows')[ind].harga,
            item_marketing: $('#schema_line').edatagrid('getRows')[ind].iklan,
            jumlah: e,
            supplier: $('#schema_line').edatagrid('getRows')[ind].supplier,
            boundary: $('#schema_line').edatagrid('getRows')[ind].rop_set,
            het: $('#schema_line').edatagrid('getRows')[ind].het
          });
        // }
      	// console.log(e);  
      // });
      console.log(mbuh);
      $.ajax({
        url:'<?php echo base_url() ?>produk/up_recomend',
        // url : url,
        type: 'POST',
        data: {data: JSON.stringify(mbuh)},
        // data: mbuh,
        cache: false,
        success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
      });
      $('#schema_line').edatagrid('reload');
    }

    function upRowRop(ind){
      var mbuh = [];
      var dg = $('#schema_line');
      // $.map(dg.edatagrid('getRows')[ind], function(row){
	  var e = $('#schema_line').edatagrid('getRows')[ind].rop;
	  var id_item = $('#schema_line').edatagrid('getRows')[ind].id_item;        
		// var editors = dg.datagrid('getEditors', index);
        // if (editors.length){
          mbuh.push({
            id_item: id_item,
            item_hb: $('#schema_line').edatagrid('getRows')[ind].hb,
            item_profit: $('#schema_line').edatagrid('getRows')[ind].profit,
            item_price: $('#schema_line').edatagrid('getRows')[ind].harga,
            item_marketing: $('#schema_line').edatagrid('getRows')[ind].iklan,
            jumlah: $('#schema_line').edatagrid('getRows')[ind].pesan,
            supplier: $('#schema_line').edatagrid('getRows')[ind].supplier,
            boundary: e,
            het: $('#schema_line').edatagrid('getRows')[ind].het
          });
        // }
      	// console.log(e);  
      // });
      console.log(mbuh);
      $.ajax({
        url:'<?php echo base_url() ?>produk/up_recomend',
        // url : url,
        type: 'POST',
        data: {data: JSON.stringify(mbuh)},
        // data: mbuh,
        cache: false,
        success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
      });
      $('#schema_line').edatagrid('reload');
    }

    function upRowProfit(ind){
      var mbuh = [];
      var dg = $('#schema_line');
      // $.map(dg.edatagrid('getRows')[ind], function(row){
    var e = $('#schema_line').edatagrid('getRows')[ind].set_profit;
    var id_item = $('#schema_line').edatagrid('getRows')[ind].id_item;        
    // var editors = dg.datagrid('getEditors', index);
        // if (editors.length){
          mbuh.push({
            id_item: id_item,
            item_hb: $('#schema_line').edatagrid('getRows')[ind].hb,
            item_profit: e,
            item_price: $('#schema_line').edatagrid('getRows')[ind].harga,
            item_marketing: $('#schema_line').edatagrid('getRows')[ind].iklan,
            jumlah: $('#schema_line').edatagrid('getRows')[ind].pesan,
            supplier: $('#schema_line').edatagrid('getRows')[ind].supplier,
            boundary: $('#schema_line').edatagrid('getRows')[ind].rop_set,
            het: $('#schema_line').edatagrid('getRows')[ind].het
          });
        // }
        // console.log(e);  
      // });
      console.log(mbuh);
      $.ajax({
        url:'<?php echo base_url() ?>produk/up_recomend',
        // url : url,
        type: 'POST',
        data: {data: JSON.stringify(mbuh)},
        // data: mbuh,
        cache: false,
        success: function(result){
                var result = eval('('+result+')');
                if(result.success){
                } else {
                    $.messager.alert({
                        title: 'Error',
                        msg: result.msg,
                        icon: 'error'
                    });
                }
            }
      });
      $('#schema_line').edatagrid('reload');
    }

  </script>
     