<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Laporan Harian
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Laporan</a></li>
          <li class="active">Laporan Harian</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <p id="judul"></p>
                <table id="laporan" class="easyui-treegrid" style="width:auto;height:400px"
                      url="<?php echo base_url() ?>gl/harian"
                      toolbar="#toolbar" method="get" idField="id" treeField="name" 
                      rownumbers="true" fitColumns="true" >
                  <thead>
                      <tr>
                          <th field="name" width="50">Nama</th>
                          <th field="jumlah" width="50">Banyak</th>
                          <th field="harga" width="50">Harga Satuan</th>
                          <th field="total" width="50">Harga Total</th>
                      </tr>
                  </thead>
                </table>
                <div id="toolbar">
                  <span>Tanggal :</span>
                    <input id="tgl" type="text" class="easyui-datebox" style="line-height:26px;border:1px solid #ccc">
                  <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="doSearch()">Search</a>

                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Company</a> -->
                </div>
               
              <!-- Dialog Button -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">

    function doSearch(){
      $('#judul').text('Laporan Harian Tanggal : '+ $('#tgl').val());
      $('#laporan').treegrid('load',{
        tanggal: $('#tgl').val(),
      });
    }

  </script>
     