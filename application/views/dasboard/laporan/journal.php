<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Daftar Jurnal
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Master Data</a></li>
          <li class="active">Daftar Jurnal</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <table id="schema_data" class="easyui-datagrid" style="width:auto;height:400px"
                      url="<?php echo base_url() ?>gl/gl_data"
                      toolbar="#toolbar"
                      rownumbers="true" fitColumns="true" singleSelect="true" pagination="true">
                  <thead>
                      <tr>
                          <th field="period_id" width="50%">Periode</th>
                          <th field="journal_name" width="50%">Nama Jurnal</th>
                      </tr>
                  </thead>
                </table>
                <div id="toolbar">
                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newRole()">New Role</a> -->
                  <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editSchema()">Detail Jurnal</a>
                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editPrivilege()">Edit Privilege</a> -->

                  <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Company</a> -->
                </div>
              <div id="tabelLine" class="easyui-dialog" style="width:50%; height:400px; padding: 10px 20px" closed="true" buttons="#dialog-buttons2">
                <!-- <form id="#form2" method="post"> -->
                <table id="schema_line" title="Detail Jurnal" style="width:100%;height:auto"
                  pagination="true" idField="id"
                  rownumbers="true" fitColumns="true" singleSelect="true">
                  <thead>
                    <tr>
                      <th field="name" width="40%" >Kode Akun</th>
                      <th field="line_debit" width="30%" formatter="formatPrice">Debit</th>
                      <th field="line_credit" width="30%" formatter="formatPrice">Kredit</th>
                    </tr>
                  </thead>
                </table>
              </div>
               
              <!-- Dialog Button -->
              <div id="dialog-buttons2">
                  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#tabelLine').dialog('close')">Close</a>
              </div>

              <!-- <div id="tb" style="height:auto">
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#schema_line').edatagrid('addRow')">New</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#schema_line').edatagrid('destroyRow')">Destroy</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#schema_line').edatagrid('saveRow')">Save</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#schema_line').edatagrid('cancelRow')">Cancel</a>
              </div> -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">

    $(function(){
      // var row = $('#schema_data').datagrid('getSelected');
        $('#schema_line').edatagrid({
            // url: 'get_users.php',
            // saveUrl: '<?php //echo base_url() ?>gl/schema_insert/'+row.id,
            updateUrl: '<?php echo base_url() ?>gl/schema_update',
            destroyUrl: '<?php echo base_url() ?>gl/schema_delete',
        });
    });

    function editSchema(){
      var row = $('#schema_data').datagrid('getSelected');
      if (row){
          $('#tabelLine').dialog('open').dialog('setTitle','Detail Jurnal '+row.journal_name);
          $('#schema_line').edatagrid('loadData', []);
          url = "<?php echo base_url() ?>gl/journal_line/"+row.id;
          // $('#schema_line').edatagrid({saveUrl:'<?php //echo base_url() ?>gl/schema_insert/'+row.id,});
          $('#schema_line').edatagrid('reload', url);
      }
    }

    function formatPrice(val,row){
      var x = parseInt(val);
      return x.toLocaleString('ind');
      // console.log(typeof val);
    }

  </script>
     