<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>jQuery EasyUI</title>
  <link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
  <script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.min.js"></script>
  <script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
  <script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.edatagrid.js"></script>
  <script type="text/javascript">
    $.extend($.fn.validatebox.defaults.rules,{
      range: {
        validator: function(value,param){
          var row =  $("#tt").datagrid("getSelected").max;
          var min = 0;
          var maxin = row;
          return value>=min && value<=maxin;
        },
        message: 'The value must be between {0} and '
      }
    });
    var data = [
        {"productid":"FI-SW-01","unitcost":10.00,"status":"P","listprice":16.50,"attr1":"Large","itemid":"EST-1","max":10},
        {"productid":"K9-DL-01","unitcost":12.00,"status":"P","listprice":18.50,"attr1":"Spotted Adult Female","itemid":"EST-2","max":30},
        {"productid":"RP-SN-01","unitcost":12.00,"status":"P","listprice":18.50,"attr1":"Venomless","itemid":"EST-3","max":20},
        {"productid":"RP-LI-02","unitcost":12.00,"status":"P","listprice":18.50,"attr1":"Green Adult","itemid":"EST-5","max":40},
        {"productid":"FL-DSH-01","unitcost":12.00,"status":"P","listprice":58.50,"attr1":"Tailless","itemid":"EST-6","max":50},
        {"productid":"FL-DSH-01","unitcost":12.00,"status":"P","listprice":23.50,"attr1":"With tail","itemid":"EST-7","max":60},
        {"productid":"FL-DLH-02","unitcost":12.00,"status":"P","listprice":93.50,"attr1":"Adult Female","itemid":"EST-8","max":20},
        {"productid":"FL-DLH-02","unitcost":12.00,"status":"P","listprice":93.50,"attr1":"Adult Male","itemid":"EST-9","max":70},
        {"productid":"RP-SN-01","unitcost":12.00,"status":"P","listprice":18.50,"attr1":"Rattleless","itemid":"EST-4","max":80},
        {"productid":"AV-CB-01","unitcost":92.00,"status":"P","listprice":193.50,"attr1":"Adult Male","itemid":"EST-10","max":90}
    ]

  </script>
</head>
<body>
  <h1>Editable DataGrid</h1>
  <div style="margin-bottom:10px">
    <a href="#" onclick="javascript:$('#tt').edatagrid('addRow')">AddRow</a>
    <a href="#" onclick="javascript:$('#tt').edatagrid('saveRow')">SaveRow</a>
    <a href="#" onclick="javascript:$('#tt').edatagrid('cancelRow')">CancelRow</a>
    <a href="#" onclick="javascript:$('#tt').edatagrid('destroyRow')">destroyRow</a>
  </div>
  <table id="tt" class="easyui-edatagrid" style="width:600px;height:200px" data-options="
      singleSelect: true,
      data: data
      ">
    <thead>
      <tr>
        <th field="itemid" width="100" editor="{type:'textbox',options:{required:true}}">Item ID</th>
        <th field="productid" width="100" editor="textbox">Product ID</th>
        <th field="listprice" width="100" align="right" editor="
            {
              type:'numberbox',
              options:{
                precision:1,
                validType:'range[10,20]'
              }
            }
            ">List Price</th>
        <th field="unitcost" width="100" align="right" editor="
            {
              type:'numberbox',
              options:{
                validType:'range[10,20]'
              }
            }
            " >Unit Cost</th>
        <th field="attr1" width="150" editor="text">Attribute</th>
      </tr>
    </thead>
  </table>
</body>
</html>