  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Income Statement
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Keuangan</a></li>
            <li class="active">Income Statement</li>
          </ol>
        </section>
      
        <!-- Main content -->
        <section class="content">
         
          <div class="row">
            <div class="col-xs-12 col-lg-10">
              <div class="box">
                <div class="box-header">
                   <div class="row">
					<div class="co-lg-12">
                   <form method="post" action="<?php echo base_url(),"gl/filterIstateTahun" ?>" enctype="multipart/form-data">
				   <div class="col-lg-1">
						<label for="exampleInputEmail1">Filter</label>                   
					</div>
                    <div class="col-lg-2">
						<input name="tgl_awal" type="text" class="form-control datepicker"  placeholder="Periode" data-date-format="yyyy" required>
                    </div>
					<!-- <div class="col-lg-2">
						<input name="tgl_akhir" type="text" class="form-control datepicker"  placeholder="Tanggal akhir" data-date-format="yyyy-mm-dd" required>
                    </div> -->
					<div class="col-lg-1">
						 <button type="submit" class="btn btn-primary">Filter</button>
					</div>
					 </form>
					 <div class="col-lg-1">
					 <form method="post" action="<?php echo base_url(),"gl/printIstate" ?>" enctype="multipart/form-data">
					 <input name="tgl_awal" type="hidden" value="<?php echo $tgl_tampil; ?>">
					 <!-- <input name="tgl_akhir" type="hidden"  value="<?php //echo $tgl_akhir; ?>">  -->
						 <button type="submit" class="btn btn-info"><li class="fa fa-print"></li>Print</button>
					</form>
					 </div>
                  </div>
               </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                <h4>Periode : <?php echo $tgl_awal; ?> </h4>
                  <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                            <th><b><i>Pendapatan</i></b></th></tr>
                                        </thead>
                                        <tbody>
                                            <th bgcolor="gray"><b>Pendapatan dari Penjualan</b></th><th bgcolor="gray"></th>
                                              <?php $total_dapat=0;
                                              if(!empty($isi)){ 
                                              foreach ($isi as $rows) {
                                                if($rows->acc_id%4000<1000&&$rows->acc_id/4000==1){ ?> 
                                                        <tr>
                                                  <td><?php echo $rows->nama;?></td>
                                                  <?php if($rows->acc_id==4000){?>
                                                            <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_crd-$rows->total_dbt,0,'','.').",-"; $total_dapat=$total_dapat+($rows->total_crd-$rows->total_dbt);?></td>
                                                        <?php } else{ ?>
                                                            <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-"; $total_dapat=$total_dapat+($rows->total_dbt-$rows->total_crd);?></td>
                                                        <?php } ?> 
                                                  </tr>
                                          <?php }}} ?>
                                            
                                              <tr>
                                                <th><b>Total Pendapatan dari Penjualan</b></th>
                                                <td style="text-align:right;"><b><?php echo "Rp ".number_format($total_dapat,0,'','.').",-"; ?></b></td>
                                              </tr>
                                            <th bgcolor="gray"><b>Harga Pokok Penjualan</b></th><th bgcolor="gray"></th>
                                                <?php $total=0;
                                                if(!empty($isi)){ 
                                                foreach ($isi as $rows) {
                                                    if($rows->acc_id<6000&&$rows->acc_id>4999){ ?> 
                                                        <tr>
                                                        <td><?php echo $rows->nama;?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-"; $total=$total+($rows->total_dbt-$rows->total_crd);?></td>
                                                        </tr>
                                            <?php }}} ?>
                                                
                                                    <tr>
                                                        <th style="color:red"><b>Total Harga Pokok Penjualan</b></th>
                                                        <td style="text-align:right;"><b><?php echo "Rp ".number_format($total,0,'','.').",-"; $total=$total_dapat-$total; ?></b></td>
                                                    </tr>
                                                    <tr>
                                                        <th><b>Laba Kotor</b></th>
                                                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total,0,'','.').",-"; ?></i></b></td>
                                                    </tr>
                                            <th bgcolor="gray"><b>Biaya Operasional</b></th><th bgcolor="gray"></th>
                                                <?php $total_oper=0;
                                                if(!empty($isi)){ 
                                                foreach ($isi as $rows) {
                                                    if($rows->acc_id<7000&&$rows->acc_id>5999){ ?> 
                                                        <tr>
                                                        <td><?php echo $rows->nama;?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-"; $total_oper=$total_oper+($rows->total_dbt-$rows->total_crd);?></td>
                                                        </tr>
                                            <?php }}} ?>
                                                
                                                    <tr>
                                                        <th style="color:red"><b>Total Biaya</b></th>
                                                        <td style="text-align:right;"><b><?php echo "Rp ".number_format($total_oper,0,'','.').",-"; $total=$total-$total_oper; ?></b></td>
                                                    </tr>
                                                    <tr>
                                                        <th><b><i>Pendapatan Bersih Operasional</i></b></th>
                                                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total,0,'','.').",-"; ?></i></b></td>
                                                    </tr>       
                                      <!-- </tbody>     -->
                                      <!-- <thead>
                                            <tr>
                                            <th><b><i>Pendapatan Lainnya</i></b></th></tr>
                                        </thead> -->
                                        <!-- <tbody> -->
                                             <th bgcolor="gray"><b>Pendapatan Lainnya</b></th><th bgcolor="gray"></th>
                                                <?php $total_lain=0;
                                                if(!empty($isi)){ 
                                                foreach ($isi as $rows) {
                                                    if(($rows->acc_id<8000&&$rows->acc_id>6999)){ ?> 
                                                        <tr>
                                                        <td><?php echo $rows->nama;?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-"; $total_lain=$total_lain+($rows->total_dbt-$rows->total_crd);?></td>
                                                        </tr>
                                            <?php } else if($rows->acc_id<4000&&$rows->acc_id>=2000&&($rows->total_crd-$rows->total_dbt>0)){ ?> 
                                                        <tr>
                                                        <td><?php echo $rows->nama;?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_crd-$rows->total_dbt,0,'','.').",-"; $total_lain=$total_lain+($rows->total_crd-$rows->total_dbt);?></td>
                                                        </tr>?>

                                            <?Php }}} ?>
                                                
                                                    <tr>
                                                        <th><b>Total Pendapatan Lainnya</b></th>
                                                        <td style="text-align:right;"><b><?php echo "Rp ".number_format($total_lain,0,'','.').",-"; ?></b></td>
                                                    </tr>
                                            <th bgcolor="gray"><b>Biaya Lainnya</b></th><th bgcolor="gray"></th>
                                                <?php $total_blain=0;
                                                if(!empty($isi)){ 
                                                foreach ($isi as $rows) {
                                                    if($rows->acc_id<9000&&$rows->acc_id>7999){ ?> 
                                                        <tr>
                                                        <td><?php echo $rows->nama;?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-"; $total_blain=$total_blain-($rows->total_dbt-$rows->total_crd);?></td>
                                                        </tr>
                                            <?php }}} ?>
                                                
                                                    <tr>
                                                        <th style="color:red"><b>Total Biaya Lainnya</b></th>
                                                        <td style="text-align:right;"><b><?php echo "Rp ".number_format($total_blain,0,'','.').",-"; ?></b></td>
                                                    </tr>
                                                    <tr>
                                                        <th><b><i>Pendapatan Bersih</i></b></th>
                                                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total+$total_lain+$total_blain,0,'','.').",-"; ?></i></b></td>
                                                    </tr>
                                        </tbody>
                                    </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     