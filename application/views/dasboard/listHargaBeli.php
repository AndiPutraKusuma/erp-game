<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          List Harga Beli
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Master Data</a></li>
          <li class="active">Harga Beli</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-body table-responsive no-padding">
                <?php if($this->session->flashdata('pesan')){
                  echo $this->session->flashdata('pesan');
                } ?>
                <table id="schema_line" title="List Harga" style="width:100%;height:auto"
                  pagination="false" idField="id"
                  rownumbers="true" fitColumns="true" singleSelect="false">
                  <thead>
                    <tr>
                      <th field="nama_item" width="25%">Nama</th>
                      <th field="harga1" width="10%" formatter="formatPrice">Sinar Abadi</th>
                      <th field="harga2" width="10%" formatter="formatPrice">Lancar Jaya</th>
                      <th field="harga3" width="10%" formatter="formatPrice">Nikmat</th>
                      <th field="harga4" width="10%" formatter="formatPrice">Sakinah</th>
                      <th field="harga5" width="15%" formatter="formatPrice">Maju Terus</th>
                    </tr>
                  </thead>
                </table>               

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          <div class="row">
            <div class="col-md-12 text-center">
              <?php //echo $paging; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">

    $(function(){
      // var row = $('#schema_data').datagrid('getSelected');
        $('#schema_line').edatagrid({
            url: '<?php echo base_url() ?>Produk/listHarga_data',
            // saveUrl: '<?php //echo base_url() ?>gl/schema_insert/'+row.id,
            // updateUrl: '<?php //echo base_url() ?>gl/schema_update',
            // destroyUrl: '<?php //echo base_url() ?>gl/schema_delete',
        });
    });

    function formatPrice(val,row){
      var x = parseInt(val);
      return x.toLocaleString('ind');
      // console.log(typeof val);
    }

  </script>
     