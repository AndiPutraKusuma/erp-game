<?php class MPegawai extends CI_Model {

	 public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
	public function addPegawai($data){
		$this->db->reconnect();
		$query1=$this->db->query("SELECT id_pegawai FROM pegawai WHERE id_pegawai='$data[idPegawai]'");
		if($query1->result()){
			return -1;
		}else{
			$query=$this->db->query("INSERT INTO pegawai (id_pegawai,nama,jenkel,alamat,hp,email,tgl,jabatan,tanggungan,shift,id_pemilik) values('$data[idPegawai]','$data[nama]','$data[jenkel]','$data[alamat]','$data[hp]','$data[email]',NOW(),'$data[jabatan]','$data[tanggungan]','$data[shift]','$data[id_pemilik]')");
		
		return 0;
		}
		
	}
	//list data
	public function list_customer($id){
		$this->db->reconnect();
			$query = $this->db->query("SELECT id_pegawai,nama,jenkel,alamat,hp,pegawai.email,tgl,photo_link,jabatan,tanggungan,shift
	 FROM pegawai WHERE pegawai.`id_pemilik`= '$id' ORDER BY nama ASC");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function pageList_pegawai($page,$per_page,$id){
		$this->db->reconnect();
			$query = $this->db->query("SELECT id_pegawai,nama,jenkel,alamat,hp,pegawai.email,tgl,photo_link,jabatan,tanggungan,shift
	 FROM pegawai WHERE pegawai.`id_pemilik`= '$id' ORDER BY nama ASC LIMIT $per_page OFFSET $page");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	
	public function view_pegawai($id){
		$this->db->reconnect();
			$query = $this->db->query("SELECT id_pegawai,nama,jenkel,alamat,hp,pegawai.email,tgl,photo_link,jabatan,tanggungan,shift
	 FROM pegawai  WHERE id_pegawai= '$id'");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}

	public function update_gaji($data){
		$this->db->reconnect();
		$query1 = $this->db->query("SELECT id FROM gaji WHERE id_pemilik='$data[id]'");
		if($query1->result()){
			$query=$this->db->query("UPDATE `gaji` SET `pokok`='$data[pokok]',`tunjangan`='$data[tunjangan]',`bonus`='$data[bonus]',`satuan_bonus`='$data[satuan]' WHERE `id_pemilik`='$data[id]'");
		}else{
			$query=$this->db->query("INSERT INTO `gaji`(`pokok`, `tunjangan`, `bonus`,`satuan_bonus`, `id_pemilik`) VALUES ('$data[pokok]','$data[tunjangan]','$data[bonus]','$data[satuan]','$data[id]')");	
		return 0;
		}
	}

	public function list_jual($data,$id){
		$this->db->reconnect();
		$query = $this->db->query("SELECT SUM(total) as penjualan, kurir, tanggungan, jabatan FROM `penjualan` p, `pegawai` g WHERE p.id_pemilik='$id' AND p.tanggal<'$data[tanggal]' AND g.nama=p.kurir GROUP BY kurir ORDER BY penjualan desc");
		if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}

	public function list_atur($id){
		$this->db->reconnect();
			$query = $this->db->query("SELECT * FROM gaji WHERE id_pemilik='$id'");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}

	public function bayarGaji($id,$data,$idjurnalL){
		 $this->db->reconnect();
		 // $tanggal = $data['tanggal'].'-27';
		// $query=$this->db->query("UPDATE modal set status=1 where id_modal=$id");
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$data[tanggal]',CONCAT('Pembayaran Gaji','$idjurnalL'),'$id')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',2010,'$data[total]',0,'$id')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',1000,0,'$data[total]','$id')");
	
	 }

	//update
	public function update($data){
		$this->db->reconnect();
		//echo $data['idInstitut'],$data['ktp'],$data['nama'],$data['jenkel'],$data['alamat'],$data['hp'],$data['email'],$data['jabatan'];
		$query=$this->db->query("UPDATE pegawai SET nama='$data[nama]',jenkel='$data[jenkel]', alamat='$data[alamat]',hp='$data[hp]',email='$data[email]',jabatan='$data[jabatan]',tanggungan='$data[tanggungan]',shift='$data[shift]' WHERE id_pegawai='$data[idPegawai]'");
		
		return $query;

	}
	
	//delete
	public function delete($id){
		$this->db->reconnect();
		$query=$this->db->query("DELETE FROM pegawai WHERE id_pegawai=$id");
		
		return $query;
	}
	
	public function countPegawai($id){

		$this->db->reconnect();
			$query = $this->db->query("SELECT COUNT(id_pegawai) AS jumlah FROM pegawai WHERE pegawai.`id_pemilik`='$id' ");
			if ($query->num_rows() > 0)
			{
				$row=$query->row();
				return $row->jumlah;
			}
			else{
				return 0;
			}

	}

	public function cek2()
	{
		$this->db->reconnect();
		$query=$this->db->query("SELECT MAX(id)+1 as apa FROM gl_journal_h ");
		return $query->row();
	}
}
?>
