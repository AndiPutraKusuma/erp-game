<?php
class Mpembelian extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }
	 //add data
	  public function addPurchasing($data,$idjurnalL,$idjurnalH,$pemilik){
		$temp=$idjurnalH;
		$this->db->reconnect();
		$query=$this->db->query("CALL sp_input_purchasing('$data[idTransaksi]','$data[idSuplier]','$data[email]','$data[tgl]','$data[total]','$data[id_pemilik]','$data[ongkir]')");
		foreach($this->cart->contents() as $item){
			$this->db->reconnect();
			$this->db->query("CALL sp_input_detailPurchasing('$data[idTransaksi]','$item[id]','$item[qty]','$item[price]','$data[idSuplier]')");

		}
		$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$data[tgl]',CONCAT('Pembelian ','$data[idTransaksi]'),'$data[id_pemilik]')");

		$this->load->model('mgl');
		$schema = $this->mgl->schema_line(1);
		$searchArray = array("HB", "HJ", "0");
		$replaceArray = array($data['total'],0,0);
		foreach ($schema as $line) {
			$data_line = array(
				'journal_id' => $temp,
				'acc_id' => $line->acc_id,
				'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
				'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
				'uid' => $this->session->userdata('id_petugas'),
			);
			$oke = $this->db->insert('gl_journal_l',$data_line);
		}

		$this->cart->destroy();
	 }
	 //list pemesanan
	 public function listPemesanan($id){
		 $this->db->reconnect();
			$query = $this->db->query("CALL sp_list_purchasing($id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	 }
	 public function pageList_pembelian($start,$limit,$id){
		$this->db->reconnect();
		$game_id = $this->session->userdata('game_id');
			$query = $this->db->query("CALL sp_pageList_pembelian($start,$limit,$id,$game_id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	 //deatil pemesanan
	 public function rincianPemesanan($id){
		 $this->db->reconnect();
			$query = $this->db->query("CALL sp_rincianPembelian('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row) 
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	 }
	 //update status
	 public function updateStatus($data){
		 $this->db->reconnect();
		 $query=$this->db->query("CALL sp_updateStatus_pembelian('$data[idTransaksi]','$data[status]')");
	 }
	 public function countPembelian($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungPembelian($id)");

				$row=$query->row();
				return $row->jumlah;


	}
	 public function countPembelianSukses($id){

		$this->db->reconnect();
		$game_id = $this->session->userdata('game_id');

			$query = $this->db->query("CALL sp_hitungPembelianSukses($id,$game_id)");

				$row=$query->row();
				return $row->jumlah;
	}
				
	public function purchasing_header($data)
	{
		$this->db->reconnect();
		$this->db->trans_start();
		$ok = $this->db->insert('purchasing',$data);
		$this->load->model('mpenjualan');
		$haha = $this->mpenjualan->cek2();
		$idjurnalH = $haha->apa;
		$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$data[tanggal_po]',CONCAT('Pembelian ','$data[id_po]'),'$data[id_pemilik]')");
		$this->load->model('mgl');
		$schema = $this->mgl->schema_line(1);
		$searchArray = array("HB", "HJ", "0");
		$replaceArray = array($data['totalHarga'],0,0);
		foreach ($schema as $line) {
			$data_line = array(
				'journal_id' => $idjurnalH,
				'acc_id' => $line->acc_id,
				'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
				'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
				'uid' => $this->session->userdata('id_petugas'),
			);
			$okesip = $this->db->insert('gl_journal_l',$data_line);
		}
		if($ok)
		{
			$this->db->trans_commit();
			return true;
		}
		else
		{
			$this->db->trans_rollback();
			return false;	
		}
	}
	public function addPembelian($data,$supplier)
	{
		$id_pemilik = $this->session->userdata('id_retail');			
		$this->db->reconnect();
		$this->db->trans_start();
		$insert = array(
			'id_purchasing' => $data['id_po'],
			'id_item' => $data['id_barang'],
			'jumlah' => $data['jumlah'],
			'hargaSatuan' => $data['harga'],
			'id_pemilik' =>$id_pemilik,
			'id_supplier' => $supplier,
			'game_id' => $this->session->userdata('game_id'),
			);
		$ok = $this->db->insert('detail_purchasing',$insert);
		if($ok)
		{
			$this->db->trans_commit();
			return true;
		}
		else
		{
			$this->db->trans_rollback();
			return false;
		}
	}

	public function cekstock($id_item)
	{
		$this->db->reconnect();
		$stock = 0;
		$this->db->select('sum(jumlah) as stock');
		// $this->db->where('id_pemilik','1111');
		$this->db->where('id_pemilik',$this->session->userdata('id_retail'));
		$this->db->where('id_item',$id_item);
		$this->db->where('game_id',$this->session->userdata('game_id'));
		$this->db->group_by('gudang.id_item');
		$read = $this->db->get('gudang');
		foreach ($read->result() as $data) 
		{
			$stock = $data->stock;
		}
		
		return $stock;
	}

	public function autoStock()
	{
		$this->db->reconnect();
		$total = 0;
		$game_id = $this->session->userdata('game_id');
		// $game_id = 1;
		$id_pemilik = $this->session->userdata('id_retail');
		// $id_pemilik = '1111';
		$this->db->where('id_pemilik',$id_pemilik);
		$this->db->where('game_id',$game_id);
		$this->db->where('jumlah > 0');
		$this->db->where('supplier > 0');
        $this->db->order_by("supplier", "asc"); 
		$read = $this->db->get('config_product');
       	// $id_po	= 0;
       	// $id_rec	= 0;
       	$flag = 0;
       	$tanggal = $this->session->userdata('tanggal');
       	$supp = 0;

		foreach ($read->result() as $data) 
		{
			$stock = $this->cekstock($data->id_item);
			// echo 'id_item : '.$data->id_item.' stock : '.$stock.' boundary : '.$data->boundary.'<br>';
			if ($stock <= $data->boundary)
			{	
				if (!$supp)
				{
					$supp = $data->supplier;
					$id_po	= uniqid("PO");
       				$id_rec	= uniqid("REC");
				}
				// echo 'yeay <br>';
			    // echo $supp.' '.$data->supplier.'<br>';
				if ($supp == $data->supplier)
				{
					// echo 'sama supplier <br>';
					// $flag = 1;
					$id_supplier = $data->supplier;
					//echo $id_supplier;
					$barcode = mt_rand(00001, 99999);
					$this->db->select('harga');
					$this->db->where('id_item',$data->id_item);
					$this->db->where('id_suplier',$id_supplier);
					$ok = $this->db->get('detail_suplier');
					foreach ($ok->result() as $dataharga) 
					{
						$harga = $dataharga->harga;
					}
					$totalbeli = $harga * $data->jumlah;
					// echo 'harga : '.$harga.' jumlah : '.$data->jumlah.' total : '.$total.'<br>';
					$total = $total + $totalbeli;
					$insert = array(
						'id_purchasing' => $id_po,
						'id_item' => $data->id_item,
						'jumlah' => $data->jumlah,
						'hargaSatuan' => $harga,
						'id_pemilik' =>$id_pemilik,
						'id_supplier' => $data->supplier,
						'game_id' => $game_id,
						);
					$ok = $this->db->insert('detail_purchasing',$insert);
					$randef = mt_rand(1,10);
					$defect = floor(($data->jumlah/100) *$randef);
					$this->db->select('jangka_waktu');
					$this->db->where('id_suplier',$data->supplier);
					$readread = $this->db->get('suplier');
					foreach ($readread->result() as $result) 
					{
						$jangka = $result->jangka_waktu;
					}

					$datetime = new DateTime($tanggal);
					$datetime->add(new DateInterval('P'.$jangka.'D'));
					$tanggal_masuk =  $datetime->format('Y-m-d');
					
					$receive = array(
						'id_rec' => $id_rec,
						'id_item' => $data->id_item,
						'jumlah' => $data->jumlah - $defect,
						'defect' => $defect,
						'hargaSatuan' => $harga,
						'id_pemilik' => $id_pemilik,
						'barcode_barang' =>$data->id_item.$barcode,
						'id_supplier' => $data->supplier,
						'game_id' => $game_id,					
						);
					$receive2 = array(
						'id_rec' => $id_rec,
						'id_item' => $data->id_item,
						'jumlah' => $data->jumlah - $defect,
						// 'defect' => $defect,
						'hargaSatuan' => $harga,
						'id_pemilik' => $id_pemilik,
						'barcode_barang' =>$data->id_item.$barcode,
						'id_supplier' => $data->supplier,
						'game_id' => $game_id,
						'tanggal_masuk' =>$tanggal_masuk					
						);

					$ok2 = $this->db->insert('detail_penerimaan',$receive);
					$ok3 = $this->db->insert('gudang',$receive2);
					// echo 'insert detail '.$supp.' <br>';
				}
				else
				{
					// echo 'beda supplier '.$supp.'  total :'.$total.'<br>';
					if($total)
					{ 
						// echo $supp.' ';
						$this->db->select('ongkir');
						$this->db->where('id_suplier',$data->supplier);
						$read = $this->db->get('suplier');
						foreach ($read->result() as $dataongkir) 
						{
							$ongkir = $dataongkir->ongkir;
						}
						$total = $total + $ongkir;
						$data_header = array(
							'id_po' => $id_po,
							'id_suplier' => $supp,
							'id_petugas' => $id_pemilik,
							'tanggal_po' => $tanggal,
							'totalHarga' => $total,
							'status' =>3,
							'id_pemilik' => $id_pemilik,
							'ongkir' => $ongkir,
							'game_id' => $game_id,
							);
						$data_receive = array(
							'id_rec' => $id_rec,
							'id_po' => $id_po,
							'id_suplier' => $supp,
							'id_petugas' => $id_pemilik,
							'tanggal_receive' => $tanggal_masuk,
							'status' =>3,
							'totalHarga' => $total,
							'id_pemilik' => $id_pemilik,
							'kurir' => 'nanda',
							'game_id' => $game_id,
							);
						$this->db->insert('purchasing',$data_header);
						$this->db->insert('penerimaan_barang',$data_receive);
						// echo 'insert header '.$supp.' <br>';
						$this->load->model('mgl');
						$this->load->model('mpenjualan');
						$haha = $this->mpenjualan->cek2();
						if($haha->apa < 1)
						{
							$idjurnalH = 1;
						}
						else
						{
							$idjurnalH = $haha->apa;
						}
						$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES('$tanggal_masuk',CONCAT('Pembelian ','$id_po'),'$id_pemilik',$game_id)");
						$this->load->model('mgl');
						$schema = $this->mgl->schema_line(1);
						$searchArray = array("HB", "HJ", "0");
						$replaceArray = array($total,0,0);
						foreach ($schema as $line) {
							$data_line = array(
								'journal_id' => $idjurnalH,
								'acc_id' => $line->acc_id,
								'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
								'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
								'uid' => $this->session->userdata('id_petugas'),
								'game_id' => $this->session->userdata('game_id')

							);
							$oke = $this->db->insert('gl_journal_l',$data_line);
							// echo 'insert gl <br>';
						}

						// echo 'insert detail supp :'.$data->supplier.'<br>';
						// echo 'sama supplier <br>';
					// $flag = 1;
					$id_supplier = $data->supplier;
					//echo $id_supplier;
					$barcode = mt_rand(00001, 99999);
					$this->db->select('harga');
					$this->db->where('id_item',$data->id_item);
					$this->db->where('id_suplier',$id_supplier);
					$ok = $this->db->get('detail_suplier');
					foreach ($ok->result() as $dataharga) 
					{
						$harga = $dataharga->harga;
					}
					$totalbeli = $harga * $data->jumlah;
					// echo 'harga : '.$harga.' jumlah : '.$data->jumlah.' total : '.$total.'<br>';
					$total =  $totalbeli;
					// echo 'totalfinal : '.$total.'<br>';
					$insert = array(
						'id_purchasing' => $id_po,
						'id_item' => $data->id_item,
						'jumlah' => $data->jumlah,
						'hargaSatuan' => $harga,
						'id_pemilik' =>$id_pemilik,
						'id_supplier' => $data->supplier,
						'game_id' => $game_id,
						);
					$ok = $this->db->insert('detail_purchasing',$insert);
					$randef = mt_rand(1,10);
					$defect = floor(($data->jumlah/100) *$randef);
					$this->db->select('jangka_waktu');
					$this->db->where('id_suplier',$data->supplier);
					$readread = $this->db->get('suplier');
					foreach ($readread->result() as $result) 
					{
						$jangka = $result->jangka_waktu;
					}

					$datetime = new DateTime($tanggal);
					$datetime->add(new DateInterval('P'.$jangka.'D'));
					$tanggal_masuk =  $datetime->format('Y-m-d');
					
					$receive = array(
						'id_rec' => $id_rec,
						'id_item' => $data->id_item,
						'jumlah' => $data->jumlah - $defect,
						'defect' => $defect,
						'hargaSatuan' => $harga,
						'id_pemilik' => $id_pemilik,
						'barcode_barang' =>$data->id_item.$barcode,
						'id_supplier' => $data->supplier,
						'game_id' => $game_id,					
						);
					$receive2 = array(
						'id_rec' => $id_rec,
						'id_item' => $data->id_item,
						'jumlah' => $data->jumlah - $defect,
						// 'defect' => $defect,
						'hargaSatuan' => $harga,
						'id_pemilik' => $id_pemilik,
						'barcode_barang' =>$data->id_item.$barcode,
						'id_supplier' => $data->supplier,
						'game_id' => $game_id,
						'tanggal_masuk' =>$tanggal_masuk					
						);

					$ok2 = $this->db->insert('detail_penerimaan',$receive);
					$ok3 = $this->db->insert('gudang',$receive2);
					// echo 'insert detail <br>';				
					}
					$id_po	= uniqid("PO");
       				$id_rec	= uniqid("REC");
       				$supp = $data->supplier;
       				//$total = 0;
				}
				// $config[] = $data;
			}
		}
		if ($total)
		{
			$this->db->select('ongkir');
			$this->db->where('id_suplier',$supp);
			$read = $this->db->get('suplier');
			foreach ($read->result() as $dataongkir) 
			{
				$ongkir = $dataongkir->ongkir;
			}
			$total = $total + $ongkir;
			$data_header = array(
				'id_po' => $id_po,
				'id_suplier' => $supp,
				'id_petugas' => $id_pemilik,
				'tanggal_po' => $tanggal,
				'totalHarga' => $total,
				'status' =>3,
				'id_pemilik' => $id_pemilik,
				'ongkir' => $ongkir,
				'game_id' => $game_id,
				);
			$data_receive = array(
				'id_rec' => $id_rec,
				'id_po' => $id_po,
				'id_suplier' => $supp,
				'id_petugas' => $id_pemilik,
				'tanggal_receive' => $tanggal_masuk,
				'status' =>3,
				'totalHarga' => $total,
				'id_pemilik' => $id_pemilik,
				'kurir' => 'nanda',
				'game_id' => $game_id,
				);
			$this->db->insert('purchasing',$data_header);
			$this->db->insert('penerimaan_barang',$data_receive);
			// echo 'insert header <br>';
			$this->load->model('mgl');
			$dataggl = array(
                    'tgl'=> $tanggal_masuk,
                    'idTransaksi' =>$id_po,
                    'id_pemilik' =>$id_pemilik,
                    'game_id' =>$game_id,
                    'hargabeli' =>$total,
                    'ongkir' =>$ongkir,
                    );
                $this->mgl->bayar_pembelian($dataggl);
			// $this->load->model('mpenjualan');
			// $haha = $this->mpenjualan->cek2();
		 //    	$idjurnalH = $haha->apa;
			// $query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES('$tanggal_masuk',CONCAT('Pembelian ','$id_po'),'$id_pemilik',$game_id)");
			// $schema = $this->mgl->schema_line(1);
			// $searchArray = array("HB", "HJ", "0");
			// $replaceArray = array($total,0,0);
			// foreach ($schema as $line) {
			// 	$data_line = array(
			// 		'journal_id' => $idjurnalH,
			// 		'acc_id' => $line->acc_id,
			// 		'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
			// 		'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
			// 		'uid' => $this->session->userdata('id_petugas'),
			// 		'game_id' => $this->session->userdata('game_id')

			// 	);
			// 	$oke = $this->db->insert('gl_journal_l',$data_line);
			// 	// echo 'insert gl <br>';
			// }
		}
		
	}

public function autoStock2()
	{
		$this->db->reconnect();
		$total = 0;
		$game_id = $this->session->userdata('game_id');
		//$game_id = 8;
		//$id_pemilik = $this->session->userdata('id_retail');
		// $id_pemilik = '1111';
		$this->db->select('game_player');
		$this->db->where('game_id',$game_id);
		$readuser = $this->db->get('game_detail');
		foreach ($readuser->result() as $datausert) 
		{
			$user[] = $datausert->game_player;
		}

		foreach ($user as $users) 
		{
			$id_pemilik = $users;
			// echo $id_pemilik.' ';
			$this->db->where('id_pemilik',$id_pemilik);
			$this->db->where('game_id',$game_id);
			$this->db->where('jumlah > 0');
			$this->db->where('supplier > 0');
	        $this->db->order_by("supplier", "asc"); 
			$read = $this->db->get('config_product');
	       	// $id_po	= 0;
	       	// $id_rec	= 0;
	       	$flag = 0;
	       	$total = 0;
	       	$tanggal = $this->session->userdata('tanggal');
	       	$supp = 0;

			foreach ($read->result() as $data) 
			{
				$stock = $this->m_game->getStock($data->id_item,$id_pemilik);
				//echo 'id_item : '.$data->id_item.' stock : '.$stock.' boundary : '.$data->boundary.'<br>';
				if ($stock <= $data->boundary)
				{	
					 //echo '<br> masuk sini <br>';
					if (!$supp)
					{
						$supp = $data->supplier;
						$id_po	= uniqid("PO");
	       				$id_rec	= uniqid("REC");
					}
					// echo 'yeay <br>';
				    // echo $supp.' '.$data->supplier.'<br>';
					if ($supp == $data->supplier)
					{
						// echo 'sama supplier <br>';
						// $flag = 1;
						$id_supplier = $data->supplier;
						// echo 'id_item : '.$data->id_item.' supp :'.$id_supplier.'<br>';
						$barcode = mt_rand(00001, 99999);
						$this->db->select('harga');
						$this->db->where('id_item',$data->id_item);
						$this->db->where('id_suplier',$id_supplier);
						$ok = $this->db->get('detail_suplier');
						foreach ($ok->result() as $dataharga) 
						{
							$harga = $dataharga->harga;
						}
						$totalbeli = $harga * $data->jumlah;
						// echo 'harga : '.$harga.' jumlah : '.$data->jumlah.' total : '.$total.'<br>';
						$total = $total + $totalbeli;
						$insert = array(
							'id_purchasing' => $id_po,
							'id_item' => $data->id_item,
							'jumlah' => $data->jumlah,
							'hargaSatuan' => $harga,
							'id_pemilik' =>$id_pemilik,
							'id_supplier' => $data->supplier,
							'game_id' => $game_id,
							);
						$ok = $this->db->insert('detail_purchasing',$insert);
						$randef = mt_rand(1,10);
						$defect = floor(($data->jumlah/100) *$randef);
						$this->db->select('jangka_waktu');
						$this->db->where('id_suplier',$data->supplier);
						$readread = $this->db->get('suplier');
						foreach ($readread->result() as $result) 
						{
							$jangka = $result->jangka_waktu;
						}

						$datetime = new DateTime($tanggal);
						$datetime->add(new DateInterval('P'.$jangka.'D'));
						$tanggal_masuk =  $datetime->format('Y-m-d');
						
						$receive = array(
							'id_rec' => $id_rec,
							'id_item' => $data->id_item,
							'jumlah' => $data->jumlah - $defect,
							'defect' => $defect,
							'hargaSatuan' => $harga,
							'id_pemilik' => $id_pemilik,
							'barcode_barang' =>$data->id_item.$barcode,
							'id_supplier' => $data->supplier,
							'game_id' => $game_id,					
							);
						$receive2 = array(
							'id_rec' => $id_rec,
							'id_item' => $data->id_item,
							'jumlah' => $data->jumlah - $defect,
							// 'defect' => $defect,
							'hargaSatuan' => $harga,
							'id_pemilik' => $id_pemilik,
							'barcode_barang' =>$data->id_item.$barcode,
							'id_supplier' => $data->supplier,
							'game_id' => $game_id,
							'tanggal_masuk' =>$tanggal_masuk					
							);

						$ok2 = $this->db->insert('detail_penerimaan',$receive);
						$ok3 = $this->db->insert('gudang',$receive2);
						// echo 'insert detail '.$supp.' <br>';
					}
					else
					{
						// echo 'beda supplier '.$supp.'  total :'.$total.'<br>';
						if($total)
						{ 
							// echo $supp.' ';
							$this->db->select('ongkir');
							$this->db->where('id_suplier',$data->supplier);
							$read = $this->db->get('suplier');
							foreach ($read->result() as $dataongkir) 
							{
								$ongkir = $dataongkir->ongkir;
							}
							$total = $total + $ongkir;
							$data_header = array(
								'id_po' => $id_po,
								'id_suplier' => $supp,
								'id_petugas' => $id_pemilik,
								'tanggal_po' => $tanggal,
								'totalHarga' => $total,
								'status' =>3,
								'id_pemilik' => $id_pemilik,
								'ongkir' => $ongkir,
								'game_id' => $game_id,
								);
							$data_receive = array(
								'id_rec' => $id_rec,
								'id_po' => $id_po,
								'id_suplier' => $supp,
								'id_petugas' => $id_pemilik,
								'tanggal_receive' => $tanggal_masuk,
								'status' =>3,
								'totalHarga' => $total,
								'id_pemilik' => $id_pemilik,
								'kurir' => 'nanda',
								'game_id' => $game_id,
								);
							$this->db->insert('purchasing',$data_header);
							$this->db->insert('penerimaan_barang',$data_receive);
							// echo 'insert header '.$supp.' <br>';
							$this->load->model('mgl');
							$dataggl = array(
			                    'tgl'=> $tanggal_masuk,
			                    'idTransaksi' =>$id_po,
			                    'id_pemilik' =>$id_pemilik,
			                    'game_id' =>$game_id,
			                    'hargabeli' =>$total,
			                    'ongkir' =>$ongkir,
			                    );
			                $this->mgl->bayar_pembelian($dataggl);
							// $this->load->model('mpenjualan');
							// $haha = $this->mpenjualan->cek2();
							// if($haha->apa < 1)
							// {
							// 	$idjurnalH = 1;
							// }
							// else
							// {
							// 	$idjurnalH = $haha->apa;
							// }
							// $query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES('$tanggal_masuk',CONCAT('Pembelian ','$id_po'),'$id_pemilik',$game_id)");
							// $this->load->model('mgl');
							// $schema = $this->mgl->schema_line(1);
							// $searchArray = array("HB", "HJ", "0");
							// $replaceArray = array($total,0,0);
							// foreach ($schema as $line) {
							// 	$data_line = array(
							// 		'journal_id' => $idjurnalH,
							// 		'acc_id' => $line->acc_id,
							// 		'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
							// 		'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
							// 		'uid' => $this->session->userdata('id_petugas'),
							// 		'game_id' => $this->session->userdata('game_id')

							// 	);
							// 	$oke = $this->db->insert('gl_journal_l',$data_line);
							// 	// echo 'insert gl <br>';
							// }

							// echo 'insert detail supp :'.$data->supplier.'<br>';
							// echo 'sama supplier <br>';
						// $flag = 1;
						$id_supplier = $data->supplier;
						//echo $id_supplier;
						$barcode = mt_rand(00001, 99999);
						$this->db->select('harga');
						$this->db->where('id_item',$data->id_item);
						$this->db->where('id_suplier',$id_supplier);
						$ok = $this->db->get('detail_suplier');
						foreach ($ok->result() as $dataharga) 
						{
							$harga = $dataharga->harga;
						}
						$totalbeli = $harga * $data->jumlah;
						// echo 'harga : '.$harga.' jumlah : '.$data->jumlah.' total : '.$total.'<br>';
						$total =  $totalbeli;
						// echo 'totalfinal : '.$total.'<br>';
						$insert = array(
							'id_purchasing' => $id_po,
							'id_item' => $data->id_item,
							'jumlah' => $data->jumlah,
							'hargaSatuan' => $harga,
							'id_pemilik' =>$id_pemilik,
							'id_supplier' => $data->supplier,
							'game_id' => $game_id,
							);
						$ok = $this->db->insert('detail_purchasing',$insert);
						$randef = mt_rand(1,10);
						$defect = floor(($data->jumlah/100) *$randef);
						$this->db->select('jangka_waktu');
						$this->db->where('id_suplier',$data->supplier);
						$readread = $this->db->get('suplier');
						foreach ($readread->result() as $result) 
						{
							$jangka = $result->jangka_waktu;
						}

						$datetime = new DateTime($tanggal);
						$datetime->add(new DateInterval('P'.$jangka.'D'));
						$tanggal_masuk =  $datetime->format('Y-m-d');
						
						$receive = array(
							'id_rec' => $id_rec,
							'id_item' => $data->id_item,
							'jumlah' => $data->jumlah - $defect,
							'defect' => $defect,
							'hargaSatuan' => $harga,
							'id_pemilik' => $id_pemilik,
							'barcode_barang' =>$data->id_item.$barcode,
							'id_supplier' => $data->supplier,
							'game_id' => $game_id,					
							);
						$receive2 = array(
							'id_rec' => $id_rec,
							'id_item' => $data->id_item,
							'jumlah' => $data->jumlah - $defect,
							// 'defect' => $defect,
							'hargaSatuan' => $harga,
							'id_pemilik' => $id_pemilik,
							'barcode_barang' =>$data->id_item.$barcode,
							'id_supplier' => $data->supplier,
							'game_id' => $game_id,
							'tanggal_masuk' =>$tanggal_masuk					
							);

						$ok2 = $this->db->insert('detail_penerimaan',$receive);
						$ok3 = $this->db->insert('gudang',$receive2);
						// echo 'insert detail <br>';				
						}
						$id_po	= uniqid("PO");
	       				$id_rec	= uniqid("REC");
	       				$supp = $data->supplier;
	       				// $total = 0;
					}
					// $config[] = $data;
				}
			}
			if ($total)
			{
				$this->db->select('ongkir');
				$this->db->where('id_suplier',$supp);
				$read = $this->db->get('suplier');
				foreach ($read->result() as $dataongkir) 
				{
					$ongkir = $dataongkir->ongkir;
				}
				$total = $total + $ongkir;
				$data_header = array(
					'id_po' => $id_po,
					'id_suplier' => $supp,
					'id_petugas' => $id_pemilik,
					'tanggal_po' => $tanggal,
					'totalHarga' => $total,
					'status' =>3,
					'id_pemilik' => $id_pemilik,
					'ongkir' => $ongkir,
					'game_id' => $game_id,
					);
				$data_receive = array(
					'id_rec' => $id_rec,
					'id_po' => $id_po,
					'id_suplier' => $supp,
					'id_petugas' => $id_pemilik,
					'tanggal_receive' => $tanggal_masuk,
					'status' =>3,
					'totalHarga' => $total,
					'id_pemilik' => $id_pemilik,
					'kurir' => 'nanda',
					'game_id' => $game_id,
					);
				$this->db->insert('purchasing',$data_header);
				$this->db->insert('penerimaan_barang',$data_receive);
				// echo 'insert header <br>';
				$this->load->model('mgl');
				$dataggl = array(
                    'tgl'=> $tanggal_masuk,
                    'idTransaksi' =>$id_po,
                    'id_pemilik' =>$id_pemilik,
                    'game_id' =>$game_id,
                    'hargabeli' =>$total,
                    'ongkir' =>$ongkir,
                    );
                $this->mgl->bayar_pembelian($dataggl);
				

                // $dataggl = array(
                //     'tgl'=> $tanggal_masuk,
                //     'idTransaksi' =>uniqid('ISSUE'),
                //     'id_pemilik' =>$id_pemilik,
                //     'game_id' =>$game_id,
                //     'ongkir' =>$ongkir,
                //     );
                // $this->mgl->bayar_pembelian($dataggl);
			}
		}		
	}
public function masukGudang($id_po)
	{
		$this->db->reconnect();
		$this->db->select('totalHarga,tanggal_po,id_suplier');
		$this->db->where('id_po',$id_po);
		$read = $this->db->get('purchasing');
		foreach ($read->result() as $data) 
		{
			$dataPO = $data;
		}
		$tanggal = $this->session->userdata('tanggal');
		$this->db->select('jangka_waktu');
		$this->db->where('id_suplier',$dataPO->id_suplier);
		$readread = $this->db->get('suplier');
		foreach ($readread->result() as $result) 
		{
			$jangka = $result->jangka_waktu;
		}

		$datetime = new DateTime($tanggal);
		$datetime->add(new DateInterval('P'.$jangka.'D'));
		$tanggal_masuk =  $datetime->format('Y-m-d');


		$idtrans=$this->input->post('idtransaksi');
			$tgl =  str_replace("-","",$this->input->post('tgl'));
			$addOn = DateTime::createFromFormat('H:i:s', date('H:i:s'));
			$unique =  $addOn->getTimestamp();
			// $barcode = 'S'.$this->input->post('idSuplier').$tgl.$unique;
			$barcode = mt_rand(00001, 99999);
			$data = array(
					'idTransaksi' => uniqid('REC'),
					'idSuplier' => $dataPO->id_suplier,
					'total' => $dataPO->totalHarga,
					'tgl' => $tanggal_masuk,
					'idPenerima' => $this->session->userdata('id_retail'),
					'kurir' => '-',
					'idTran' => $id_po,
					'email' => $this->session->userdata('username'),
					'kode' => 1,
					'id_pemilik' => $this->session->userdata('id_retail'),
					'barcode' =>$barcode,
					'game_id' => $this->session->userdata('game_id'),
				);

			$this->load->model('mgudang');
			$this->mgudang->addGudang($data);
		return true;
	}
}
?>
