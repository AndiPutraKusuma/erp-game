<?php
class M_aset extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

	 //add data
	 public function addaset($data){
		$this->db->reconnect();
		$this->db->where('asset_name',$data['asset_name']);
		$this->db->where('asset_duration',$data['asset_duration']);
		$this->db->where('asset_duration_type',$data['asset_duration_type']);
		$this->db->where('asset_capacity',$data['asset_capacity']);
		$this->db->where('id_retail',$data['id_retail']);
		$read = $this->db->get('asset');
		if($read->num_rows() < 1)
		{
			$ok = $this->db->insert('asset',$data);
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('now()',CONCAT('Pembelian Asset ','$data[asset_name]'),'$data[id_retail]')");

			$this->load->model('mgl');
			$schema = $this->mgl->schema_line(3);
			$searchArray = array("HB", "HJ", "0");
			$replaceArray = array($data['asset_value'],0,0);
			foreach ($schema as $line) {
				$data_line = array(
					'journal_id' => $temp,
					'acc_id' => $line->acc_id,
					'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
					'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
					'uid' => $this->session->userdata('id_petugas'),
				);
				$oke = $this->db->insert('gl_journal_l',$data_line);
			}
		}
		return true;
	 }
	  
	public function list_aset($idPemilik){
		$this->db->reconnect();
		$this->db->select('asset_id , asset_name as name, asset_duration as duration , asset_duration_type as duration_type , asset_capacity as capacity, asset_value as value ');
		$this->db->where('id_retail',$idPemilik);
		$this->db->where('is_delete',0);
		$read = $this->db->get('asset');
	// 		$query = $this->db->query("select * from item_master
	// inner join satuan sat on sat.id_satuan=item_master.`satuan`
	// inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe` where item_master.`id_pemilik`=".$id." order by nama_item asc;");
			// $query = $this->db->query("CALL sp_list_item($id,$idgame)");
			if ($read->num_rows() > 0)
			 {
			 foreach ($read->result() as $row)
			 {
			 		$hasil[] = $row;
			 }
			 return $hasil;
			 }
			 else{
			 	return 0;
			 }
		//	return $query->num_rows();
	}
	
	public function updateAset($data,$id)
	{
		$this->db->reconnect();
		$this->db->where('asset_id',$id);
		$ok = $this->db->update('asset',$data);
		if($ok)
		{
			return $data['asset_name'];
		}
	}
}


?>
