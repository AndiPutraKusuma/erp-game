<?php
class Mmodal extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

	  public function addModal($data,$idjurnalL,$game_id){
		 $this->db->reconnect();
		if ($data['jenis_modal']==1) {
			$query=$this->db->query("INSERT INTO modal (jenis_modal,jumlah,status,id_pemilik,game_id,rate,lamaPinjam) values('$data[jenis_modal]','$data[jumlah]',0,'$data[id_pemilik]',$game_id,'$data[rate]','$data[lamaPinjam]')");
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES(NOW(),CONCAT('Peminjaman Modal','$idjurnalL'),'$data[id_pemilik]',$game_id)");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid,game_id) VALUES('$idjurnalL',2100,0,'$data[jumlah]','$data[id_pemilik]',$game_id)");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid,game_id) VALUES('$idjurnalL',1000,'$data[jumlah]',0,'$data[id_pemilik]',$game_id)");
			$id_modal = 0;
			$this->db->select('MAX(id_modal) as id');
			$this->db->where('id_pemilik',$data['id_pemilik']);
			$this->db->where('game_id',$game_id);
			$read = $this->db->get('modal');
			foreach ($read->result() as $lala) 
			{
				$id_modal = $lala->id;
			}
			$datetime = new DateTime($this->session->userdata('tanggal'));
				// $datetime->add(new DateInterval('P1D'));
				$tanggal =  $datetime->format('Y-m');
			// $tanggal = date("Y-m");
			$tanggalbaru = $tanggal.'-01';
			$data =array(
				'modal_id' => $id_modal,
				'jumlahModal' =>$data['jumlah'],
				'rate' =>$data['rate'],
				'lamaPinjam' =>$data['lamaPinjam'],
				'id_pemilik' =>$data['id_pemilik'],
				'game_id' =>$game_id,
				'tanggal' => $tanggalbaru,
				);
			//print_r($data);
			$this->detail_modal($data);
		}
		else{
			$query=$this->db->query("INSERT INTO modal (jenis_modal,jumlah,status,id_pemilik,game_id) values('$data[jenis_modal]','$data[jumlah]',1,'$data[id_pemilik]',$game_id)");
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES(NOW(),CONCAT('Peminjaman Modal','$idjurnalL'),'$data[id_pemilik]',$game_id)");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid,game_id) VALUES('$idjurnalL',3000,0,'$data[jumlah]','$data[id_pemilik]',$game_id)");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid,game_id) VALUES('$idjurnalL',1000,'$data[jumlah]',0,'$data[id_pemilik]',$game_id)");
		}

	 }
	public function cek($id){
		$this->db->reconnect();
		$game_id = $this->session->userdata('game_id');
		$query2=$this->db->query("SELECT (sum(l.line_debit) - sum(l.line_credit)) as total from gl_journal_l l where l.uid=$id and game_id = $game_id and l.acc_id=1000");
		$row=$query2->row();
			$hasil=$row->total;
			return $hasil;
	}
	public function cekjenis($id)
	{
		$query = $this->db->query("SELECT * from modal WHERE id_modal=$id");
		return $query->row();
	}
	 public function bayarModal($id,$data,$idjurnalL){
		 $this->db->reconnect();
		$query=$this->db->query("UPDATE modal set status=1 where id_modal=$id");
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES(NOW(),CONCAT('Pembayaran Modal','$idjurnalL'),'$data->id_pemilik')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',2100,'$data->jumlah',0,'$data->id_pemilik')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',1000,0,'$data->jumlah','$data->id_pemilik')");

	 }

	public function list_modal($id){
		$this->db->reconnect();
		$game_id =$this->session->userdata('game_id');
			$query = $this->db->query("SELECT * FROM modal where id_pemilik= $id and game_id = $game_id");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}

	public function hapus($id)
	{
		$this->db->delete('modal',array('id_modal' => $id));
	}

	public function edit($where,$data)
	{
		$this->db->where('id_modal',$where);
		$this->db->update('modal',$data);
	}

	public function total($idPemilik)
	{
		$this->db->select_sum('jumlah');
		$this->db->where('id_pemilik',$idPemilik);
		$query = $this->db->get('modal');
		// foreach ($query->result() as $value) {
		// 	$hasil[] = $value;
		// }
		return $query->row()->jumlah;
	}
	
	public function detail_modal($data)
	{
		// echo '<br>iki lho<br>';
		$lamaPinjam =  $data['lamaPinjam'];
		$jumlah = $data['jumlahModal'];
		$tanggalawal = $data['tanggal'];
		$rate1 = $data['rate']/100;
		$rate2 = pow((1+$rate1), 1/12);
		$rate = $rate2 - 1;

		$id_pemilik = $data['id_pemilik'];
		$modal_id = $data['modal_id'];
		$game_id = $data['game_id'];
		$i1 = $jumlah * $rate;
		$l1 = pow(1+$rate,-$lamaPinjam);
		$h1 = (1-$l1);
		// $h1 = (1-pow((1+$rate),-($lamaPinjam)));
		$t1 = $i1/$h1;		
		for($i = 1 ; $i<=$lamaPinjam ; $i++)
		{
			$datetime = new DateTime($tanggalawal);
			$datetime->add(new DateInterval('P'.$i.'M'));
			$tanggal =  $datetime->format('Y-m-d');
			// $this->session->set_userdata('tanggal',$tanggal);
			$i1 = $jumlah * $rate;
			$l1 = pow(1+$rate,-$lamaPinjam);
			$h1 = (1-$l1);
			// $h1 = (1-pow((1+$rate),-($lamaPinjam)));
			// $t1 = $i1/$h1;
			$awal = $jumlah;
			echo 'loan : '.$jumlah; 
			$p1 = $t1-$i1;
			echo ' schedule_payment :'.$t1.' principal : '.$p1.' interest : '.$i1.'<br>';
			$jumlah = $jumlah-$p1;
			$lala = $jumlah; 
			// echo ' jumlah : '.$jumlah.' p1 '.$p1.'<br>';
			$data2 = array(
					'modal_id' => $modal_id,
					'schedule_payment' => $t1,
					'principal' => $p1,
					'interest' => $i1,
					'id_pemilik' => $id_pemilik,
					'game_id' => $game_id,
					'payment_number' => $i,
					'payment_period' => $tanggal,
					'loan_value' => $awal,
					);
			// echo $i.' '.$jumlah;
			// echo '<br>';
			$this->db->insert('loan_calculator',$data2);
		}
	}

	public function rincianModal($id)
	{
		$this->db->reconnect();
		$this->db->where('modal_id',$id);
		$query = $this->db->get('loan_calculator');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
		}
		else
		{
				return 0;
		}
	}

	public function bayarModal2($id_pemilik,$game_id,$tanggal){
		 $this->db->reconnect();
		 $date = date($tanggal);
		 $tahun = date('Y', strtotime($date));
		 $bulan = date('m', strtotime($date));
		 $this->load->model('mpenjualan');
    		$haha = $this->mpenjualan->cek2();
    		$idjurnalL = $haha->apa;
		 $this->db->select('loan_calculator.* , modal.lamaPinjam');
		 $this->db->where('payment_period',$date);
		 //$this->db->where('EXTRACT(MONTH FROM payment_period) = ',$bulan);
		 //$this->db->where('EXTRACT(YEAR FROM payment_period) = ',$tahun);
		 //$this->db->where('loan_calculator.id_pemilik',1111);
		 //$this->db->where('loan_calculator.game_id',16);
		 $this->db->where('loan_calculator.id_pemilik',$id_pemilik);
		 $this->db->where('loan_calculator.game_id',$game_id);
		 $this->db->from('loan_calculator');
		 $this->db->join('modal','modal.id_modal = loan_calculator.modal_id','inner');
		 $read = $this->db->get();
		 foreach ($read->result() as $value) 
		 {
		 	$data2[] = $value;
			$query=$this->db->query("UPDATE loan_calculator set loan_status=1 where loan_id='$value->loan_id'");
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES('$date',CONCAT('Pembayaran Cicilan Modal ','$date'),'$value->id_pemilik','$value->game_id')");
			//total
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid,game_id) VALUES('$idjurnalL',1000,0,'$value->schedule_payment','$value->id_pemilik','$value->game_id')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid,game_id) VALUES('$idjurnalL',2100,'$value->principal',0,'$value->id_pemilik','$value->game_id')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid,game_id) VALUES('$idjurnalL',6012,'$value->interest',0,'$value->id_pemilik','$value->game_id')");
		 }
		 // print_r($data2);
		 return true;
	 }	

}


?>
