<?php class M_role extends CI_Model {

	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

    public function read($id){
        $this->db->reconnect();
        $this->db->select();
        $this->db->where('parent_roleId',$id);
        $this->db->from('tbl_roles');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }

    public function view($id){
        $this->db->reconnect();
        $this->db->select('role');
        $this->db->where('roleId',$id);
        $this->db->from('tbl_roles');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result = $data->role;
            }        
            return $result;
        }
    }

    public function create($data){
        $this->db->reconnect();
        // $query=$this->db->query("CALL sp_input_petugas('$data[ktp]','$data[nama]','$data[email]','$data[passwd]')");
        $query = $this->db->query("SELECT `role`,`parent_roleId` FROM `tbl_roles` WHERE `parent_roleId` = '$data[parent_roleId]' AND `role` = '$data[role]'");
        // print_r($query->num_rows());
        if($query->num_rows()==0){
            $ok = $this->db->insert('tbl_roles',$data);
            // echo "yeay";
            return $ok; 
        }
        else{
            // echo "yaah";
            return 0;
        }
        // $row=$query->row();
        // return $row->cek;
    }

    public function update($data,$id){
        $this->db->reconnect();
        $query = $this->db->query("SELECT `role`,`parent_roleId` FROM `tbl_roles` WHERE `parent_roleId` = '$data[parent_roleId]' AND `role` = '$data[role]'");
        if($query->num_rows()==0){
            $this->db->where('roleId',$id);
            $ok = $this->db->update('tbl_roles',$data);
            // echo "yeay";
            return $ok; 
        }
        else{
            // echo "yaah";
            return 0;
        }
                
    }
    //tambahan andi
    public function listRole()
    {
        $company_id = $this->session->userdata('company');
        $this->db->reconnect();
        $this->db->select('roleId as id , role as name');
        $this->db->where('company_id',$company_id);
        $read = $this->db->get('tbl_roles');
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($read->result() as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }
    //sampe sini tambahan andi
}
?>
