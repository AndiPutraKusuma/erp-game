<?php
set_time_limit(90000000); // 15 minutes


class M_expense extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

	 public function listExpense($data)
	 {
	 	$this->db->reconnect();
	 	// $this->db->select('expenseType_name as name , expense_jumlah as jumlah , expense_value as value');
	 	$this->db->select('expense_code  as code, expense_jumlah as jumlah , expense_value as value');
	 	$this->db->from('expense');
	 	// $this->db->join('expense_type','expense_type.expenseType_id = expense.expense_code and expense.is_delete = 0');
	 	$this->db->where('expense.id_pemilik',$data['id_pemilik']);
	 	$this->db->where('expense.game_id',$data['game_id']);
	 	$read = $this->db->get();
	 	$row = $read->num_rows();
	 	if($row)
	 	{
		 	foreach ($read->result() as $data) 
		 	{
		 		$code = $data->code;
		 		if($code == 1)
		 		{
		 			$hasil['iklan'] = $data->value;
		 		}
		 		elseif ($code ==2) 
		 		{
		 			$hasil['jumlah_pekerja'] = $data->jumlah;
		 			$hasil['gaji_pekerja'] = $data->value;
		 		}
		 		elseif ($code ==3) 
		 		{
		 			$hasil['air'] = $data->value;
		 				 		}
		 		elseif ($code ==4) 
		 		{
		 			$hasil['listrik'] = $data->value;
		 				 		}
		 		elseif ($code ==5) 
		 		{
		 			$hasil['asuransi'] = $data->value;
		 				 		}
		 		elseif ($code ==6) 
		 		{
		 			$hasil['depresiasi'] = $data->value;
		 		}
		 		elseif ($code ==7) 
		 		{
		 			$hasil['amortisasi'] = $data->value;
		 		}

		 	}
	 	}
	 	else
	 	{
	 		$hasil = array();
	 	}
	 	return $hasil;
	 }

	 public function addExpense($data)
	 {
	 	$this->db->reconnect();
	 	$this->db->where('expense_code',$data['expense_code']);
	 	$this->db->where('id_pemilik',$data['id_pemilik']);
	 	$this->db->where('game_id',$data['game_id']);
	 	$read = $this->db->get('expense');
	 	$num_row = $read->num_rows();
	 	if($num_row)
	 	{
	 		return false;
	 	}
	 	else
	 	{
		 	$this->db->insert('expense',$data);
		 	return true;
	 	}

	 }

	 public function update($dataupdate)
	 {
	 	$this->db->reconnect();
	 	foreach ($dataupdate as $dataupdates) 
	 	{
	 		$this->db->where('expense_code',$dataupdates['expense_code']);
	 		$this->db->where('id_pemilik',$dataupdates['id_pemilik']);
	 		$this->db->where('game_id',$dataupdates['game_id']);
	 		$read = $this->db->get('expense');
	 		$row = $read->num_rows();
	 		if($row)
	 		{
	 			$query  = $this->updateExpense($dataupdates);
	 			if(!$query)
	 			{
	 				break;
	 				return false;
	 			}
	 		}
	 		else
	 		{
	 			$query = $this->addExpense($dataupdates);
	 			if(!$query)
	 			{
	 				break;
	 				return false;
	 			}
	 		}
	 	}
	 	return true;
	 }

	 public function updateExpense($dataupdate)
	 {
	 	$this->db->reconnect();

	 	$this->db->where('expense_code',$dataupdate['expense_code']);
	 	$this->db->where('id_pemilik',$dataupdate['id_pemilik']);
	 	$this->db->where('game_id',$dataupdate['game_id']);
	 	$query = $this->db->update('expense',$dataupdate);
	 	if($query)
	 	{
	 		return true;
	 	}
	 	else
	 	{
	 		return false;
	 	}
	 }

	 public function addExpenseType($data)
	 {
	 	$this->db->reconnect();
	 	$query  = $this->db->insert('expense_type',$data);
	 	if($query)
	 	{
	 		return true;
	 	}
	 	else
	 	{
	 		return false;
	 	}
	 }

		public function bayarExpense()
	{
		$game_id = $this->session->userdata('game_id');
		$this->db->reconnect();
		$this->db->select('`expenseType_name` AS `name`, `expenseType_debit` AS `debit`, `expenseType_credit` AS `credit`, (expense_jumlah * expense_value) AS total, `expense`.`id_pemilik`');
		$this->db->from('expense');
		$this->db->join('expense_type','expense.`expense_code` = expense_type.`expenseType_id`','INNER');
		$this->db->where('expense.is_delete',0);
		$this->db->where('expense.game_id',$game_id);
		$read = $this->db->get();
		foreach ($read->result() as $data) 
		{
			$yeye[] = $data->id_pemilik;
			$id_pemilik = $data->id_pemilik;
			$debit = $data->debit;
			$credit = $data->credit;
			$name = $data->name;
			$header = $name.' '.$this->session->userdata('tanggal');
			$jumlah = $data->total;
			// echo 'insert data '.$header.' '.$id_pemilik.'<br>';
			$tanggal = $this->session->userdata('tanggal');

			//insert gl header
			$query=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES('$tanggal','$header','$id_pemilik','$game_id')");
			//insert gl l
			$query2=$this->db->query("SELECT id from gl_journal_h where journal_name = '$header' AND uid = '$id_pemilik' and game_id = '$game_id' AND period_id = '$tanggal'");
	    	$jurnal_ida = $query2->row();
	    	$jurnal_id = $jurnal_ida->id;
	    	$data_line1 = array(
				'journal_id' => $jurnal_id,
				'acc_id' => $debit,
				'line_debit' => (int)$jumlah,
				'line_credit' => 0,
				'uid' => $id_pemilik,
				'game_id' => $game_id,
			);
			$oke1 = $this->db->insert('gl_journal_l',$data_line1);
			$data_line2 = array(
				'journal_id' => $jurnal_id,
				'acc_id' => $credit,
				'line_debit' => 0,
				'line_credit' => (int)$jumlah,
				'uid' => $id_pemilik,
				'game_id' => $game_id,
			);
			$oke2 = $this->db->insert('gl_journal_l',$data_line2);
			// if ($oke1 && $oke2) {
			// 	return true;
			// }
			// $yeye[] = $data;
		}
		// print_r($yeye);
	}

	public function updateExpenseData($game_id)
	{
		$this->db->reconnect();
		$total = 0;
        	$depresiasi = 0;
        	$this->load->model('m_game');
        	$this->load->model('mgl');
        	$user = $this->m_game->searchUser($game_id);
        	foreach ($user as $id_pemilik) 
        	{
        		$persediaan = $this->mgl->searchPersediaan($game_id,$id_pemilik);

		        $this->db->select('asset_value,asset_duration,asset_duration_type');
		        $this->db->where('game_id',$game_id);
	        	$this->db->where('id_retail',$id_pemilik);
		        $this->db->where('asset_type',1);
		        $read22 =  $this->db->get('asset');
	       		foreach ($read22->result() as $data) 
			{
	            		$value = $data->asset_value;
	           		$duration = $data->asset_duration;
	            		$duration_type = $data->asset_duration_type;
	            		if($duration_type == 'y')
	            		{
	                		$dep_temp = round($value/($duration*12));
	            		}
	            		elseif ($duration_type == 'm') 
	            		{
	                		$dep_temp = round($value/$duration);     
	            		}
	            		$total = $total+$value;
	            		$depresiasi = abs($depresiasi) + abs($dep_temp);
	        	}
	        	// $total = $total + $persediaan;
	        	$asuransi = round((2/1000)*(($total/12) + abs($persediaan)));
	        	$dataupdate = array();
	        	array_push(
		        	$dataupdate, 
		        	array(
						'expense_code' => 5,
						'expense_jumlah' => 1,
						'expense_value' => $asuransi,
						'id_pemilik' => $id_pemilik ,
			            		'game_id' => $game_id , 
						)
	                );
			$query = $this->update($dataupdate);
        	}
	}
	public function bayarAmortisasi()
	{
		$game_id = $this->session->userdata('game_id');
		$this->db->reconnect();
		$this->db->select('`expenseType_name` AS `name`, `expenseType_debit` AS `debit`, `expenseType_credit` AS `credit`, (expense_jumlah * expense_value) AS total, `expense`.`id_pemilik`');
		$this->db->from('expense');
		$this->db->join('expense_type','expense.`expense_code` = expense_type.`expenseType_id`','INNER');
		$this->db->where('expense.expense_code',7);
		$this->db->where('expense.is_delete',0);
		$this->db->where('expense.game_id',$game_id);

		$read = $this->db->get();
		foreach ($read->result() as $data) 
		{
			$yeye[] = $data->id_pemilik;
			$id_pemilik = $data->id_pemilik;
			$debit = $data->debit;
			$credit = $data->credit;
			$name = $data->name;
			$header = $name.' '.$this->session->userdata('tanggal');
			$jumlah = $data->total;
			// echo 'insert data '.$header.' '.$id_pemilik.'<br>';
			$tanggal = $this->session->userdata('tanggal');

			//insert gl header
			$query=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES('$tanggal','$header','$id_pemilik','$game_id')");
			//insert gl l
			$query2=$this->db->query("SELECT id from gl_journal_h where journal_name = '$header' AND uid = '$id_pemilik' and game_id = '$game_id' AND period_id = '$tanggal'");
	    	$jurnal_ida = $query2->row();
	    	$jurnal_id = $jurnal_ida->id;
	    	$data_line1 = array(
				'journal_id' => $jurnal_id,
				'acc_id' => $debit,
				'line_debit' => (int)$jumlah,
				'line_credit' => 0,
				'uid' => $id_pemilik,
				'game_id' => $game_id,
			);
			$oke1 = $this->db->insert('gl_journal_l',$data_line1);
			$data_line2 = array(
				'journal_id' => $jurnal_id,
				'acc_id' => $credit,
				'line_debit' => 0,
				'line_credit' => (int)$jumlah,
				'uid' => $id_pemilik,
				'game_id' => $game_id,
			);
			$oke2 = $this->db->insert('gl_journal_l',$data_line2);
			// if ($oke1 && $oke2) {
			// 	return true;
			// }
			// $yeye[] = $data;
		}
		// print_r($yeye);
	}
}


?>
