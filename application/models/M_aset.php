<?php
class M_aset extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

	 //add data
	 public function addaset($data){
		$this->db->reconnect();
		$this->db->where('asset_name',$data['asset_name']);
		$this->db->where('asset_duration',$data['asset_duration']);
		$this->db->where('asset_duration_type',$data['asset_duration_type']);
		$this->db->where('asset_capacity',$data['asset_capacity']);
		$this->db->where('id_retail',$data['id_retail']);
		$this->db->where('game_id',$data['game_id']);
		$read = $this->db->get('asset');
		if($read->num_rows() < 1)
		{
			$ok = $this->db->insert('asset',$data);
		}
		return true;
	 }

	 public function addaset2($datas){
		$this->db->reconnect();
		foreach ($datas as $data) 
		{
			$this->db->where('asset_name',$data['asset_name']);
			$this->db->where('asset_duration',$data['asset_duration']);
			$this->db->where('asset_duration_type',$data['asset_duration_type']);
			$this->db->where('asset_capacity',$data['asset_capacity']);
			$this->db->where('id_retail',$data['id_retail']);
			$this->db->where('game_id',$data['game_id']);
			$read = $this->db->get('asset');
			if($read->num_rows() < 1)
			{
				$ok = $this->db->insert('asset',$data);
			}
			
		}
		return true;
	 }
	  
	public function list_aset($idPemilik){
		$this->db->reconnect();
		$this->db->select('asset_id , asset_name as name, asset_duration as duration , asset_duration_type as duration_type , asset_capacity as capacity, asset_value as value, status');
		$this->db->where('game_id',$this->session->userdata('game_id'));
		$this->db->where('id_retail',$idPemilik);
		$this->db->where('is_delete',0);
		$read = $this->db->get('asset');
	// 		$query = $this->db->query("select * from item_master
	// inner join satuan sat on sat.id_satuan=item_master.`satuan`
	// inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe` where item_master.`id_pemilik`=".$id." order by nama_item asc;");
			// $query = $this->db->query("CALL sp_list_item($id,$idgame)");
			if ($read->num_rows() > 0)
			 {
			 foreach ($read->result() as $row)
			 {
			 		$hasil[] = $row;
			 }
			 return $hasil;
			 }
			 else{
			 	return 0;
			 }
		//	return $query->num_rows();
	}
	
	public function updateAset($data,$id)
	{
		$this->db->reconnect();
		$this->db->where('asset_id',$id);
		$ok = $this->db->update('asset',$data);
		// if($ok)
		// {
		// 	return $data['asset_name'];
		// }
		return $ok;
	}

	public function cekStatus($id)
	{
		$this->db->reconnect();
		$this->db->select('status');
		$this->db->where('asset_id',$id);
		$read = $this->db->get('asset');
		if ($read->num_rows() > 0)
		{
			foreach ($read->result() as $row)
			{
				$hasil = $row;
			}
			return $hasil;
		}
		else{
			return 0;
		}	
	}

	public function beliAsset($id)
	{
		$game_id = $this->session->userdata('game_id');
		$this->db->reconnect();
		$this->db->select('asset_name, asset_value, id_retail');
		$this->db->where('asset_id',$id);
		$read = $this->db->get('asset');
		if ($read->num_rows() > 0)
		{
			foreach ($read->result() as $row)
			{
				$data = $row;
			}
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid,game_id) VALUES('now()',CONCAT('Pembelian Asset ','$data->asset_name'),'$data->id_retail', '$game_id')");

			$query3 = $this->db->query("SELECT id FROM `gl_journal_h` WHERE journal_name LIKE CONCAT('Pembelian Asset ','$data->asset_name') and uid='$data->id_retail' and game_id = '$game_id'");
			foreach ($query3->result() as $item) {
				$temp = $item;
			}
			$this->load->model('mgl');
			$schema = $this->mgl->schema_line(3);
			$searchArray = array("HB", "HJ", "0");
			$replaceArray = array($data->asset_value,0,0);
			foreach ($schema as $line) {
				$data_line = array(
					'journal_id' => $temp->id,
					'acc_id' => $line->acc_id,
					'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
					'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
					'uid' => $this->session->userdata('id_petugas'),
					'game_id' => $game_id,
				);
				$oke = $this->db->insert('gl_journal_l',$data_line);
			}
		}
		else{
			return 0;
		}
	}

	// public function depresiasiAsset($id)
	// {
	// 	$this->db->reconnect();
	// 	$this->db->select('asset_name, asset_value, asset_duration, asset_duration_type, id_retail');
	// 	$this->db->where('asset_id',$id);
	// 	$read = $this->db->get('asset');
	// 	if ($read->num_rows() > 0)
	// 	{
	// 		foreach ($read->result() as $row)
	// 		{
	// 			$data = $row;
	// 		}
	// 		$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES(DATEADD('now()'),CONCAT('Pembelian Asset ','$data[asset_name]'),'$data[id_retail]')");

	// 		$this->load->model('mgl');
	// 		$schema = $this->mgl->schema_line(3);
	// 		$searchArray = array("HB", "HJ", "0");
	// 		$replaceArray = array($data['asset_value'],0,0);
	// 		foreach ($schema as $line) {
	// 			$data_line = array(
	// 				'journal_id' => $temp,
	// 				'acc_id' => $line->acc_id,
	// 				'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
	// 				'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
	// 				'uid' => $this->session->userdata('id_petugas'),
	// 			);
	// 			$oke = $this->db->insert('gl_journal_l',$data_line);
	// 		}
	// 		if ($data['asset_duration_type'] == 'm') {
	// 			for ($i=0; $i < $data['asset_duration']; $i++) { 
	// 				$querya=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES(DATEADD(month, '$data[asset_duration]', 'now()'),CONCAT('Penyusutan Asset ','$data[asset_name]'),'$data[id_retail]')");
	// 				$queryb1=$this->db->query("INSERT INTO gl_journal_l(journal_id,journal_name,uid) VALUES(DATEADD(month, '$data[asset_duration]', 'now()'),CONCAT('Penyusutan Asset ','$data[asset_name]'),'$data[id_retail]')");
	// 			}
	// 		}
			
	// 	}
	// 	else{
	// 		return 0;
	// 	}
	// }
}


?>
