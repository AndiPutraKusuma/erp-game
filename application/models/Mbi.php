<?php class Mbi extends CI_Model {

	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

    public function read_rop($id){
        $this->db->reconnect();
        $this->db->select();
        $this->db->where('parent_id',$id);
        $this->db->from('core_company');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }

    public function create_rop($data){
        $this->db->reconnect();
        // $uid = $this->session->userdata('id_retail');
        // $query=$this->db->query("CALL sp_input_petugas('$data[ktp]','$data[nama]','$data[email]','$data[passwd]')");
        $query = $this->db->query("SELECT `id`,`id_item`,`id_pemilik` FROM `bi_rop` WHERE `id_item` = '$data[id_item]' AND `id_pemilik` = '$data[id_pemilik]'");
        // print_r($query->num_rows());
        if($query->num_rows()==0){
            $ok = $this->db->insert('bi_rop',$data);
            // echo "yeay";
            return $ok; 
        }
        else{
            $child = $query->result();
            foreach ($child as $item) 
            {
                $id = $item->id;
            }
            $this->db->where('id',$id);
            $ok = $this->db->update('bi_rop',$data);
            return $ok;
            // echo "yaah";
            // return 0;
        }
        // $row=$query->row();
        // return $row->cek;
    }

    public function update_rop($data){
        $this->db->reconnect();
        $id =0;
        // $uid = $this->session->userdata('id_retail');
        // $query=$this->db->query("CALL sp_input_petugas('$data[ktp]','$data[nama]','$data[email]','$data[passwd]')");
        $jumlah = 0;
        $update_data = $this->db->query("SELECT penjualan.tanggal, SUM(detail_penjualan.jumlah) as jual, detail_penjualan.id_so FROM penjualan, detail_penjualan WHERE penjualan.id_so = detail_penjualan.id_so AND detail_penjualan.id_item = '$data[id_item]' AND penjualan.id_pemilik = '$data[id_pemilik]' GROUP BY penjualan.tanggal ORDER BY penjualan.tanggal DESC LIMIT 100 ");
        $avg_lt = $this->db->query("SELECT AVG(DATEDIFF(penerimaan_barang.tanggal_receive,purchasing.tanggal_po)) as avg_lt FROM purchasing, penerimaan_barang WHERE purchasing.id_pemilik = '$data[id_pemilik]' AND purchasing.id_po in (SELECT detail_purchasing.id_purchasing from detail_purchasing where id_item = '$data[id_item]' AND id_pemilik = '$data[id_pemilik]') AND penerimaan_barang.id_po = purchasing.id_po ORDER BY penerimaan_barang.tanggal_receive DESC LIMIT 10");
        $data1 = $avg_lt->result();
        foreach ($data1 as $item) 
        {
            $av_lt = $item->avg_lt;
        }
        $max_lt = $this->db->query("SELECT MAX(DATEDIFF(penerimaan_barang.tanggal_receive,purchasing.tanggal_po)) as maxlt FROM purchasing, penerimaan_barang WHERE purchasing.id_pemilik = '$data[id_pemilik]' AND purchasing.id_po in (SELECT detail_purchasing.id_purchasing from detail_purchasing where id_item = '$data[id_item]' AND id_pemilik = '$data[id_pemilik]') AND penerimaan_barang.id_po = purchasing.id_po");
        $data2 = $max_lt->result();
        foreach ($data2 as $item) 
        {
            $maxlt = $item->maxlt;
        }
        $max_penj = $this->db->query("SELECT MAX(A.max_jual) as max_jual FROM (SELECT SUM(detail_penjualan.jumlah) as max_jual FROM penjualan, detail_penjualan WHERE penjualan.id_so = detail_penjualan.id_so AND detail_penjualan.id_item = '$data[id_item]' AND penjualan.id_pemilik = '$data[id_pemilik]' GROUP BY penjualan.tanggal)A");
        $data3 = $max_penj->result();
        foreach ($data3 as $item) 
        {
            $max_jual = $item->max_jual;
        }
        // print_r($query->num_rows());
        $jual = $update_data->result();
        foreach ($jual as $item) {
            $jumlah = $jumlah+(int)$item->jual;
        }
        $data_jual = $jumlah/$update_data->num_rows();
        $query = $this->db->query("SELECT `id`,`id_item`,`id_pemilik` FROM `bi_rop` WHERE `id_item` = '$data[id_item]' AND `id_pemilik` = '$data[id_pemilik]'");
        $child = $query->result();
        foreach ($child as $item) 
        {
            $id = $item->id;
        }
        $data_rop = array(
            'id_item' => $data['id_item'],
            'id_pemilik' => $data['id_pemilik'],
            'penjualan' => $data_jual,
            'safety_stock' => ($max_jual*$maxlt)-($data_jual*$av_lt),
            );
        $this->db->where('id_item',$data['id_item']);
        $this->db->where('id_pemilik',$data['id_pemilik']);
        $ok = $this->db->update('bi_rop',$data_rop);
        return $ok;
        // echo "yaah";
        // return 0;
        // $row=$query->row();
        // return $row->cek;
    }

    public function update(){
        $this->db->reconnect();
        $query = $this->db->query("SELECT `id_item` FROM `item_master`");
        $uid = $this->session->userdata('id_retail');
        // print_r($query->num_rows());
        if($query->num_rows() >0 )
        {
            foreach ($query->result() as $data) 
            {
                // $result[] = $data;
                $data_rop = array('id_item' => $data->id_item, 'id_pemilik' => $uid);
                $ok = $this->create_rop($data_rop);
                $ok2 = $this->update_rop($data_rop);
                // print_r($data_rop);
            }        
        }
        if ($ok && $ok2) {
            return 'success';
        } else {
            return 'fail';
        }
                
    }
    public function update2(){
        $this->db->reconnect();
        $query = $this->db->query("SELECT `id_item` FROM `item_master`");
        $uid = $this->session->userdata('id_retail');
        // print_r($query->num_rows());
        if($query->num_rows() >0 )
        {
            foreach ($query->result() as $data) 
            {
                // $result[] = $data;
                $data_rop = array('barang_id' => $data->id_item, 'id_retail' => $uid);
                $ok = $this->create_eoq($data_rop);
                $ok2 = $this->update_eoq($data_rop);
                // print_r($data_rop);
            }        
        }
        if ($ok && $ok2) {
            return 'success';
        } else {
            return 'fail';
        }
                
    }

    public function getId($email){
        // print_r($email);
        $this->db->reconnect();
        $query = $this->db->query("SELECT `company_id` FROM `petugas` where `email` = '$email'");
        // $query = "SELECT `company_id` FROM `petugas` where `email` = '$email'";
        // $this->db->select('company_id');
        // $this->db->where('email',$email);
        // $this->db->from('petugas');
        // $read = $this->db->get();
        // $child = $read->result();
        // $row = $read->num_rows();
        // var_dump($query);
        // if($row >0 )
        // {
        //     foreach ($child as $data) 
        //     {
        //         $result[] = $data;
        //     }        
        //     return $result;
        // }
        $row=$query->row();
        return $row->company_id;
    }

    public function item(){
        $this->db->reconnect();
        $query = $this->db->query("SELECT `id_item` FROM `item_master`");
        $child = $query->result();
        $row = $query->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }

    public function getComp($id){
        $this->db->reconnect();
        // $this->db->select('');
        // $this->db->where('parent_id',$id);
        // $this->db->from('core_company');
        $read = $this->db->query("SELECT id, company_code, company_name FROM core_company WHERE id = '$id' OR parent_id = '$id'");
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }

    public function findCompany($id)
    {
        $where = 'id ='.$id.' OR parent_id = '.$id;
        $result = array();
        $this->db->reconnect();
        $this->db->select('id');
        $this->db->where($where);
        $read = $this->db->get('core_company');
        if($read->num_rows() > 0 )
        {    
            foreach ($read->result() as $data) 
            {
                $result[] = $data->id; 
            }
        }
        return $result;
    }

    public function insert_data()
    {
        $this->db->reconnect();
        $this->db->select('suplier.id_suplier as supp_id, jangka_waktu, ongkir, config_value, id_item, harga');
        $this->db->where('suplier.id_pemilik',0);
        $this->db->from('suplier');
        $this->db->join('detail_suplier','detail_suplier.id_suplier = suplier.id_suplier','LEFT');
        $this->db->join('config','config.config_code = suplier.return_barang','LEFT');
        $read = $this->db->get();
        foreach ($read->result() as $data) 
        {
            $hasil[] = $data;
        }
        foreach ($hasil as $item) {
            $data = array(
                'supp_id' => $item->supp_id,
                'barang_id' => $item->id_item,
                'quality' => $item->config_value*0.419,
                'cost' => ($item->ongkir+$item->harga)*(-0.263),
                'deliv' => $item->jangka_waktu*(-0.160),
                'flex' => $item->config_value*0.097,
                'respon' =>$item->config_value*0.062,
                'total' => ($item->config_value*0.419)+(($item->ongkir+$item->harga)*(-0.263))+($item->jangka_waktu*(-0.160))+($item->config_value*0.097)+($item->config_value*0.062),
                'id_retail' => 0,
            );
            $this->db->insert('bi_supplier',$data);
        }
        return 'success';

    }

    public function list_produk_pagination($id, $awal, $hitung){
        $this->db->reconnect();
        $this->db->select("id_suplier,nama_item,item_master.`id_item`,subKategori_name as nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi");
        $this->db->from('penerimaan_barang');
        $this->db->join('gudang','gudang.`id_rec`=penerimaan_barang.`id_rec` and gudang.jumlah >0 ','INNER');
        $this->db->join('item_master','item_master.`id_item`=gudang.`id_item`','INNER');
        $this->db->join('satuan as sat','sat.id_satuan=item_master.`satuan`','INNER');
        $this->db->join('sub_kategori','sub_kategori.subKategori_kode=item_master.`tipe`','INNER');
        //$this->db->join('tipe_item','tipe_item.`id_tipe_item`=item_master.`tipe`','INNER');
        if($this->session->userdata('game'))
        {
            $this->db->where('gudang.`id_pemilik`',$id);
            $this->db->where('item_master.`id_pemilik` = 0');
        }
        else
        {
            $this->db->where('item_master.`id_pemilik`',$id);
        }
        $this->db->group_by('item_master.id_item');
        $this->db->order_by('nama_item','ASC');
        $this->db->limit($hitung, $awal);
        $query = $this->db->get();
            if ($query->num_rows() > 0){
                return $query->result();
            }else{
                return 0;
            }
    }

    public function trendHarian($tanggal, $order, $sort){
        $game_id = $this->session->userdata('game_id');
        $uid = $this->session->userdata('id_retail');
        $this->db->reconnect();
        $this->db->select('d.id_item as id_item, i.nama_item as nama_item, sum(d.jumlah) as total, d.harga as hj, 
            s.harga as hb, (sum(d.jumlah)*d.harga) as dapat, (d.harga - s.harga)*(sum(d.jumlah)) as profit');
        $this->db->from('detail_penjualan d');
        $this->db->join('penjualan p','p.id_so = d.id_so','INNER');
        $this->db->join('gudang g','g.barcode_barang = d.barcode_barang','INNER');
        $this->db->join('detail_suplier s','g.id_supplier = s.id_suplier and g.id_item = s.id_item and s.id_item = d.id_item','INNER');
        $this->db->join('item_master i','d.id_item = i.id_item','INNER');


        $this->db->where('p.id_petugas',$uid);
        $this->db->where('p.tanggal',$tanggal);
        $this->db->where('p.game_id',$game_id);
        $this->db->group_by('d.id_item');
        $this->db->order_by($sort,$order);        
        $query = $this->db->get();

        // $query = $this->db->query("SELECT d.id_item as id_item, i.nama_item as nama_item, sum(d.jumlah) as total, d.harga as hj, 
        //     s.harga as hb, (sum(d.jumlah)*d.harga) as dapat, (d.harga - s.harga)*(sum(d.jumlah)) as profit 
        //     FROM `detail_penjualan` d, `penjualan` p, `detail_suplier` s, `item_master` i 
        //     WHERE p.id_so = d.id_so AND d.id_suplier = s.id_suplier and d.id_item = s.id_item AND d.id_item = i.id_item AND p.id_petugas = '$uid' AND p.tanggal = '$tanggal' AND p.game_id = '$game_id' GROUP BY d.id_item ORDER BY $sort $order");
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                    $hasil[] = $row;
            }
            return $hasil;
        }
        else{
            return 0;
        }
    }

    public function king(){
	$game_id = 4;
        //$game_id = $this->session->userdata('game_id');
        $uid = $this->session->userdata('id_retail');
        $this->db->reconnect();
        $this->db->select('calendar.dt as ds, SUM(detail_penjualan.jumlah) as y');
        $this->db->from('penjualan');
	$this->db->join('detail_penjualan','penjualan.id_so = detail_penjualan.id_so and detail_penjualan.game_id = '.$game_id,'INNER');
        $this->db->join('calendar','calendar.dt = penjualan.tanggal','RIGHT');
        $this->db->where('calendar.dt BETWEEN (DATE_ADD(CURRENT_DATE(), INTERVAL -30 DAY) and CURRENT_DATE())');
        $this->db->where('penjualan.id_so = detail_penjualan.id_so and detail_penjualan.id_item = 1200011121 or detail_penjualan.id_item = "NULL"');
        $this->db->where('penjualan.id_pemilik',$uid);
        $this->db->where('penjualan.game_id',$game_id);
        $this->db->group_by('calendar.dt');
        // $this->db->order_by($sort,$order);        
        $query = $this->db->get();

        // $query = $this->db->query("SELECT d.id_item as id_item, i.nama_item as nama_item, sum(d.jumlah) as total, d.harga as hj, 
        //     s.harga as hb, (sum(d.jumlah)*d.harga) as dapat, (d.harga - s.harga)*(sum(d.jumlah)) as profit 
        //     FROM `detail_penjualan` d, `penjualan` p, `detail_suplier` s, `item_master` i 
        //     WHERE p.id_so = d.id_so AND d.id_suplier = s.id_suplier and d.id_item = s.id_item AND d.id_item = i.id_item AND p.id_petugas = '$uid' AND p.tanggal = '$tanggal' AND p.game_id = '$game_id' GROUP BY d.id_item ORDER BY $sort $order");
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                    $hasil[] = $row;
            }
            return $hasil;
        }
        else{
            return 0;
        }
    }

    public function trendBulanan($tanggal, $order, $sort){
        $game_id = $this->session->userdata('game_id');
        $uid = $this->session->userdata('id_retail');
        $this->db->reconnect();
        $query = $this->db->query("SELECT d.id_item as id_item, i.nama_item as nama_item, sum(d.jumlah) as total, d.harga as hj, s.harga as hb, (sum(d.jumlah)*d.harga) as dapat, (d.harga - s.harga)*(sum(d.jumlah)) as profit FROM `detail_penjualan` d, `penjualan` p, `detail_suplier` s, `item_master` i WHERE p.id_so = d.id_so AND d.id_suplier = s.id_suplier and d.id_item = s.id_item AND d.id_item = i.id_item AND p.id_petugas = '$uid' AND MONTH(p.tanggal) = MONTH('$tanggal') AND YEAR(p.tanggal) = YEAR('$tanggal') AND p.game_id = '$game_id' GROUP BY d.id_item ORDER BY $sort $order");
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                    $hasil[] = $row;
            }
            return $hasil;
        }
        else{
            return 0;
        }
    }

    public function list_dashboard_data($sort){
        $game_id = $this->session->userdata('game_id');
        $uid = $this->session->userdata('id_retail');
        $this->db->reconnect();
        $query = $this->db->query("SELECT d.id_item as id_item, i.nama_item as nama_item, sum(d.jumlah) as total, d.harga as hj, s.harga as hb, (sum(d.jumlah)*d.harga) as dapat, (d.harga - s.harga)*(sum(d.jumlah)) as profit FROM `detail_penjualan` d, `penjualan` p, `detail_suplier` s, `item_master` i WHERE p.id_so = d.id_so AND d.id_suplier = s.id_suplier and d.id_item = s.id_item AND d.id_item = i.id_item AND p.id_petugas = '$uid' AND p.game_id = '$game_id' GROUP BY d.id_item ORDER BY $sort DESC LIMIT 5");
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                    $hasil[] = $row;
            }
            return $hasil;
        }
        else{
            return 0;
        }
    }

    public function trendBarang($tanggal, $id_item, $order, $sort){
        $game_id = $this->session->userdata('game_id');
        $uid = $this->session->userdata('id_retail');
        $this->db->reconnect();
        $query = $this->db->query("SELECT p.tanggal as tanggal, sum(d.jumlah) as total, d.harga as hj, s.harga as hb, (sum(d.jumlah)*d.harga) as dapat, (d.harga - s.harga)*(sum(d.jumlah)) as profit FROM `detail_penjualan` d, `penjualan` p, `detail_suplier` s, `item_master` i WHERE p.id_so = d.id_so AND d.id_suplier = s.id_suplier and d.id_item = s.id_item AND d.id_item = '$id_item' AND p.id_petugas = '$uid' AND MONTH(p.tanggal) = MONTH('$tanggal') AND YEAR(p.tanggal) = YEAR('$tanggal') AND p.game_id = '$game_id' GROUP BY p.tanggal ORDER BY $sort $order");
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                    $hasil[] = $row;
            }
            return $hasil;
        }
        else{
            return 0;
        }
    }

    public function create_eoq($data){
        $this->db->reconnect();
        // $query=$this->db->query("CALL sp_input_petugas('$data[ktp]','$data[nama]','$data[email]','$data[passwd]')");
        $query = $this->db->query("SELECT `id`,`barang_id`,`id_retail` FROM `bi_eoq` WHERE `barang_id` = '$data[barang_id]' AND `id_retail` = '$data[id_retail]'");
        // print_r($query->num_rows());
        if($query->num_rows()==0){
            $ok = $this->db->insert('bi_eoq',$data);
            // echo "yeay";
            return $ok; 
        }
        else{
            $child = $query->result();
            foreach ($child as $item) 
            {
                $id = $item->id;
            }
            $this->db->where('id',$id);
            $ok = $this->db->update('bi_eoq',$data);
            return $ok;
            // echo "yaah";
            // return 0;
        }
        // $row=$query->row();
        // return $row->cek;
    }

    public function update_eoq($data){
        $this->db->reconnect();
        // $query=$this->db->query("CALL sp_input_petugas('$data[ktp]','$data[nama]','$data[email]','$data[passwd]')");
        $jumlah = 0;
        $hold_c = 1;
        $query = $this->db->query("SELECT `id`,`barang_id`,`id_retail` FROM `bi_eoq` WHERE `barang_id` = '$data[barang_id]' AND `id_retail` = '$data[id_retail]'");
        $update_data = $this->db->query("SELECT penjualan.tanggal, SUM(detail_penjualan.jumlah) as jual, detail_penjualan.id_so FROM penjualan, detail_penjualan WHERE penjualan.id_so = detail_penjualan.id_so AND detail_penjualan.id_item = '$data[barang_id]' AND penjualan.id_pemilik = '$data[id_retail]' GROUP BY penjualan.tanggal ORDER BY penjualan.tanggal DESC LIMIT 100 ");
        $hold = $this->db->query("SELECT (`asset_value`/`asset_duration`)/`asset_capacity` FROM `asset` WHERE `asset_name` = 'Gedung' AND `id_retail` = '$data[id_retail]' AND `is_delete`=0 AND `status` = 1 ");
        $data1 = $hold->result();
        if (is_null($data1)) {
            $hold_c = 1;
        }
        else{
            foreach ($data1 as $item) 
            {
                $hold_c = $item->hold;
            }
        }
        $jual = $update_data->result();
        foreach ($jual as $item) {
            $jumlah = $jumlah+(int)$item->jual;
        }
        $data_jual = $jumlah/$update_data->num_rows();
        $demand = $data_jual*365;
        $child = $query->result();
        foreach ($child as $item) 
        {
            $id = $item->id;
        }
        $data_eoq = array(
            'barang_id' => $data['barang_id'],
            'id_retail' => $data['id_retail'],
            'demand' => $data_jual,
            // 'cost' => $data['ongkir'],
            'cost' => 5000,
            'rent' => $hold_c,
            'eoq' => sqrt((2*$data_jual*5000)/$hold_c),
            );
        $this->db->where('id',$id);
        $ok = $this->db->update('bi_eoq',$data_eoq);
        return $ok;
        // echo "yaah";
        // return 0;
        // $row=$query->row();
        // return $row->cek;
    }

    public function chart_data(){
        $game_id = $this->session->userdata('game_id');
        $uid = $this->session->userdata('id_retail');
        $this->db->reconnect();
        $query = $this->db->query("SELECT month(p.tanggal) as month, year(p.tanggal) as tahun , (sum(d.jumlah)*d.harga) as dapat, (d.harga - s.harga)*(sum(d.jumlah)) as profit FROM `detail_penjualan` d, `penjualan` p, `detail_suplier` s, `item_master` i WHERE p.id_so = d.id_so AND d.id_suplier = s.id_suplier and d.id_item = s.id_item AND d.id_item = i.id_item AND p.id_petugas = '$uid' AND p.game_id = '$game_id' GROUP BY month(p.tanggal) ");
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                    $hasil[] = $row;
            }
            return $hasil;
        }
        else{
            return 0;
        }
    }

    public function roa($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query = $this->db->query("select (A.total-(B.expenses+D.total)) as net, C.total as totalc from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' and h.game_id='$game_id')A, (SELECT (sum(e.expense_jumlah*e.expense_value)) as expenses from expense e where e.id_pemilik='$uid' and e.game_id='$game_id')B, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '1%' and h.uid='$uid' and h.game_id='$game_id')C, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '5%' and h.uid='$uid' and h.game_id='$game_id')D");
            if ($query->num_rows() > 0)
            {
            foreach ($query->result() as $row)
            {
                    $hasil[] = $row;
            }
            return $hasil;
            }
            else{
                return 0;
            }
    }

    public function roi($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
        $query = $this->db->query("select (A.total-(B.expenses+D.total)) as net, C.total as totalc from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' and h.game_id='$game_id')A, (SELECT (sum(e.expense_jumlah*e.expense_value)) as expenses from expense e where e.id_pemilik='$uid' and e.game_id='$game_id')B, (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '2%' and h.uid='$uid' and h.game_id='$game_id')C, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '5%' and h.uid='$uid' and h.game_id='$game_id')D");
        if ($query->num_rows() > 0)
        {
        foreach ($query->result() as $row)
        {
                $hasil[] = $row;
        }
        return $hasil;
        }
        else{
            return 0;
        }
    }

    public function roe($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query = $this->db->query("select (A.total-(B.expenses+D.total)) as net, C.total as totalc from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' and l.game_id='$game_id')A, (SELECT (sum(e.expense_jumlah*e.expense_value)) as expenses from expense e where e.id_pemilik='$uid' and e.game_id='$game_id')B, (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '3%' and h.uid='$uid' and h.game_id='$game_id')C, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '5%' and h.uid='$uid' and h.game_id='$game_id')D");
            if ($query->num_rows() > 0)
            {
            foreach ($query->result() as $row)
            {
                    $hasil[] = $row;
            }
            return $hasil;
            }
            else{
                return 0;
            }
    }

    public function liability($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            // $query = $this->db->query("select (A.total-(B.expenses+D.total)) as net, C.total as totalc from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' and l.game_id='$game_id')A, (SELECT (sum(e.expense_jumlah*e.expense_value)) as expenses from expense e where e.id_pemilik='$uid' and e.game_id='$game_id')B, (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '3%' and h.uid='$uid' and h.game_id='$game_id')C, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '5%' and h.uid='$uid' and h.game_id='$game_id')D");
        $query = $this->db->query("SELECT sum(l.line_debit) as debit, sum(l.line_credit) as credit from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and h.uid='$uid' and l.game_id='$game_id' and (l.acc_id = 2100 or l.acc_id = 6012)");
            if ($query->num_rows() > 0)
            {
            foreach ($query->result() as $row)
            {
                    $hasil[] = $row;
            }
            return $hasil;
            }
            else{
                return 0;
            }
    }

    public function asset($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT (sum(l.line_debit)- sum(l.line_credit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '1%' and h.uid='$uid' and  h.game_id='$game_id'");
            if ($query2->num_rows() > 0)
            {
            foreach ($query2->result() as $row)
            {
                    $hasil2[] = $row;
            }
            return $hasil2;
            }
            else{
                return 0;
            }
    }

    public function hpp($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT (sum(l.line_debit)-sum(l.line_credit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '5%' and h.uid='$uid' and  h.game_id='$game_id'");
            if ($query2->num_rows() > 0)
            {
            return $query2->row();
            }
            else{
                return 0;
            }
    }

    public function hpp_bulan($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT (sum(l.line_debit)-sum(l.line_credit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '5%' and h.uid='$uid' and  h.game_id='$game_id' GROUP BY month(h.period_id) ORDER BY month(h.period_id) DESC limit 1");
            if ($query2->num_rows() > 0)
            {
            return $query2->row();
            }
            else{
                return 0;
            }
    }

    public function avg_hpp($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT (avg(l.line_debit-l.line_credit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id like '5%' and h.uid='$uid' and h.game_id='$game_id'");
            if ($query2->num_rows() > 0)
            {
            return $query2->row();
            }
            else{
                return 0;
            }
    }

    public function hpp_satu($uid,$id_item){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT s.harga as harga FROM detail_suplier s, config_product c where s.id_item=c.id_item and s.id_suplier= c.supplier and c.id_item = '$id_item' and c.id_pemilik = '$uid' and c.game_id = '$game_id'");
            if ($query2->num_rows() > 0)
            {
            return $query2->row();
            }
            else{
                return 0;
            }
    }

    public function fore_all($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT sum(day1) as total FROM `bi_forecast` where id_pemilik='$uid' and game_id='$game_id'");
            if ($query2->num_rows() > 0)
            {
            return $query2->row();
            }
            else{
                return 0;
            }
    }

        public function fore_satu($uid,$id_item){
        $this->db->reconnect();
        $hasil = 0; //iki sisan
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT day1 FROM `bi_forecast` where id_pemilik='$uid' and game_id='$game_id' and id_item='$id_item'");
            if ($query2->num_rows() > 0)
            {
                //mulai kene
                foreach ($query2->row() as $data) 
                {
		    //echo ' ';
		    //print_r($data);
		    //echo ' ';
                    $hasil = $data;
                }
            // return $query2->row(); //iki juga
            }
            //editan andi
            return $hasil;
    }

    public function expenses($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT (sum(e.expense_jumlah*e.expense_value)) as expenses from expense e where e.id_pemilik='$uid' and e.game_id='$game_id'");
            if ($query2->num_rows() > 0)
            {
            foreach ($query2->result() as $row)
            {
                    $hasil2 = $row;
            }
            return $hasil2;
            }
            else{
                return 0;
            }
    }

    public function revenue($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' and h.game_id='$game_id'");
            if ($query2->num_rows() > 0)
            {
            // foreach ($query2->result() as $row)
            // {
            //         $hasil2 = $row;
            // }
            return $query2->row();
            }
            else{
                return 0;
            }
    }

    public function revenue_bulan($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' and h.game_id='$game_id' GROUP BY month(h.period_id) ORDER BY month(h.period_id) DESC limit 1");
            if ($query2->num_rows() > 0)
            {
            // foreach ($query2->result() as $row)
            // {
            //         $hasil2 = $row;
            // }
            return $query2->row();
            }
            else{
                return 0;
            }
    }

    public function iklan_bulan($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT (sum(l.line_credit)- sum(l.line_debit)) as total from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=6000 and h.uid='$uid' and h.game_id='$game_id' GROUP BY month(h.period_id) ORDER BY month(h.period_id) DESC limit 1");
            if ($query2->num_rows() > 0)
            {
            // foreach ($query2->result() as $row)
            // {
            //         $hasil2 = $row;
            // }
            return $query2->row();
            }
            else{
                return 0;
            }
    }

public function profit($uid){
    	$this->db->reconnect();
    	// $uid = $this->session->userdata('id_retail');
    	// $game_id = $this->session->userdata('game_id');
        $game_id = 12;
        $this->load->model('mpetugas');
        $nama = $this->mpetugas->findNama($uid);
    	$dapat = $this->revenue($uid);
    	$keluar = $this->expenses($uid);
    	$hpp = $this->hpp($uid);
    	$profit = $dapat->total - ($keluar->expenses+$hpp->total);
        $data = array(
            'profit' => $profit,
            'revenue' => $dapat->total,
            'id_pemilik' => $nama,

            );
    	return $data;
    }
    public function marginal_cost($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT avg(s.harga) as hb, (sum(e.expense_jumlah*e.expense_value)) as expenses FROM `detail_penjualan` d, `penjualan` p, `detail_suplier` s, `item_master` i, `expense` e WHERE p.id_so = d.id_so AND d.id_suplier = s.id_suplier and d.id_item = s.id_item AND d.id_item = i.id_item AND p.id_petugas = '$uid' AND p.game_id = '$game_id' AND e.id_pemilik='$uid' AND e.game_id = ''");
            if ($query2->num_rows() > 0)
            {
            // foreach ($query2->result() as $row)
            // {
            //         $hasil2 = $row;
            // }
            return $query2->row();
            }
            else{
                return 0;
            }
    }

    public function real_mc($id_item){
        $this->db->reconnect();
        $uid = $this->session->userdata('id_retail');
        $game_id = $this->session->userdata('game_id');
        $exp = $this->expenses($uid);
        $fore_tot = $this->fore_all($uid);
        $hpp = $this->hpp_satu($uid,$id_item);
        $fore_one = $this->fore_satu($uid, $id_item);
        //editan andi
	if ($fore_one==0) {
            $fore_one= 1;
        }
	//if ($fore_one->day1==0) {
          //  $fore_one->day1 = 1;
        //}
        if ($fore_tot->total==0) {
            return 0;
        } else {
            $exp_hari = $exp->expenses/30;
            $exp_satu = $exp_hari/$fore_tot->total;
	    //editan andi
            $hpp_tot = $hpp->harga*$fore_one;
            $mc = ($exp_satu + $hpp_tot)/$fore_one;
            //editan nanda
            $mc = $exp_satu + $hpp->harga;
	    //$hpp_tot = $hpp->harga*$fore_one->day1;
            //$mc = ($exp_satu + $hpp_tot)/$fore_one->day1;
            return $mc;
        }
    }

    public function avg_real_mc(){
        $this->db->reconnect();
        $uid = $this->session->userdata('id_retail');
        $game_id = $this->session->userdata('game_id');
        $mc = 0;
        $i = 0;
        $item = $this->item();
        foreach ($item as $row) {
            $input = $this->real_mc($row->id_item);
            $mc =$mc+$input;
            $i++;
        }
        $avg_mc = $mc/$i;
        return $avg_mc;
    }

    public function searchHargaMin($id_item)
    {
    	$uid = $this->session->userdata('id_retail');
    	$game_id = $this->session->userdata('game_id');
        // $game_id = 17;
        $this->db->reconnect();
        $query2 = $this->db->query("SELECT harga, round(avg(total)) as q FROM (SELECT p.tanggal, d.harga, sum(d.jumlah) as total FROM detail_penjualan d, penjualan p where p.id_so=d.id_so and d.id_item='$id_item' and p.id_pemilik='$uid' and p.game_id='$game_id' group by p.tanggal, d.harga)A GROUP by harga ORDER BY `A`.`harga` ASC limit 1");
        //$this->db->join('penjualan','penjualan.id_so = detail_penjualan.id_so and penjualan.id_pemilik = detail_penjualan.id_pemilik and penjualan.game_id ='.$data['game_id'],'INNER);
        //$this->db->group_by('id_item');
        if ($query2->num_rows() > 0)
        {
        return $query2->row();
        }
        else{
            return 0;
        }
    }

    public function searchHargaMax($id_item)
    {
    	$uid = $this->session->userdata('id_retail');
    	$game_id = $this->session->userdata('game_id');
        // $game_id = 17;
        $this->db->reconnect();
        $query2 = $this->db->query("SELECT harga, round(avg(total)) as q FROM (SELECT p.tanggal, d.harga, sum(d.jumlah) as total FROM detail_penjualan d, penjualan p where p.id_so=d.id_so and d.id_item='$id_item'and p.id_pemilik='$uid' and p.game_id='$game_id' group by p.tanggal, d.harga)A GROUP by harga ORDER BY `A`.`harga` DESC limit 1");
        //$this->db->join('penjualan','penjualan.id_so = detail_penjualan.id_so and penjualan.id_pemilik = detail_penjualan.id_pemilik and penjualan.game_id ='.$data['game_id'],'INNER');
        //$this->db->group_by('id_item');
        if ($query2->num_rows() > 0)
        {
        return $query2->row();
        }
        else{
            return 0;
        }
    }

    public function ins_ped(){
        $this->db->reconnect();
        $uid = $this->session->userdata('id_retail');
        $game_id = $this->session->userdata('game_id');
        $item = $this->item();
        foreach ($item as $row) {
            $this->db->select();
            $this->db->from('bi_ped');
            $this->db->where('id_pemilik',$uid);
            $this->db->where('game_id',$game_id);
            $this->db->where('id_item',$row->id_item);
            $tes = $this->db->get();
            $mc = $this->real_mc($row->id_item);
            if ($tes->num_rows()==0) {
                $max = $this->searchHargaMax($row->id_item);
                $min = $this->searchHargaMin($row->id_item);
                $ped_val = (($max->q-$min->q)/($max->harga-$min->harga))*(($max->harga+$min->harga)/($max->q+$min->q));
                $op_val = $mc*($ped_val/($ped_val+1));
                // print_r(round($op_val));
                // echo '<br>';
                //echo 'INSERT INTO `bi_ped`(`id_item`, po, pi, qo, qi, ped, op, `id_pemilik`, `game_id`) VALUES ('.$item->id_item.','.$min->harga.', '.$max->harga.', '.$min->q.', '.$max->q.','.$ped_val.','.$op_val.','.$uid.','.$game_id.')<br>';
                $ok = $this->db->query("INSERT INTO `bi_ped`(`id_item`, po, pi, qo, qi, ped, op, `id_pemilik`, `game_id`) VALUES ('$row->id_item','$min->harga', '$max->harga', '$min->q', '$max->q','$ped_val','$op_val','$uid','$game_id')");
            }
        }
    }

    public function ped($uid, $data){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
        //$game_id = 12;
        $this->db->select();
        $this->db->from('bi_ped');
        $this->db->where('id_pemilik',$uid);
        $this->db->where('game_id',$game_id);
        $tes = $this->db->get();
        $mc = $this->real_mc($data['id_item']);
        if ($tes->num_rows()==0) {
            $d_item = $this->db->query("SELECT * FROM `item_master`");
            foreach ($d_item->result() as $item) {
            	$max = $this->searchHargaMax($item->id_item);
            	$min = $this->searchHargaMin($item->id_item);
                $ped_val = (($max->q-$min->q)/($max->harga-$min->harga))*(($max->harga+$min->harga)/($max->q+$min->q));
                $op_val = $mc*($ped_val/($ped_val+1));
                // print_r(round($op_val));
                // echo '<br>';
				//echo 'INSERT INTO `bi_ped`(`id_item`, po, pi, qo, qi, ped, op, `id_pemilik`, `game_id`) VALUES ('.$item->id_item.','.$min->harga.', '.$max->harga.', '.$min->q.', '.$max->q.','.$ped_val.','.$op_val.','.$uid.','.$game_id.')<br>';
                $ok = $this->db->query("INSERT INTO `bi_ped`(`id_item`, po, pi, qo, qi, ped, op, `id_pemilik`, `game_id`) VALUES ('$item->id_item','$min->harga', '$max->harga', '$min->q', '$max->q','$ped_val','$op_val','$uid','$game_id')");
            }
        }
	//echo 'row : '.$tes->num_rows().'<br>';

        $this->db->select();
        $this->db->from('bi_ped');
        $this->db->where('id_pemilik',$uid);
        $this->db->where('game_id',$game_id);
        $this->db->where('id_item',$data['id_item']);
        $data2 = $this->db->get();
        $ped = $data2->result();
	$pedl = $ped[0];
        //var_dump($ped);
        // $avg_hpp=$this->avg_hpp($uid);
        // $hpp = $this->hpp($uid);
        // $expenses = $this->expenses($uid);
        // $mc = $avg_hpp->total/($expenses->expenses + $hpp->total);
        if ($data['price']<$pedl->po) {
            //echo "hello";
            $ped_val = (($pedl->qi-$data['quantity'])/($pedl->pi-$data['price']))*(($pedl->pi+$data['price'])/($pedl->qi+$data['quantity']));
            $op_val = $mc*($ped_val/($ped_val+1));
	    //echo 'UPDATE `bi_ped` SET `po`='.$data['price'].',`qo`='.$data['quantity'].',`ped`='.$ped_val.',`op`='.$op_val.' WHERE `id_item`='.$data['id_item'].' AND `id_pemilik` = '.$uid.'  and game_id = '.$game_id.' <br>';
            $this->db->query("UPDATE `bi_ped` SET `po`='$data[price]',`qo`='$data[quantity]',`ped`='$ped_val',`op`='$op_val' WHERE `id_item`='$data[id_item]' AND `id_pemilik` = '$uid'  and game_id = '$game_id'");
        } elseif ($data['price']>$pedl->pi) {
            //echo "halo juga";
            $ped_val = (($data['quantity']-$pedl->qo)/($data['price']-$pedl->po))*(($data['price']+$pedl->po)/($data['quantity']+$pedl->qo));
            $op_val = $mc*($ped_val/($ped_val+1));
	    //echo 'UPDATE `bi_ped` SET `pi`='.$data['price'].',`qi`='.$data['quantity'].',`ped`='.$ped_val.',`op`='.$op_val.' WHERE `id_item`='.$data['id_item'].' AND `id_pemilik` = '.$uid.'  and game_id = '.$game_id.' <br>';
            $this->db->query("UPDATE `bi_ped` SET `pi`='$data[price]',`qi`='$data[quantity]',`ped`='$ped_val',`op`='$op_val' WHERE `id_item`='$data[id_item]' AND `id_pemilik` = '$uid'  and game_id = '$game_id'");
        } else {
            $ped_val = (($pedl->qi-$pedl->qo)/($pedl->pi-$pedl->po))*(($pedl->pi+$pedl->po)/($pedl->qi+$pedl->qo));
            $op_val = $mc*($ped_val/($ped_val+1));
            //echo 'UPDATE `bi_ped` SET `ped`='.$ped_val.',`op`='.$op_val.' WHERE `id_item`='.$data['id_item'].' AND `id_pemilik` = '.$uid.' and game_id = '.$game_id.' <br>';
            $this->db->query("UPDATE `bi_ped` SET `ped`='$ped_val',`op`='$op_val' WHERE `id_item`='$data[id_item]' AND `id_pemilik` = '$uid' and game_id = '$game_id'");  
            //echo $op_val;
        }
        // return true;
    }

    public function avg_ped($uid){
        $this->db->reconnect();
        $game_id = $this->session->userdata('game_id');
            $query2 = $this->db->query("SELECT (avg(ped)) as avg from bi_ped where id_pemilik='$uid' and game_id='$game_id'");
            if ($query2->num_rows() > 0)
            {
            return $query2->row();
            }
            else{
                return 0;
            }
    }
	
    public function update1($uid,$id_item){
        $this->db->reconnect();
        $data_rop = array('id_item' => $id_item, 'id_pemilik' => $uid);
        $ok = $this->create_rop($data_rop);
        $ok2 = $this->update_rop($data_rop);
        if ($ok && $ok2) {
            return 'success';
        } else {
            return 'fail';
        }
                
    }
    public function update12($uid,$id_item){
        $this->db->reconnect();
        $data_rop = array('barang_id' => $id_item, 'id_retail' => $uid);
        $ok = $this->create_eoq($data_rop);
        $ok2 = $this->update_eoq($data_rop);
        if ($ok && $ok2) {
            return 'success';
        } else {
            return 'fail';
        }
    }

}
?>
