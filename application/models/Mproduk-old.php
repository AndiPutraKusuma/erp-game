<?php
class MProduk extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

	 //add data
	 public function additem($data){
		$this->db->reconnect();
		// `_id_item` VARCHAR(100), `_nama_item` VARCHAR(200), `_harga` INT(50), `_satuan` VARCHAR(50), `_deskripsi` TEXT, `_link_photo` VARCHAR(200), `id_pemilik` VARCHAR(200)
		$query=$this->db->query("CALL sp_input_item('$data[id_item]','$data[nama_item]','$data[item_harga]','$data[satuan]','$data[deskripsi]','','$data[id_pemilik]')");
		$row=$query->row();
		return $row->cek;
	 }
	  public function addSatuan($data){
		 $this->db->reconnect();
		$query=$this->db->query("CALL sp_input_satuan('$data[nama]','$data[kelas]','$data[deskripsi]','$data[id_pemilik]')");

	 }
	 public function addTipeItem($data){
		 $this->db->reconnect();
		$query=$this->db->query("CALL sp_input_tipe('$data[nama]','$data[deskripsi]')");

	 }
	//list data
	public function list_produk($id){
		$this->db->reconnect();
		$this->db->select("id_suplier,nama_item,item_master.`id_item`, subKategori_name as nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi");
		$this->db->from('penerimaan_barang');
		$this->db->join('gudang','gudang.`id_rec`=penerimaan_barang.`id_rec`','INNER');
		$this->db->join('item_master','item_master.`id_item`=gudang.`id_item`','INNER');
		$this->db->join('satuan as sat','sat.id_satuan=item_master.`satuan`','INNER');
		//$this->db->join('tipe_item','tipe_item.`id_tipe_item`=item_master.`tipe`','INNER');
		$this->db->join('sub_kategori','sub_kategori.subKategori_kode=item_master.`tipe`','INNER');
		//$this->db->join('tipe_item','tipe_item.`id_tipe_item`=item_master.`tipe`','INNER');
		if($this->session->userdata('game'))
		{
			$this->db->where('gudang.`id_pemilik`',$id);
			$this->db->where('item_master.`id_pemilik` = 0');
		}
		else
		{
			$this->db->where('item_master.`id_pemilik`',$id);
		}
		$this->db->group_by('item_master.id_item');
		$this->db->order_by('nama_item','ASC');
		$query = $this->db->get();
	// 		$query = $this->db->query("SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi
	// FROM penerimaan_barang
	// INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	// RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	// INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	// INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	// WHERE gudang.`id_pemilik`='$id'
	// GROUP BY item_master.`id_item` ORDER BY nama_item ASC");
			if ($query->num_rows() > 0){
				return $query->result();
			}else{
				return 0;
			}
	}

	public function list_produk_pagination($id, $awal, $hitung){
		$this->db->reconnect();
		$this->db->select("id_suplier,nama_item,item_master.`id_item`,subKategori_name as nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi");
		$this->db->from('penerimaan_barang');
		$this->db->join('gudang','gudang.`id_rec`=penerimaan_barang.`id_rec` and gudang.jumlah >0 ','INNER');
		$this->db->join('item_master','item_master.`id_item`=gudang.`id_item`','INNER');
		$this->db->join('satuan as sat','sat.id_satuan=item_master.`satuan`','INNER');
		$this->db->join('sub_kategori','sub_kategori.subKategori_kode=item_master.`tipe`','INNER');
		// $this->db->join('penjualan','')
		//$this->db->join('tipe_item','tipe_item.`id_tipe_item`=item_master.`tipe`','INNER');
		if($this->session->userdata('game'))
		{
			$this->db->where('gudang.`id_pemilik`',$id);
			$this->db->where('item_master.`id_pemilik` = 0');
		}
		else
		{
			$this->db->where('item_master.`id_pemilik`',$id);
		}
		$this->db->group_by('item_master.id_item');
		$this->db->order_by('nama_item','ASC');
		$this->db->limit($hitung, $awal);
		$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}else{
				return 0;
			}
	}

	public function pageList_produk($start,$limit,$id_pemilik){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_pageList_produk($start,$limit,$id_pemilik)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function list_cariProduk($word){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_cariProduk('$word')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function list_produk_perSuplier($id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_listProduk_perSuplier('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}

	}

	public function total_produk_perSuplier($id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_listProduk_perSuplier('$id')");
			if ($query->num_rows() > 0)
			{
				return $query->num_rows();
			}
			else{
				return 0;
			}
	}

	public function list_produk_perSuplier_pagination($id, $offset, $rows){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_listProduk_perSuplier_pagination('$id','$offset',$rows)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}

	public function rincianProduk($id){
		$this->db->reconnect();
		$idPemilik=$this->session->userdata('id_retail');
			$query = $this->db->query("SELECT gudang.id_pemilik,nama_suplier,detail_suplier.`id_suplier`,nama_item,detail_suplier.`id_item`,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,item_master.item_harga as hargaSatuan,item_master.deskripsi,gudang.barcode_barang as barcode
	FROM penerimaan_barang
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN sub_kategori ON sub_kategori.subKategori_kode=item_master.`tipe` and sub_kategori.id_pemilik = item_master.id_pemilik
	INNER JOIN suplier ON suplier.`id_suplier`=detail_suplier.`id_suplier`
	WHERE detail_suplier.`id_item`='$id' AND gudang.id_pemilik='$idPemilik' GROUP BY gudang.barcode_barang,hargaSatuan ORDER BY nama_item ASC");
	// 	$query = $this->db->query("SELECT gudang.id_pemilik,nama_item,item_master.`id_item`,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,item_master.deskripsi , gudang.`barcode_barang` AS barcode
	// FROM penerimaan_barang
	// INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	// INNER JOIN item_master ON item_master.`id_item`=item_master.`id_item`
	// INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	// INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	// WHERE item_master.`id_item`='$id' AND gudang.id_pemilik='$idPemilik' GROUP BY gudang.`barcode_barang`ORDER BY nama_item ASC");
		// if($this->session->userdata('game'))
		// {
		// 	$this->db->select('nama_item,item_master.`id_item`,nama_satuan,SUM(gudang.`jumlah`) AS jumlah, item_price as hargaSatuan,item_master.deskripsi , gudang.`barcode_barang` AS barcode,id_supplier,nama_suplier,gudang.id_pemilik');
		// 	$this->db->join('player_detail',"game_player = '".$idPemilik."' and id_item = '".$id."'",'INNER');
		// }
		// else
		// {
		// 	$this->db->select('nama_item,item_master.`id_item`,nama_satuan,SUM(gudang.`jumlah`) AS jumlah, item_master.item_harga as hargaSatuan,item_master.deskripsi , gudang.`barcode_barang` AS barcode , id_supplier,nama_suplier,gudang.id_pemilik');	
		// }
		// $this->db->from('gudang');
		// $this->db->join('item_master','gudang.`id_item`=item_master.`id_item`','INNER');
		// $this->db->join('suplier','suplier.`id_suplier`=gudang.`id_supplier`','INNER');
		// // $this->db->join('gudang','item_master.`id_item`=item_master.`id_item`','INNER');
		// $this->db->join('satuan as sat','sat.id_satuan=item_master.`satuan`','INNER');
		// $this->db->join('tipe_item','tipe_item.`id_tipe_item`=item_master.`tipe`','INNER');
		// $this->db->where('gudang.id_item',$id);
		// $this->db->where('gudang.id_pemilik',$idPemilik);
		// $this->db->group_by('gudang.barcode_barang');
		// $this->db->order_by('nama_item','ASC');
		// $query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
						// array_push(
	     //                            $hasil,
	     //                            array(
	     //                                'nama_item'=>$row->nama_item,
	     //                                'id_item'=>$row->id_item,
	     //                                'nama_satuan'=>$row->nama_satuan,
	     //                                'jumlah'=>$row->jumlah,
	     //                                'hargaSatuan'=>$row->hargaSatuan,
	     //                                'deskripsi'=>$row->deskripsi,
	     //                                'barcode'=>$row->barcode,
	     //                                'id_supplier'=>$row->id_supplier,
	     //                                'nama_suplier'=>$row->nama_suplier,

	     //                            )
	     //                    );
					$hasil[] = $row;
				}
				return $hasil;
			}
			else{
				return 0;
			}

	}

	public function hargaProduk($id, $price)
	{
		$this->db->reconnect();
		$this->db->select('*');
		$this->db->from('gudang');
		$this->db->where('id_item', $id);
		$query=$this->db->get();
		//return print_r($query);
		if($query)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function rincianProdukPesanan($id){
		$this->db->reconnect();
		$idPemilik=$this->session->userdata('id_retail');
			$query = $this->db->query("SELECT nama_suplier,detail_suplier.`id_suplier`,detail_suplier.harga,nama_item,detail_suplier.`id_item`,nama_satuan,'jumlah'
	FROM  detail_suplier
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN suplier ON suplier.`id_suplier`=detail_suplier.`id_suplier`
	WHERE detail_suplier.`id_item`='$id' ORDER BY nama_item ASC");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}

	}
	public function list_item($id,$idgame){
		$this->db->reconnect();
	// 		$query = $this->db->query("select * from item_master
	// inner join satuan sat on sat.id_satuan=item_master.`satuan`
	// inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe` where item_master.`id_pemilik`=".$id." order by nama_item asc;");
			$query = $this->db->query("CALL sp_list_item($id,$idgame)");
			if ($query->num_rows() > 0)
			 {
			 foreach ($query->result() as $row)
			 {
			 		$hasil[] = $row;
			 }
			 return $hasil;
			 }
			 else{
			 	return 0;
			 }
		//	return $query->num_rows();
	}
	public function list_satuan($id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_list_satuan($id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function list_tipeItem($idPe){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_list_tipeItem2($idPe)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	//view data
	public function viewItem($id){
		$this->db->reconnect();
		$id_pemilik = $this->session->userdata('id_retail');
			$query = $this->db->query("CALL sp_view_perItem('$id','$id_pemilik')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	//update
	public function updateItem($data){
		 $this->db->reconnect();
		 $this->db->where('id_item',$data['id_item']);
		 $query = $this->db->update('item_master',$data);
		// $query=$this->db->query("CALL sp_updateItem('$data[idItem]','$data[nama]','$data[tipe]','$data[satuan]','$data[deskripsi]')");
		if($query)
		{
			return true;
		}
		else
		{
			return false;
		}

	 }
	 public function updateSatuan($data){
		 $this->db->reconnect();
		$query=$this->db->query("CALL sp_updateSatuan('$data[id]','$data[nama]','$data[kelas]','$data[deskripsi]')");
		$row=$query->row();
		return $row->cek;

	 }
	 public function updateTipeItem($data){
		 $this->db->reconnect();
		$query=$this->db->query("CALL sp_updateTipe('$data[id]','$data[nama]','$data[deskripsi]')");
		$row=$query->row();
		return $row->cek;

	 }
	//delete item
	public function delete($id){
		$this->db->reconnect();
		$query = $this->db->query("CALL sp_delete_item('$id')");
		$row=$query->row();
		return $row->cek;

	}
	public function deleteSatuan($id){
		$this->db->reconnect();
		$query = $this->db->query("CALL sp_delete_satuan('$id')");
		$row=$query->row();
		return $row->cek;

	}
	public function deleteTipe($id){
		$this->db->reconnect();
		$query = $this->db->query("CALL sp_delete_tipe('$id')");
		$row=$query->row();
		return $row->cek;

	}
	public function cekItem($id){
		$this->db->reconnect();
		$query = $this->db->query("CALL sp_delete_tipe('$id')");
		$row=$query->row();
		return $row->cek;

	}
	 public function countProduk($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungProduk($id)");

				$row=$query->row();
				return $row->jumlah;


	}

	public function makeId($id_pemilik,$tipe)
	{
		$this->db->reconnect();
		// $query = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_item),4),4,'0') AS maxi FROM item_master WHERE id_pemilik = '$id_pemilik' ");
		$query = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_item),4),4,'0') AS maxi FROM item_master WHERE id_pemilik = '$id_pemilik' ");

        $query2 = $query->result();
        $tes = $query2[0]->maxi;
        $item_id = '0';
        if(!isset($tes))
        {
            $item_id =  '0001';
        }
        else
        {
            // $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_item),4)+1,4,'0') AS sem FROM item_master WHERE id_pemilik = '$id_pemilik' ");
            $temp = $this->db->query("SELECT LPAD(SUBSTRING(MAX(id_item),5)+1,4,'0') AS sem FROM item_master WHERE id_pemilik = '$id_pemilik' ");

            $temp2 = $temp->result();
            $item_id = $temp2[0]->sem;
        }
        $id = $tipe.$item_id.$idPemilik;
        // $this->db->select('subGroup_id , itemGroup_id');
        // $this->db->where('id = '.$tipe);
        // $read = $this->db->get('item_subgroup');
        // foreach ($read->result() as $data) 
        // {
        // 	$sub = $data->subGroup_id;
        // 	$group = $data->itemGroup_id;
        // 	$id = $group.''.$sub.''.$item_id;
        // }
        return $id;
	}

	public function findItemPurchasing($data)
	{
		$this->db->reconnect();
		// $where = "id_item = '".$data['kodebarang']."' OR lower(nama_item) Like '%".$data['name']."%'";
		$this->db->select('item_master.id_item as kodeBarang , nama_item , harga');
		$this->db->where('item_master.id_item',$data[1]);
		$this->db->where('detail_suplier.id_suplier',$data[0]);
		$this->db->join('detail_suplier','item_master.id_item = detail_suplier.id_item','INNER');
		$read = $this->db->get('item_master');
		if ($read->num_rows() > 0)
		{
			foreach ($read->result() as $data2) 
			{
				$result[] = $data2;
			}
		}
		return $result;
	}


	public function findItem($kodebarang)
	{
		$this->db->reconnect();
		// $where = "id_item = '".$data['kodebarang']."' OR lower(nama_item) Like '%".$data['name']."%'";
		$this->db->select('id_item as kodeBarang , nama_item , item_harga');
		$this->db->where('id_item',$kodebarang);
		$read = $this->db->get('item_master');
		if ($read->num_rows() > 0)
		{
			foreach ($read->result() as $data2) 
			{
				$result[] = $data2;
			}
		}
		return $result;
	}

	public function listGroup($id)
	{
		$this->db->reconnect();
		// $read = $this->db->query('SELECT itemGroup_id as id , itemGroup_name as name FROM item_group where id_pemilik ='.$id);
		$read = $this->db->query('SELECT subKategori_kode as id , subKategori_name as name FROM sub_kategori where is_delete = 0 and  LENGTH(subKategori_kode) = 2  and id_pemilik ='.$id);
		foreach ($read->result() as $data) 
		{
			$result[] = $data;
		}
		return $result;
	}
	public function listSub($kategori)
	{
		$this->db->reconnect();
		$idPemilik = $this->session->userdata('id_retail');
		// $this->db->select('id , subGroup_name as name');
		// $this->db->where('itemGroup_id',$id);
		// $this->db->where('id_pemilik',$this->session->userdata('id_retail'));
		// $read = $this->db->get('item_subgroup');
		$this->db->select('subKategori_kode as id , subKategori_name as name');
		$this->db->where('LENGTH(subKategori_kode) = 3');
		$this->db->where('substr(subKategori_kode, 1,2) =',$kategori);
		$this->db->where('id_pemilik',$idPemilik);
		$this->db->where('is_delete',0);
		$read2 = $this->db->get('sub_kategori');
		if($read2->num_rows() > 0)
		{
			foreach ($read2->result() as $data) 
			{
				$result[$data->id] = $data->name;
			}	
		}
		else
		{
			$result = array();
		}
		return $result;
	}
	public function countstock($id_pemilik)
	{
		$this->db->reconnect();
		$this->db->select('COUNT(gudang.id_item) AS jumlah');
		$this->db->where('item_master.id_pemilik',$id_pemilik);
		$this->db->from('item_master');
		$this->db->join('gudang','gudang.id_item = item_master.id_item','INNER');
		$this->db->group_by('item_master.id_item');
		$read = $this->db->get();
		
		return $read->num_rows();
	}
	public function stockItem($page,$per_page,$id_pemilik)
	{
		$this->db->reconnect();
		$this->db->select('item_master.id_item , nama_item,sum(jumlah) as stock');
		$this->db->where('item_master.id_pemilik',$id_pemilik);
		$this->db->from('item_master');
		$this->db->join('gudang','gudang.id_item = item_master.id_item','INNER');
		$this->db->group_by('item_master.id_item');
		$this->db->limit($per_page, $page);
		$read = $this->db->get();
		foreach ($read->result() as $data) 
		{
			$hasil[] = $data;
		}
		if($hasil)
		{
			return $hasil;
		}
		else
		{
			return array();
		}
	}
}


?>
