<?php 
// ini_set('max_execution_time', 0); 
// ini_set('memory_limit','4096M');
// ini_set('memory_limit','2048M');
set_time_limit(900); // 15 minutes

class M_game extends CI_Model {

    public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
    public function getStock($id_barang,$pemilik)
    {
        // print_r($id_barang.' '.$pemilik);
        $stock = array();
        $this->db->reconnect();
        $this->db->select('sum(jumlah) as stock');
        $this->db->where('id_item',$id_barang);
        $this->db->where('id_pemilik',$pemilik);
        $this->db->where('game_id',$this->session->userdata('game_id'));
        $read = $this->db->get('gudang');
        if($read->num_rows() > 0)
        {
            foreach ($read->result() as $data) 
            {
                // array_push(
                //         $stock,
                //         array(
                //             'stock'=>$data->stock,
                //         )
                // ); 
                $stock = $data->stock;

            }
        }
        else
        {
            // array_push(
            //             $stock,
            //             array(
            //                 'stock'=>0,
            //                 // 'supplier_id'=>$data->supplier_id,
            //             )
            //     ); 
            $stock = 0;
        }
        // print_r($stock);
        return $stock;
    }  
    // public function getInfo($max)
    // {
    //     $game_id = $this->session->userdata('company');
    //     $idPet=$this->session->userdata('id_retail');
    //     $homina = array();
    //     $this->db->reconnect();
    //     $this->db->select('id_petugas');
    //     $this->db->where('game_id',$game_id);
    //     $this->db->where('privilege = 17');
    //     $read = $this->db->get('petugas');
    //     foreach ($read->result() as $data) 
    //     {
    //         $users[] = $data->id_petugas;
    //     }
    //     // print_r($users);
    //     // $this->db->select('id_item , item_harga , item_marketing ,id_pemilik');
    //     // $this->db->distinct('id_item');
    //     $temp = array();
    //     $this->db->select('tipe');
    //     $this->db->where_in('id_pemilik',$users);
    //     $readItem = $this->db->get('item_master');
    //     foreach ($readItem->result() as $datacek) 
    //     {
    //         if(!in_array($datacek->tipe, $temp))
    //         {
    //             $temp[] = $datacek->tipe; 
    //         }
    //     }
    //     foreach ($temp as $data2) 
    //     {   
    //         $this->db->select('sum(item_harga) as maxHarga , sum(item_marketing) as maxMarket');
    //         $this->db->where('tipe ',$data2);
    //         $this->db->where_in('id_pemilik',$users);
    //         $total = $this->db->get('item_master');
    //         foreach ($total->result() as $data) 
    //         {
    //             $totalHarga = $data->maxHarga;
    //             $totalMarket = $data->maxMarket;
    //         }
    //         $this->db->select('id_item, item_harga , item_marketing ,id_pemilik');
    //         $this->db->where('tipe ',$data2);
    //         $this->db->where_in('id_pemilik',$users);
    //         // $this->db->group_by('tipe');
    //         $readUser = $this->db->get('item_master');
    //         foreach ($readUser->result() as $data3) 
    //         {
    //             $id_so = uniqid("SO");
    //             $serapHarga = ($totalHarga - $data3->item_harga ) / $totalHarga;
    //             // $serapHarga = $data3->item_harga / $totalHarga;
    //             $serapMarket = $data3->item_marketing / $totalMarket;
    //             $serapan = floor(((0.7 * $serapHarga) + (0.3 * $serapMarket))*$max);
    //             $total = $data3->item_harga * $serapan;
    //             if($serapan > 0)
    //             {
    //                 $cekstock = $this->getStock($data3->id_item,$data3->id_pemilik);
    //                 $this->db->trans_start();
    //                 // var_dump($cekstock);
    //                 if($cekstock[0]['stock'] > 0)
    //                 {
    //                     echo $cekstock[0]['stock'].' ';
    //                     foreach ($cekstock as $param) 
    //                     {
    //                         if($serapan <= $param['stock'])
    //                         {
    //                             $dataDetail = array(
    //                                 'id_so' => $id_so,
    //                                 'id_item' => $data3->id_item,
    //                                 'jumlah' => $serapan,
    //                                 'harga' => $data3->item_harga,
    //                                 'id_suplier' =>$param['supplier_id'],
    //                                 'id_pemilik' =>$data3->id_pemilik,
    //                             );
    //                             $stock = $param['stock'] - $serapan;       
    //                         }
    //                         else
    //                         {
    //                             $dataDetail = array(
    //                                 'id_so' => $id_so,
    //                                 'id_item' => $data3->id_item,
    //                                 'jumlah' => $param['stock'],
    //                                 'harga' => $data3->item_harga,
    //                                 'id_suplier' =>$param['supplier_id'],
    //                                 'id_pemilik' =>$data3->id_pemilik,
    //                             );
    //                             $serapan = $serapan - $param['stock'];
    //                             $stock = 0;

    //                         }
    //                         $ok2 = $this->db->insert('detail_penjualan',$dataDetail);
    //                         if($ok2)
    //                         {
    //                             $update = array(
    //                             'stock' => $stock,
    //                             'last_mod_user' => $data3->id_pemilik,
    //                             'last_mod_time' => $this->session->userdata('tanggal'),
    //                             );
    //                             $this->db->where('id_barang',$data3->id_item);
    //                             $this->db->where('supplier_id',$param['supplier_id']);
    //                             $this->db->where('id_retail',$data3->id_pemilik);
    //                             $this->db->update('item_stock',$update);
    //                         }
    //                         if($total == 0)
    //                         {
    //                             break;
    //                         }
    //                     }
    //                     if($ok2)
    //                     {
    //                         $hasil = array(
    //                                     'id_so' => $id_so, 
    //                                     'id_petugas' => $data3->id_pemilik,
    //                                     'id_customer' => 1,
    //                                     'tanggal' =>  $this->session->userdata('tanggal'),
    //                                     'total' => $total ,
    //                                     'kurir' => 1,
    //                                     'status' => 0,
    //                                     'tanggal_keluar' => $this->session->userdata('tanggal'),
    //                                     'id_pemilik' => $data3->id_pemilik,
    //                                 );
                           
    //                         $ok = $this->db->insert('penjualan',$hasil);
    //                         if ($ok)
    //                         {
    //                             $this->db->trans_commit();
    //                         }
    //                         else
    //                         {
    //                             $this->db->trans_rollback();
    //                         }
    //                     }
    //                     else
    //                     {
    //                         $this->db->trans_rollback();
    //                     }
    //                 }
    //                 // else
    //                 // {
    //                 //     echo 'stock kosong';
    //                 // }
    //             }
    //             else
    //             {
    //                 echo 'total 0';
    //             }
    //         }
    //     }
    //             // if($ok)
    //             // {
    //             //     $dataDetail = array(
    //             //             'id_so' => $id_so,
    //             //             'id_item' => $data3->id_item,
    //             //             'jumlah' => $serapan,
    //             //             'harga' => $data3->item_harga,
    //             //             'id_suplier' =>1,
    //             //             'id_pemilik' =>$data3->id_pemilik,
    //             //         );
    //             //     $ok2 = $this->db->insert('detail_penjualan',$dataDetail);
    //             //     if($ok2)
    //             //     {
    //             //         $this->db->trans_commit();
    //             //     }
    //             //     else
    //             //     {
    //             //         $this->db->trans_rollback();
    //             //         break;
    //             //     }
    //             // }
    //             // else
    //             // {
    //             //     $this->db->trans_rollback();
    //             //     break;
    //             // }
    //     //     }
    //     // }
    //     return true;
    // }
    public function MarketStart()
    {
        // $id_sos = array();
        $game_id = $this->session->userdata('game_id');
        $idPet=$this->session->userdata('id_retail');
        $homina = array();
        $this->db->reconnect();
        $this->db->select('game_player');
        $this->db->where('game_id',$game_id);
        // $this->db->where('privilege = 17');
        $read = $this->db->get('game_detail');
        $jumlah_player = 0;
        foreach ($read->result() as $data) 
        {
            $users[] = $data->game_player;
            $jumlah_player++;
        }
        $temp = array();
        $this->db->select('id_item');
        // $this->db->where_in('id_pemilik',$users);
        $readItem = $this->db->get('item_master');
        foreach ($readItem->result() as $datacek) 
        {
                $temp[] = $datacek->id_item;
        }
        foreach ($temp as $data2) 
        {  // echo 'item : '.$data2.'<br>';
            $this->db->select('sum(item_price) as maxHarga , sum(item_marketing) as maxMarket');
            $this->db->where('id_item ',$data2);
            $this->db->where('game_id',$game_id);
            $this->db->where_in('game_player',$users);
            $readread = $this->db->get('player_detail');
            foreach ($readread->result() as $data) 
            {
                $totalHarga = $data->maxHarga;
                $totalMarket = $data->maxMarket;
            }
           // echo 'totalHarga : '.$totalHarga. ' totalMarket : '.$totalMarket.'<br>';
            $this->db->select('max_serapan');
            $this->db->where('id_item',$data2);
            $read = $this->db->get('item_master');
            foreach ($read->result() as $data) 
            {
               $tmp = $data->max_serapan *$jumlah_player;
               $max = floor(rand(-20,20)*$tmp/100) + $tmp;

            }
            // buat normalisasi
            $this->db->select('id_item, item_price , item_marketing ,game_player as id_pemilik');
            $this->db->where('id_item ',$data2);
            $this->db->where_in('game_player',$users);            
            $readUser2 = $this->db->get('player_detail');
            $normalisasi = 0;
            foreach ($readUser2->result() as $normal) 
            {
                $serapHarga = ($totalHarga - $normal->item_price ) / $totalHarga;
                // $serapHarga = $data3->item_harga / $totalHarga;
                $serapMarket = $normal->item_marketing / $totalMarket;
                $normalisasi = $normalisasi + ((0.7 * $serapHarga) + (0.3 * $serapMarket));
            }
            // echo $max.' '.$data2.'<br>';
            $this->db->select('id_item, item_price , item_marketing ,game_player as id_pemilik');
            $this->db->where('id_item ',$data2);
            $this->db->where_in('game_player',$users);
            // $this->db->group_by('tipe');
            $readUser = $this->db->get('player_detail');
            $total =0;
            foreach ($readUser->result() as $data3) 
            {
                //ambil max serapan
                //
                $id_so = uniqid("SO");
                // $id_sos[] = $id_so;
                $serapHarga = ($totalHarga - $data3->item_price ) / $totalHarga;
                // $serapHarga = $data3->item_harga / $totalHarga;
                $serapMarket = $data3->item_marketing / $totalMarket;
                $tes = ((0.7 * $serapHarga) + (0.3 * $serapMarket))/$normalisasi;
                // $serapan = floor(((0.7 * $serapHarga) + (0.3 * $serapMarket))*$max);
                $serapan = floor($tes*$max);

                // echo $data3->id_pemilik.' '.$serapMarket.' '.$serapHarga.' '.' '.((0.7 * $serapHarga) + (0.3 * $serapMarket)).' '.$serapan.' '.$max.' '.$tmp.'<br>';
                // echo $data3->id_pemilik.' serapan :'.$serapan.' max :'.$max.'<br>';
                //echo $data3->id_pemilik.' serapHarga :'.$serapHarga.' serapMarket :'.$serapMarket.' hasil : '.$tes.' serapan :'.$tes*$max.' max : '.$max .'<br>';

                $total = $total + ($data3->item_price * $serapan);
                if($serapan > 0)
                {
                    $cekstock = $this->getStock($data3->id_item,$data3->id_pemilik);
                    // $this->db->trans_start();
                    // var_dump($cekstock);
                    //echo 'stock '.$data3->id_pemilik.': '.$cekstock.'<br>';
                    if($cekstock > 0)
                    {
                        //foreach ($cekstock as $param) 
                        //{
                        $datagudang = array();
                        $this->db->select('barcode_barang , jumlah,id_supplier,id_pemilik');
                        $this->db->where('id_pemilik ',$data3->id_pemilik);
                        $this->db->where('jumlah > 0');
                        $this->db->where('id_item',$data3->id_item);
                        $read2 = $this->db->get('gudang');
                        {
                            foreach ($read2->result() as $data2) 
                            {
                                $datagudang[] = $data2;
                            }
                        }
                        // print_r($datagudang);
                        //
                        foreach ($datagudang as $datadata) 
                        {
                            if($serapan <= $datadata->jumlah)
                            {
                                $dataDetail = array(
                                    'id_so' => $id_so,
                                    'id_item' => $data3->id_item,
                                    'barcode_barang' => $datadata->barcode_barang,
                                    'jumlah' => $serapan,
                                    'harga' => $data3->item_price,
                                    'id_suplier' =>$datadata->id_supplier,
                                    'id_pemilik' =>$data3->id_pemilik,
                                    'game_id'=> $game_id,
                                );
                                $stock = $datadata->jumlah - $serapan;
                                $serapan = 0;       
                            }
                            else
                            {
                                $dataDetail = array(
                                    'id_so' => $id_so,
                                    'id_item' => $data3->id_item,
                                    'barcode_barang' => $datadata->barcode_barang,
                                    'jumlah' => $datadata->jumlah,
                                    'harga' => $data3->item_price,
                                    'id_suplier' =>$datadata->id_supplier,                                        
                                    'id_pemilik' =>$data3->id_pemilik,
                                    'game_id'=> $game_id,
                                );
                                $serapan = $serapan - $datadata->jumlah;
                                $stock = 0;
                            }
                            //echo $datadata->barcode_barang.' '.$datadata->jumlah.' '.$stock.'<br>';
                            $ok2 = $this->db->insert('detail_penjualan',$dataDetail);
                            if($ok2)
                            {
                                //echo 'ok2'.$datadata->barcode_barang.' '.$stock.' '.$serapan.'<br>';
                                $update = array(
                                'jumlah' => $stock,
                                // 'last_mod_user' => $data3->id_pemilik,
                                // 'last_mod_time' => $this->session->userdata('tanggal'),
                                );
                                // $this->db->where('id_barang',$data3->id_item);
                                // $this->db->where('supplier_id',$param['supplier_id']);
                                // $this->db->update('item_stock',$update);
                                $this->db->where('barcode_barang',$datadata->barcode_barang);
                                $this->db->where('id_pemilik',$data3->id_pemilik);
                                $this->db->where('game_id',$game_id);
                                $this->db->update('gudang',$update);
                            }
                            if ($serapan == 0)
                            {
                                break;
                            }
                        }
                        //
                        
                        if($ok2)
                        {
                            $hasil = array(
                                        'id_so' => $id_so, 
                                        'id_petugas' => $data3->id_pemilik,
                                        'id_customer' => 1,
                                        'tanggal' =>  $this->session->userdata('tanggal'),
                                        'total' => $total ,
                                        'kurir' => 1,
                                        'status' => 0,
                                        'tanggal_keluar' => $this->session->userdata('tanggal'),
                                        'id_pemilik' => $data3->id_pemilik,
                                    );
                           
                            $ok = $this->db->insert('penjualan',$hasil);
                            // gl
                                $this->load->model('mgudang');
                                    $dataupdate = array(
                                        'idTransaksi' => $id_so,
                                        'status' => 4
                                    );
                                    $ok5 = $this->mpenjualan->updateStatus($dataupdate);

                                $detail = $this->db->query("SELECT DISTINCT c.harga*c.jumlah AS jumlahjual , a.hargaSatuan*c.jumlah AS jumlahbeli , a. barcode_barang
                                        FROM gudang a 
                                        INNER JOIN detail_penjualan AS c ON c.`barcode_barang` = a.`barcode_barang`
                                        where a.id_pemilik='$data3->id_pemilik' and c.id_so='$id_so'");
                                    $jual = $detail->row();
                                $query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$hasil[tanggal]',CONCAT('Pembayaran Invoice ','$id_so'),'$data3->id_pemilik')");
                                $this->load->model('mgl');
                                $schema = $this->mgl->schema_line(2);
                                $searchArray = array("HB", "HJ", "0");
                                $replaceArray = array($jual->jumlahbeli,$jual->jumlahjual,0);
                                $haha = $this->mpenjualan->cek2();
                                $idjurnalH = $haha->apa;
                                // echo 'id jurnal : '.$idjurnalH.'<br>';
                                foreach ($schema as $line) 
                                    {
                                        $data_line = array(
                                            'journal_id' => $idjurnalH,
                                            'acc_id' => $line->acc_id,
                                            'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
                                            'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
                                            'uid' => $data3->id_pemilik,
                                        );
                                        $ok2 = $this->db->insert('gl_journal_l',$data_line);
                                    }   
                                //if($query2)
                                //{
                                    // $this->load->model('mpenjualan');
                                    // $this->load->model('mgudang');
                                    // $data2 = $this->mgudang->rincianViewSO($id_so);
                                    // $dataSO = $data2[0];
                                    // $datagudang = array(
                                    //         'idSO' => $id_so,
                                    //         'idTransaksi' => uniqid("ISSUE"),
                                    //         'total' => $dataSO->total,
                                    //         'tgl' => $dataSO->tanggal,
                                    //         'kurir' => 1,
                                    //         'idCustomer' => $dataSO->id_customer,
                                    //         'email' => $this->session->userdata('username'),
                                    //         'kode' => 1,
                                    //         'id_pemilik' =>$this->session->userdata('id_retail')
                                    //     );
                                    // // print_r($datagudang);
                                    // $ok4 = $this->mgudang->keluarGudang($datagudang);
                                    // if($ok4)
                                    // {
                                        
                                    // }
                                    
                                // }
                                //gl
                            // if ($ok)
                            // {
                            //     $this->db->trans_commit();                                        
                            // }
                            // else
                            // {
                            //     $this->db->trans_rollback();
                            // }
                        }
                        // else
                        // {
                        //     $this->db->trans_rollback();
                        // }
                    }
                    
                }
                // else
                // {
                //     echo 'total 0';
                // }
                // echo 'ganti user<br><br>';
            }
            // echo '<br>== ganti item ==<br>';
        }
        return true;
    }  

    public function sortArray($array)
    {
        function cmp($a,$b)
        {
            return strcmp($a['jumlah'],$b['jumlah']);

        }
        // $this->mpembelian->cekstock('1200011121');
        $coba = array(
                array(
                    'id_item' =>'1111111',
                    'supplier' => '12',
                    'jumlah' => '3',
                ),array(
                    'id_item' =>'1111112',
                    'supplier' => '13',
                    'jumlah' => '2',
                ),
                array(
                    'id_item' =>'1111110',
                    'supplier' => '15',
                    'jumlah' => '7',
                ),
            );

        usort($array,"cmp");
        // print_r($coba);
        return $array;
    }

    public function cek($tanggal)
    {
        $this->db->reconnect();
        $this->db->select('id_petugas');
        $this->db->where('company_id',$this->session->userdata('company'));
        $this->db->where('privilege = 17');
        $read = $this->db->get('petugas');
        foreach ($read->result() as $data) 
        {
            $users[] = $data->id_petugas;
        }

        // $this->db->select('count(1) as tes');
        $this->db->where('tanggal',$tanggal);
        $this->db->where_in('id_pemilik',$users);
        $ok = $this->db->get('penjualan');
        return $ok->result();
    }

    public function haabbababa()
    {
        $item = array('1200011121','1200021121','1200031121','1200091121','1200101121','1200111121','1400011121','1400021121','1400031121','1400041121','1400051121','1400061121','1400071121','1500011121','1500031121','1500091121','1500101121','1700011121','1700021121','1700031121','1700041121','1700051121','2100011121','2100021121','2100031121','2100151121','2200041121','2200051121','2200061121','2200111121');
        $user = array('1111','1112','1113','1114','1115');

        foreach ($user as $users) 
        {
           foreach ($item as $items) 
           {
                $barcode = mt_rand(00001, 99999);
                $barcode_barang = $items.$users;

                $data = array(
                    'id_item' => $items,
                    // 'jumlah' => rand(15,30),
                    'item_price' => rand(20000,25000),
                    'item_marketing' => rand(0,2000),
                    
                    'game_player' => $users,
                    // 'barcode_barang' => $barcode_barang,
                    // 'id_supplier' => rand(1,5),
                    'game_id' =>1,
                    );
                $this->db->insert('player_detail',$data);               
           }
        }
    }
     public function hayoooo()
    {
        $item = array('1200011121','1200021121','1200031121','1200091121','1200101121','1200111121','1400011121','1400021121','1400031121','1400041121','1400051121','1400061121','1400071121','1500011121','1500031121','1500091121','1500101121','1700011121','1700021121','1700031121','1700041121','1700051121','2100011121','2100021121','2100031121','2100151121','2200041121','2200051121','2200061121','2200111121');        
        $user = array('1111','1112','1113','1114','1115');

        foreach ($user as $users) 
        {
           foreach ($item as $items) 
           {
                // $barcode = mt_rand(00001, 99999);
                // $barcode_barang = $items.$users;

                $data = array(
                    'id_item' => $items,
                    // 'jumlah' => rand(15,30),
                    'item_price' => rand(22000,30000),
                    'item_marketing' => rand(0,2000),
                    'game_player' => $users,
                    // 'barcode_barang' => $barcode_barang,
                    // 'id_supplier' => rand(1,5),
                    'game_id' =>1,
                    );
                $this->db->insert('player_detail',$data);               
           }
        }
    }
}
?>
