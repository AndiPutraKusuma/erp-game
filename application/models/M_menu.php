<?php class M_menu extends CI_Model {

    public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
    

    public function listMenu($company_id,$flag)
    {
        $group_id = $this->session->userdata('roles');
        $this->db->reconnect();
        $this->db->select('menu.id , menu.name, master2.access , controller');
        if($flag)
        {
            $this->db->where('menu.parent_id',0);
        }
        $this->db->where('menu.is_delete',0);
        $this->db->where('group_id',$group_id);
        $this->db->where_in('menu.company_id',$company_id);
        $this->db->from('core_role as master2');
        $this->db->join('core_menu as menu','master2.menu_id = menu.id and menu.is_delete = 0','INNER');
        $read = $this->db->get();
        $count = $read->num_rows();
        if($count > 0)
        {          
            foreach ($read->result() as $data) 
            {
                $result[] = $data;     
            }
        
            return $result;
        }
    }

    public function listIdMenu($company_id,$flag)
    {
        $this->db->reconnect();
        $this->db->select('id');
        if($flag)
        {
            $this->db->where('deleteable',1);
        }
        else
        {
            $this->db->where('deleteable',0);
        }
        $this->db->where_in('company_id',$company_id);
        $read = $this->db->get('core_menu');
        $count = $read->num_rows();
        if($count > 0)
        {          
            foreach ($read->result() as $data) 
            {
                $result[] = $data->id;     
            }
        
            return $result;
        }


    }

    // public function listMenu($company_id)
    // {
    //     $this->db->reconnect();
    //     $this->db->select('menu.id , menu.name, master2.access , controller as link');
    //     $this->db->where_in('menu.company_id',$company_id);
    //     $this->db->from('core_role as master2');
    //     $this->db->join('core_menu as menu','master2.menu_id = menu.id and menu.is_delete = 0','INNER');
    //     $read = $this->db->get();
    //     $count = $read->num_rows();
    //     if($count > 0)
    //     {   
    //         foreach ($read->result() as $data) 
    //         {
    //             $result[] = $data;     
    //         }
    //         return $result;
    //     }
    // }

    public function listParent($role,$flag)
    {   
        $company=$this->session->userdata('company'); 
        // print_r($company);       
        $this->db->reconnect();
        
        if($flag)
        {
            $where_join = 'master2.menu_id = menu.id and menu.is_delete = 0 and menu.parent_id = 0 and menu.company_id = '.$company;
        }
        else
        {
            $where_join = 'master2.menu_id = menu.id and menu.is_delete = 0 and menu.company_id = '.$company;
        }

        // print_r($user);
        // $this->db->reconnect();
        $this->db->select('master2.menu_id , menu.name , controller,icon_name');
        // $this->db->where('menu.company_id',$company);
        $this->db->where('master2.group_id = '.$role);
        $this->db->where('master2.access = 1');
        $this->db->from('core_role as master2');
        $this->db->join('core_menu as menu',$where_join,'INNER');
        $this->db->order_by('sort','asc');
        $read = $this->db->get();
        $parent = $read->result();
        foreach ($parent as $data) 
        {
            $parents[] = $data;
        }
        if($flag)
        {
            $result = array();
            if($parent)
            {
                foreach ($parents as $data) 
                {
                    $menu = $this->childMenu($data->menu_id,$role);
                    // print_r($menu);
                   if($menu)
                   {
                        $data->controller = '#';
                       array_push(
                                $result,
                                array(
                                    'parent'=>$data,
                                    'child'=>$menu,
                                )
                        ); 
                   }
                   else
                   {
                        array_push(
                                $result,
                                array(
                                    'parent'=>$data,
                                )
                        ); 
                   }
                }
                // print_r($result);
                return $result;
            }
        }
        else
        {
            if($parent)
            {
                return $parent;
            }
            else
            {
                return array();
            }
        }
        // return json_encode($result);
    }


    public function childMenu($parent,$user)
    {   
        $this->db->select('menu_id , name , controller , icon_name');
        $this->db->where('master.group_id = '.$user.'');
        $this->db->where('master.access = 1');
        $this->db->from('core_role as master');
        $this->db->join('core_menu as menu',"master.menu_id = menu.id and menu.is_delete = 0 and menu.parent_id =".$parent,'INNER');
        $this->db->order_by('sort','asc');
        $read = $this->db->get();
        $child = $read->result();
        $row = $read->num_rows();
        if($row >0 )
        {
            foreach ($child as $data) 
            {
                $result[] = $data;
            }        
            return $result;
        }
    }

    public function addMenu($data)
    {
        $this->db->reconnect();
        $query = $this->db->query("INSERT INTO core_menu(parent_id,NAME,controller,icon_name,creation_user,company_id) VALUES('$data[parent_id]','$data[name]','$data[controller]','$data[icon_name]','$data[creation_user]','$data[company_id]')");


        // $ok = $this->db->insert('core_menu', $data);
        if($query)
        {
            $this->db->select('id');
            $this->db->where('name',$data['name']);
            $this->db->where('parent_id',$data['parent_id']);
            $this->db->where('controller',$data['controller']);
            $this->db->where('icon_name',$data['icon_name']);
            $this->db->where('creation_user',$data['creation_user']);
            $read = $this->db->get('core_menu');
            foreach ($read->result() as $datamenu) 
            {
                $menu_id = $datamenu->id;
            }

            $data2 = array(
                'menu_id' => $menu_id,
                'group_id' => $data['group_id'],
                // 'group_id' => $data->group_id,
                'company_id' => $data['company_id'],
                'access' => 0,
                'creation_user' => $data['creation_user']
                );
            $ok2 = $this->db->insert('core_role', $data2);   
            return 'yeay';
        }
    }

    public function update($data,$id)
    {   $this->db->reconnect();
        $this->db->where('is_delete',0);
        $this->db->where('parent_id',$data['parent_id']);
        $this->db->where('name',$data['name']);
        $this->db->where('company_id',$this->session->userdata('company'));
        $this->db->where('id <>',$id);
        $read = $this->db->get('core_menu');
        // print_r($data);
        if($read->num_rows() < 1 )
        {
            $this->db->where('id',$id);
            $ok2 = $this->db->update('core_menu', $data);   
            if($ok2)
            {
                return true;
            }    
        }
        else
        {
            return false;
        }
        
    }

   public function addPrivilege($id,$group_id)
   {
        $company_id = $this->session->userdata('company');
        $status = 1;
        $this->db->reconnect();
        $this->db->select('menu_id');
        $this->db->where('group_id',$group_id);
        // $this->db->where('company_id',$company_id);
        $this->db->where('access',1);
        $read = $this->db->get('core_role');
        foreach ($read->result() as $data) 
        {
            $result[] = $data->menu_id;
        }
        $this->db->select('menu_id');
        $this->db->where('group_id',$group_id);
        // $this->db->where('company_id',$company_id);
        // $this->db->where('access',1);
        $read = $this->db->get('core_role');
        foreach ($read->result() as $data) 
        {
            $all[] = $data->menu_id;
        }
        foreach ($id as $menu_id) 
        {
            if(in_array($menu_id, $result) == false)
            {   if(in_array($menu_id, $all) == true)
                {   
                    $data = array(
                                'access' =>1,
                                );
                    $this->db->where('group_id',$group_id);
                    $this->db->where('menu_id',$menu_id);
                    $ok = $this->db->update('core_role',$data);
                    if(!$ok)
                    {
                        $status = 0;
                        break;
                    }
                }
                else
                {
                    $data = array(
                        'menu_id' => $menu_id,
                        'group_id' => $group_id,
                        'access' => 1,
                        'company_id' => $company_id,
                        'creation_user' => $this->session->userdata('id_retail'),
                        'creation_time' => date('Y-m-d'),
                        );
                    $ok = $this->db->insert('core_role',$data);
                    if(!$ok)
                    {
                        $status = 0;
                        break;
                    }
                }

            }
        }

        foreach ($result as $iddb) 
        {
            if(in_array($iddb, $id) == false)
            {
                $data = array(
                            'access' =>0,
                            );
                $this->db->where('group_id',$group_id);
                $this->db->where('menu_id',$iddb);
                $ok = $this->db->update('core_role',$data);
                if(!$ok)
                {
                    $status = 0;
                }
            }
        }
        
        if($status)
        {
            return 1;
        }
        else
        {
            return 0 ;
        }
   }

   public function FullMenu($company_id)
   {
        $group_id = $this->session->userdata('roles');

        $this->db->reconnect();
        $this->db->select('menu.id, menu.name , menu.parent_id , menu.controller , menu.icon_name ,master2.access ');
        $this->db->where('menu.is_delete',0);
        // $this->db->where('group_id',$group_id);
        $this->db->where_in('menu.company_id',$company_id);
        $this->db->from('core_menu as menu');
        $this->db->join('core_role as master2','master2.menu_id = menu.id and master2.group_id = '.$group_id,'INNER');
        $read = $this->db->get();
        $hasil = array();
        if($read->num_rows() > 0)
        {
            $i = 0;
            foreach ($read->result() as $data) 
            {
                $result[] = $data;
                $menu = $result[$i];
                $this->db->select('name');
                $this->db->where('id',$menu->parent_id);
                $read2 = $this->db->get('core_menu');
                if($read2->num_rows() > 0)
                {
                    foreach ($read2->result() as $data2) 
                    {
                        $parent = $data2->name;
                    }
                }
                else
                {
                    $parent = 'null';
                }

                array_push(
                        $hasil,
                        array(
                            'id' => $menu->id,
                            'name'=>$menu->name,
                            'parent'=>$parent,
                            'parent_id'=>$menu->parent_id,
                            'controller'=>$menu->controller,
                            'access' =>$menu->access,
                            'icon_name'=>$menu->icon_name
                        )
                ); 
                $i++;
            }


            return $hasil;
        }
   }

   public function delete($data,$id)
   {
        $this->db->reconnect();
        $this->db->trans_start();
        $this->db->where_in('id',$id);
        $ok = $this->db->update('core_menu',$data);
        if($ok)
        {
            $this->db->trans_commit();
            return true;
        }
        else
        {
            $this->db->trans_rollback();
            return false;
        }
   }
   public function getID($menu)
   {    
        $hasil = 0;
        $this->db->reconnect();
        $this->db->select('id');
        $this->db->where('lower(controller)',strtolower($menu));
        $this->db->where('company_id',$this->session->userdata('company'));
        $read = $this->db->get('core_menu');
        foreach ($read->result() as $data) 
        {
           $hasil = $data->id;
        }
        return $hasil;
   }

   public function checkAccess($role)
   {
        $hasil = 0;
        $menu = $this->getId(strtolower($this->uri->uri_string()));
        $this->db->reconnect();
        $this->db->select('access');
        $this->db->where('group_id',$role);
        $this->db->where('menu_id',$menu);
        $read = $this->db->get('core_role');
        foreach ($read->result() as $data) 
        {
            $hasil = $data->access;
        }
        return $hasil;
   }

}
?>
