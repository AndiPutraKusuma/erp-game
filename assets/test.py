#!/usr/bin/env python
import pandas as pd
import numpy as np
import math
import sys;
import gc
import pprint as pprint
import datetime
import MySQLdb
import matplotlib.pyplot as plt
from statsmodels.tsa.holtwinters import ExponentialSmoothing
uid = sys.argv[1];
game_id = sys.argv[2];
id_item = sys.argv[3];
conn = MySQLdb.connect(host= "localhost",
                  user="root",
                  passwd="hominatest",
                  db="retail_game")
#print(sys.argv[1]);
		
if uid != None :
	# conn = MySQLdb.connect(host= "localhost",
 #                  user="root",
 #                  passwd="hominatest",
 #                  db="retail_game")
	# df=None
	# sql=None
	# train=None
	# model=None
	# pred=None
	# start = None
	# end = None
	# gc.collect()
	# print(id_item)
	# print(uid)
	# print(game_id)
	sql = 'SELECT * FROM (SELECT penjualan.tanggal as ds, SUM(detail_penjualan.jumlah) as y FROM penjualan, detail_penjualan WHERE penjualan.id_so = detail_penjualan.id_so AND penjualan.id_pemilik = %s and penjualan.game_id= %s and detail_penjualan.id_item = %s GROUP BY penjualan.tanggal ORDER BY penjualan.tanggal DESC LIMIT 30) A ORDER BY ds ASC'
	# df = pd.read_csv('sales2.csv', 
	#                  parse_dates=['ds'], 
	#                  index_col='ds'
	# )
	# print(conn)
	# print(sql)
	df = pd.read_sql(sql, con=conn, params=(uid,game_id,id_item), index_col='ds');
	# df["ds"] = pd.to_datetime(df["ds"])
	# df["ds"] = df["ds"].dt.strftime('%d-%m-%Y')
	# df["y"] = df["y"].astype(int)
	# print(df)
	# df.set_index('ds');
	# print(type(df["y"]))
	# print(type(df["ds"]))
	# print(df)
	# df["ds"] = df["ds"].strftime('%d/%m/%Y')
	# pd.option_context('display.date_dayfirst', True)
	df.index = pd.to_datetime(df.index)
	df = df.resample('D').mean()
	df = df.fillna(0.1)
	# print(type(df))
	print (df)
	# df.index.freq = 'D'
	train= df.iloc[:-1,0]
	start = df.index[-1]
	end = df.index[-1]+ datetime.timedelta(7)
	print(train)
	# print(model)
	model = ExponentialSmoothing(train, seasonal='mul', seasonal_periods=7).fit()
	# print(start)
	pred = model.predict(start=start, end=end)
	# # mse = ((test - pred) ** 2).mean();
	# # smape = 100 - (200*abs(pred - test)/(pred + test)).mean();
	# # print('Prediction quality: {:.2f} MSE ({:.2f} RMSE)'.format(mse, math.sqrt(mse)));
	# # print('Prediction quality: {:.2f} % SMAPE'.format(smape));
	# print(pred)
	a_list = [];
	for p in range (0, 7):
		a_list.append(round(pred[p]));
	sdate = pred.index[0]
	# print (a_list)
	try:
		cur = conn.cursor();
		cur.execute("""SELECT `id` FROM `bi_forecast` WHERE `id_pemilik`=%s and `game_id` = %s and id_item=%s""",(uid,game_id,id_item));
		row = cur.fetchone();
		# print(row);
		if row is not None:
			for ind in cur:
				up_id = ind[0];
			# print (up_id);
			cur.execute("""UPDATE `bi_forecast` SET `start_date`=%s,`day1`=%s,`day2`=%s,`day3`=%s,`day4`=%s,`day5`=%s,`day6`=%s,`day7`=%s WHERE `id`=%s""",(sdate,a_list[0],a_list[1],a_list[2],a_list[3],a_list[4],a_list[5],a_list[6],up_id));
			# print("lala");
		else :
			cur.execute("""INSERT INTO `bi_forecast`(id_item, `start_date`, `id_pemilik`,game_id, `day1`, `day2`, `day3`, `day4`, `day5`, `day6`, `day7`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(id_item,sdate,uid,game_id,a_list[0],a_list[1],a_list[2],a_list[3],a_list[4],a_list[5],a_list[6]));
			# print("lili");
		print('Success');
	except MySQLdb.Error as e: 
		print(e);
		sys.exit(1);
	finally:
		if conn:
			conn.commit();
			conn.close()
	# print(test)
	# plt.plot(train.index, train, label='Train')
	# plt.plot(test.index, test, label='Test')
	# plt.plot(pred.index, pred, label='Holt-Winters')
	# plt.legend(loc='best')
	# plt.show()
	# ExponentialSmoothing.initialize(pred)
	# return None