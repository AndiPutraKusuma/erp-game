#!/usr/bin/env python
import pandas as pd;
import numpy as np;
import sys;
# from pprint import pprint
import math;
import MySQLdb
from fbprophet import Prophet;
import matplotlib.pyplot as plt;
filePath = sys.argv[1];
conn = MySQLdb.connect(host= "localhost",
                  user="root",
                  passwd="hominatest",
                  db="retail_game")
#print(sys.argv[1]);
if filePath != None :
	# df = pd.read_csv('C:\py_file\\testBrg01.csv');
	# df = pd.read_csv(filePath + '.csv');
	# df = pd.read_csv('C:\py_file\\test.csv');
	df = pd.read_sql('SELECT penjualan.tanggal as ds, SUM(detail_penjualan.jumlah) as y FROM penjualan, detail_penjualan WHERE penjualan.id_so = detail_penjualan.id_so AND penjualan.id_pemilik = %(uid)s and penjualan.game_id = 10 and detail_penjualan.id_item = "1200011121" GROUP BY penjualan.tanggal ORDER BY penjualan.tanggal DESC ', con=conn, params={"uid":filePath});
	df['y'] = np.log(df['y']);
	# ax = df.plot(color='#006699');
	# ax.set_ylabel('Sales');
	# ax.set_xlabel('Date');
	# plt.show();
	# df_train = df[:475];
	# df_test = df[475:];
	# df_train = df[:350];
	# df_test = df[350:];
	# df_test['y'] = np.log(['y'][:2500]);
	# print(df_train.tail());
	# # print(df_test.head());
	m = Prophet(daily_seasonality = True, weekly_seasonality = True, yearly_seasonality = True);
	m.fit(df);
	future = m.make_future_dataframe(periods=1, freq='d');
	# print(future.tail());
	forecast = m.predict(future);
	# print (forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail());
	# m.plot(forecast);
	# y_hat = np.exp(forecast['yhat'][350:]);
	# y_hat = np.exp(forecast['yhat'][475:]);
	# y_true = np.exp(df_test['y']);
	# print(y_hat.tail());
	# print(y_true.tail());
	# y_true_plot = df['y'];
	# y_hat_plot = forecast['yhat'][350:];
	# yhat_upper = forecast['yhat_upper'][350:];
	# yhat_lower = forecast['yhat_lower'][350:];
	# y_hat_plot = forecast['yhat'][475:];
	# yhat_upper = forecast['yhat_upper'][475:];
	# yhat_lower = forecast['yhat_lower'][475:];
	# mse = ((y_hat - y_true) ** 2).mean();
	# smape = 100 - (200*abs(y_hat - y_true)/(y_hat + y_true)).mean();
	# print('Prediction quality: {:.2f} MSE ({:.2f} RMSE)'.format(mse, math.sqrt(mse)));
	# print('Prediction quality: {:.2f} % SMAPE'.format(smape));
	df['y'] = np.exp(df['y']);
	# print('done');
	forecast_data = forecast;
	forecast_data['yhat'] = round(np.exp(forecast_data['yhat']), 2);
	# out = forecast_data[['ds','yhat']].to_json(orient='records', date_format='iso');
	# with open(filePath + '.json', 'w') as f:
	#     f.write(out)
	print(forecast_data[['ds','yhat']].tail(7));
	# print("done");
	print(df.tail(7));
	# x = conn.cursor();
	print(forecast_data.iloc[7]['yhat']);
	print(len(forecast_data.index));
	la = len(forecast_data.index);
	a_list = [];
	for p in range (la-7, la):
		a_list.append(forecast_data.iloc[p]['yhat']);
	sdate = forecast_data.iloc[la-7]['ds']
	print (type(filePath));
	try:
		cur = conn.cursor();
		cur.execute("""SELECT `id` FROM `bi_forecast` WHERE `id_pemilik`=%s""" % filePath);
		row = cur.fetchone();
		# print(row);
		if row is not None:
			for ind in cur:
				up_id = ind[0];
			# print (up_id);
			cur.execute("""UPDATE `bi_forecast` SET `start_date`=%s,`day1`=%s,`day2`=%s,`day3`=%s,`day4`=%s,`day5`=%s,`day6`=%s,`day7`=%s WHERE `id`=%s""",(sdate,a_list[0],a_list[1],a_list[2],a_list[3],a_list[4],a_list[5],a_list[6],up_id));
		# print("lala");
		else :
			cur.execute("""INSERT INTO `bi_forecast`(`start_date`, `id_pemilik`, `day1`, `day2`, `day3`, `day4`, `day5`, `day6`, `day7`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(sdate,filePath,a_list[0],a_list[1],a_list[2],a_list[3],a_list[4],a_list[5],a_list[6]));
		print('Success');
	except MySQLdb.Error as e: 
		print(e);
		sys.exit(1);
	finally:
		if conn:
			# conn.commit();
			conn.close()
	# plt.plot(y_hat_plot, color='#ff0066', label='Forecast');
	# plt.plot(y_true_plot, label='Original', color='#006699');
	# plt.fill_between(y_true_plot[260:].index, yhat_upper, yhat_lower, color='#ff0066', alpha=0.25)
	# plt.ylabel('y');
	# plt.xlabel('ds');
	# # plt.savefig('./img/prophet_forecast.png')
	# plt.show()
